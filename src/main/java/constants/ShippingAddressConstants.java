package constants;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import util.parsers.ExcelParser;

import java.io.IOException;
import java.util.List;

/**
 * Created by M.Malyus on 3/23/2018.
 */
public class ShippingAddressConstants {
    private ExcelParser excelParser = new ExcelParser();
    private List<String> stringList = excelParser.getStringExcelList();
    private List<Double> numericList = excelParser.getNumericExcelList();
    private String street = stringList.get(0);
    private String city = stringList.get(1);
    private String state = stringList.get(2);
    private String zip = stringList.get(6);
    private String chicoStreet = stringList.get(3);
    private String chicoCity = stringList.get(4);
    private String chicoState = stringList.get(5);
    private Double chicoZip = numericList.get(0);

    public ShippingAddressConstants() throws IOException, InvalidFormatException {
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getChicoStreet() {
        return chicoStreet;
    }

    public String getChicoCity() {
        return chicoCity;
    }

    public String getChicoState() {
        return chicoState;
    }

    public Double getChicoZip() {
        return chicoZip;
    }
}
