package constants;

/**
 * Created by M.Malyus on 3/19/2018.
 */
public class ServicesConstants {
    /*Services names*/
    public static final String FIRST_SERVICE_NAME = "American Airlines Special Form";
    public static final String SECOND_SERVICE_NAME = "Hawaiian Airlines Special Form";
    public static final String THIRD_SERVICE_NAME = "JetBlue Airways Special Form";
    public static final String FOURTH_SERVICE_NAME = "Spirit Airways Special Form";
    public static final String FIFTH_SERVICE_NAME = "United Airways Special Form";

    /*Services prices*/
    public static final String DEF_SUBTOTAL = "$0";
    public static final String FIRST_SERVICE_PRICE = "$18";
    public static final String SECOND_SERVICE_PRICE = "$19";
    public static final String THIRD_SERVICE_PRICE = "$23";
    public static final String FOURTH_SERVICE_PRICE = "$23";
    public static final String FIFTH_SERVICE_PRICE = "$5";

    /*Services combinations prices*/
    public static final String TOTAL_PRICE = "$88";
    public static final String FIRST_AND_SECOND_PRICE = "$37";
    public static final String FIRST_AND_THIRD_PRICE = "$41";
    public static final String FIRST_AND_FOURTH_PRICE = "$41";
    public static final String FIRST_AND_FIFTH_PRICE = "$23";
    public static final String SECOND_AND_THIRD_PRICE = "$42";
    public static final String SECOND_AND_FOURTH_PRICE = "$42";
    public static final String SECOND_AND_FIFTH_PRICE  = "$24";

    /*Services logo*/
    public static final String FIRST_SERVICE_ICON = "http://10.10.0.77/esa/img/airlines/American_Airlines_logo.png";
    public static final String SECOND_SERVICE_ICON = "http://10.10.0.77/esa/img/airlines/Hawaiian_Airlines_logo.png";
    public static final String THIRD_SERVICE_ICON = "http://10.10.0.77/esa/img/airlines/Hawaiian_Airlines_logo.png";
    public static final String FOURTH_SERVICE_ICON = "http://10.10.0.77/esa/img/airlines/Spirit_Airways_logo.png";
    public static final String FIFTH_SERVICE_ICON = "http://10.10.0.77/esa/img/airlines/United_Airways_logo.png";

}
