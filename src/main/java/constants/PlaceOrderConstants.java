package constants;

/**
 * Created by M.Malyus on 4/3/2018.
 */
public class PlaceOrderConstants {
    public static final String COMBO_PLAN_PRICE = "$139.00";
    public static final String THREE_PETS_PRICE = "$15.00";
    public static final String FULL_NAME = "Alex Mason";
    public static final String LA_STREET = "8727 S HARVARD BLVD";
    public static final String CA_CITY = "LOS ANGELES";
    public static final String CA_STATE = "CA";
    public static final String LA_ZIP = "90047-3316";

    /*Pet#1*/
    public static final String FIRST_PETS_TYPE = "raccoon";
    public static final String FIRST_PETS_BREED = "cat";
    public static final String FIRST_PETS_NAME = "elmo";
    public static final String FIRST_PETS_WEIGHT="1 to 10lbs.";

    /*Pet#2*/
    public static final String SECOND_PETS_TYPE = "raccoon";
    public static final String SECOND_PETS_BREED = "cat#2";
    public static final String SECOND_PETS_NAME = "name#2";
    public static final String SECOND_PETS_WEIGHT = "1 to 10lbs.";

    /*Pets#3*/
    public static final String THIRD_PETS_TYPE="raccoon";
    public static final String THIRD_PETS_BREED = "cat#3";
    public static final String THIRD_PETS_NAME = "name#3";
    public static final String THIRD_PETS_WEIGHT = "1 to 10lbs.";

    /*Packages names*/
    public static final String COMBO_PLAN_NAME = "Combo Plan";

    /*Additional services names*/
    public static final String FIRST_SERVICE_NAME = "American Airlines Special Form";
    public static final String SECOND_SERVICE_NAME = "Hawaiian Airlines Special Form";
    public static final String THIRD_SERVICE_NAME = "JetBlue Airways Special Form";
    public static final String FOURTH_SERVICE_NAME = "Spirit Airways Special Form";
    public static final String FIFTH_SERVICE_NAME = "United Airways Special Form";
    /*Additional services prices*/
    public static final String FIRST_SERVICE_PRICE = "$18.00";
    public static final String SECOND_SERVICE_PRICE = "$19.00";
    public static final String THIRD_SERVICE_PRICE = "$23.00";
    public static final String FOURTH_SERVICE_PRICE = "$23.00";
    public static final String FIFTH_SERVICE_PRICE = "$5.00";

}
