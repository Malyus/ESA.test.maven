package constants;

import ru.qatools.properties.PropertyLoader;
import util.config.LinkConfig;

/**
 * Created by M.Malyus on 3/28/2018.
 */
public class PricingConstants {
    private static LinkConfig linkConfig = PropertyLoader.newInstance().populate(LinkConfig.class);
    /*ESA certification prices*/
    public static final String ESA_HOUSING_LETTER_PRICE = "$109";
    public static final String ESA_COMBO_PLAN_PRICE = "$139";
    public static final String ESA_TRAVEL_LETTER_PRICE = "$119";
    /*ESA privileges first*/
    public static final String LEFT_COLUMN_FIRST = "Price Match Guarantee.";
    public static final String LEFT_COLUMN_SECOND = "Open 7 days a week.";
    public static final String LEFT_COLUMN_THIRD = "Medical Board Licensed Physicians.";
    /*ESA privileges second*/
    public static final String RIGHT_COLUMN_FIRST = "HIPAA and ASA Compliant.";
    public static final String RIGHT_COLUMN_SECOND = "HIGH QUALITY Plastic Photo ID Cards.";
    public static final String RIGHT_COLUMN_THIRD = "Evaluation is FREE if you are NOT approved.";
    /*Links*/
    public static final String TERMS_LINK = linkConfig.getTermsLink();
    public static final String PRIVACY_LINK = linkConfig.getPrivacyLink();

}
