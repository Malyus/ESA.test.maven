import com.constants.ShippingAddressConstants;
import com.dbutils.ConnectionFactory;
import com.dbutils.DataBaseExecute;
import com.stepspages.ShippingAddressPage;
import com.util.ExcelParser;
import com.util.UserRandomGenerator;
import com.util.Years;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class Main {
    final static Logger LOG = Logger.getLogger(Main.class);
    public static void main(String[] args) throws SQLException, IOException, InvalidFormatException {
        ExcelParser excelParser = new ExcelParser();
        ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();
        List<String> list = excelParser.getStringExcelList();
        for(String a:list){
            System.out.println(a);
        }
        System.out.println("Street:" + shippingAddressConstants.getChicoStreet());
        System.out.println("City:" + shippingAddressConstants.getChicoCity());
        System.out.println("State:" + shippingAddressConstants.getChicoState());
        System.out.println("Zip:" + shippingAddressConstants.getChicoZip());
    }

}

