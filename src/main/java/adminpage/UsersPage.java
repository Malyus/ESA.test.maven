package adminpage;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import stepspage.PaymentDetailsPage;
import webdriver.SingletonDriver;

import java.util.List;

/**
 * Created by M.Malyus on 4/24/2018.
 */
public class UsersPage extends Page {
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);
    final static Logger LOG = Logger.getLogger(UsersPage.class);

    @FindBy(xpath = "//i[@class='fa fa-plus']")
    private WebElement addNewUserButton;

    @FindAll(@FindBy(xpath = "//div[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4']"))
    private List<WebElement> listOfUsers;

    @FindAll(@FindBy(xpath = "//div[@class='doctor-data']//p"))
    private List<WebElement> listOfUsersNames;

    @FindAll(@FindBy(xpath = "//i[@class='fa fa-times']"))
    private List<WebElement> listOfUsersDeleteButtons;

    @FindAll(@FindBy(xpath = "//label//span"))
    private List<WebElement> listOfUsersCheckboxTypes;

    @FindAll(@FindBy(xpath = "//p[@class='user-name']"))
    private List<WebElement> listOfUsersEmails;

    @FindAll(@FindBy(xpath = "//i[@class='fa fa-pencil']"))
    private List<WebElement> listOfUsersEditButtons;

    public UsersPage() throws Exception {
    }

    @Step
    public AddNewUserPage clickOnAddNewUser() {
        wait.until(ExpectedConditions.visibilityOf(addNewUserButton));
        addNewUserButton.click();
        return PageFactory.initElements(driver,AddNewUserPage.class);
    }

    @Step
    @Override
    public boolean IsPageLoaded(WebElement element) throws Exception {
        return super.IsPageLoaded(addNewUserButton);
    }
    @Step
    public boolean checkPresentsOfUsersPage(){
        return addNewUserButton.isDisplayed();
    }
    @Step
    public int getCountOfUsers(){
        return listOfUsers.size();
    }
    @Step
    public String getLastUserName(){
        return listOfUsersNames.get(listOfUsers.size()-1).getText();
    }
    @Step
    public void deleteCreatedUser(){
        listOfUsersDeleteButtons.get(listOfUsersDeleteButtons.size()-1).click();
        driver.switchTo().alert().accept();
    }
    /*You can select type of user by:
    * admin
    * manager
    * shipper*/
    @Step
    public String getActiveOfCreatedUserType(String typeOfOfUser){
        String active = "";
        if(typeOfOfUser.equalsIgnoreCase("admin")){
            LOG.info("Getting admin-checkbox active");
            active = listOfUsersCheckboxTypes.get(listOfUsersCheckboxTypes.size()-3).
                    getCssValue("border-color");
        }
        else if(typeOfOfUser.equalsIgnoreCase("manager")){
            LOG.info("Getting manager-checkbox active");
            active = listOfUsersCheckboxTypes.get(listOfUsersCheckboxTypes.size()-2)
                    .getCssValue("border-color");
        }
        else if(typeOfOfUser.equalsIgnoreCase("shipper")){
            LOG.info("Getting shipper-checkbox active");
            active = listOfUsersCheckboxTypes.get(listOfUsersCheckboxTypes.size()-1)
                    .getCssValue("border-color");
        }
       return active;
    }
    @Step
    public String getCreatedUserName(){
        return listOfUsersEmails.
                get(listOfUsersEmails.size()-1).getText();
    }
    @Step
    public AddNewUserPage editCreatedUser(){
        listOfUsersEditButtons.
                get(listOfUsersEditButtons.size()-1).click();
        return PageFactory.initElements(driver,AddNewUserPage.class);
    }

}
