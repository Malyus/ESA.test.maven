package adminpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 4/24/2018.
 */
public class DashboardPage extends Page{
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//ul//li[9]")
    private WebElement usersMenuButton;

    @FindBy(xpath = "//i[@class='fa fa-fw fa-sign-out']")
    private WebElement logOutButton;

    @FindBy(xpath = "//div[@class='row diagram sales_overview']//h3[@class='diagram-header']")
    private WebElement presentsOfDashboardPage;

    @FindBy(xpath = "//li[@data-key='activity']")
    private WebElement presentsOfOrdersPage;

    @FindBy(xpath = "//nav/div[2]/ul/li[2]")
    private WebElement clientsMenuButton;

    public DashboardPage() throws Exception {
    }

    @Step
    public UsersPage clickOnUsersMenuButton(){
        wait.until(ExpectedConditions.visibilityOf(usersMenuButton));
        usersMenuButton.click();
        return PageFactory.initElements(driver,UsersPage.class);
    }
    @Step
    public AdminLoginPage clickOnLogOutButton(){
        logOutButton.click();
        return PageFactory.initElements(driver,AdminLoginPage.class);
    }
    @Step
    public boolean checkPresentsOfDashboardPage(){
        return presentsOfDashboardPage.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfOrdersPage(){
        return presentsOfOrdersPage.isDisplayed();
    }
    @Step
    public ClientsPage clickOnClientsMenuButton(){
        clientsMenuButton.click();
        return PageFactory.initElements(driver, ClientsPage.class);
    }
}
