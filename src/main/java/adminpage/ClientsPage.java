package adminpage;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

import java.util.List;

/**
 * Created by M.Malyus on 5/10/2018.
 */
public class ClientsPage extends Page {
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);
    final static Logger LOG = Logger.getLogger(ClientsPage.class);

    @FindBy(xpath = "//li[@data-key='all']")
    private WebElement allFilterButton;

    @FindBy(xpath = "//select[@id='limit']")
    private WebElement itemsInListMenu;

    @FindBy(xpath = "//select[@id='limit']//option[@selected]")
    private WebElement selectedOptionInItemListMenu;

    @FindAll(@FindBy(xpath = "//select[@id='limit']//option"))
    private List<WebElement> itemsInListOptionList;

    @FindBy(xpath = "//button[@id='select_all']")
    private WebElement selectAllButton;

    @FindBy(xpath = "//span[@class='selected-count']")
    private WebElement selectedItemsCount;

    @FindBy(xpath = "//div[1]/div[1]/div[1]/i")
    private WebElement selectFirstClientButton;

    @FindAll(@FindBy(xpath = "//div[@class='checker']"))
    private List<WebElement> listOfSelectClientButton;

    @FindBy(xpath = "//select[@id='sort']")
    private WebElement sortByLetters;

    public ClientsPage() throws Exception {
    }

    @Step
    public boolean checkPresentsOfClientsPage(){
        return allFilterButton.isDisplayed();
    }
    @Step
    public void selectItemsInListByValue(String value){
        Select itemsInList = new Select(itemsInListMenu);
        itemsInList.selectByValue(value);
    }
    @Step
    public String getSelectedOptionOfItemsListMenu(){
       return selectedOptionInItemListMenu.getAttribute("value");
    }
    @Step
    public String getOptionItemOfListMenu(int index){
        return itemsInListOptionList.get(index).getAttribute("value");
    }
    @Step
    public void clickOnSelectAllButton(){
        selectAllButton.click();
    }
    @Step
    public String getCountOfSelectedItems(){
        return selectedItemsCount.getText();
    }
    @Step
    public void selectFirstClient(){
        selectFirstClientButton.click();
    }
    @Step
    public void selectClients(int countOfClients){
        for(int i = 0;i<countOfClients;i++){
            listOfSelectClientButton.get(i).click();
        }
    }
    @Step
    public void selectPairedClients(int countOfClients){
        for(int i=0;i<countOfClients*2;i++){
            listOfSelectClientButton.get(++i).click();
        }
    }
    @Step
    public String getOptionsOfSortByLetters(int index){
        Select select = new Select(sortByLetters);
        return select.getOptions().get(index).getText();
    }
}
