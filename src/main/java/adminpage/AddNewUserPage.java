package adminpage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

import java.util.List;

/**
 * Created by M.Malyus on 4/24/2018.
 */
public class AddNewUserPage extends Page {
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//input[@id='first-name']")
    private WebElement nameField;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//label[@for='type_admin_1']//span")
    private WebElement adminTypeButton;

    @FindBy(xpath = "//label[@for='type_manager_2']//span")
    private WebElement managerTypeButton;

    @FindBy(xpath = "//label[@for='type_shipper_1']//span")
    private WebElement shipperTypeButton;

    @FindBy(xpath = "//button[@class='btn-main btn-save']")
    private WebElement saveButton;

    @FindAll(@FindBy(xpath = "//div[@class='radio-checker']//span"))
    private List<WebElement> listOfUsersTypesButtons;

    @FindBy(xpath = "//button[@class='btn-main btn-type-3 btn-cancel']")
    private WebElement cancelButton;

    public AddNewUserPage() throws Exception {
    }
    @Step
    public boolean checkPresentsOfAddNewUserPage(){
        return nameField.isDisplayed();
    }
    @Step
    public void writeName(String name){
        nameField.sendKeys(name);
    }
    @Step
    public void writeEmail(String email){
        emailField.clear();
        emailField.sendKeys(email);
    }
    @Step
    public void writePassword(String password){
        passwordField.sendKeys(password);
    }
    @Step
    public void clickOnManagerTypeButton(){
        managerTypeButton.click();
    }
    @Step
    public void clickOnShipperTypeButton(){
        shipperTypeButton.click();
    }
    @Step
    public void clickOnAdminTypeButton(){
        adminTypeButton.click();
    }
    @Step
    public void clickOnSaveButton(){
        saveButton.click();
    }
    @Step
    public void clickOnCancelButton(){
        cancelButton.click();
    }
    @Step
    public void selectUserType(String userType){
        if(userType.equalsIgnoreCase("manager")){
            listOfUsersTypesButtons.
                    get(listOfUsersTypesButtons.size()-2).click();
        }
        else if(userType.equalsIgnoreCase("shipper")){
            listOfUsersTypesButtons.
                    get(listOfUsersTypesButtons.size()-1).click();
        }
    }
}
