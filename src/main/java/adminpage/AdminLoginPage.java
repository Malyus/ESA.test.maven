package adminpage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

import java.util.Set;

/**
 * Created by M.Malyus on 4/24/2018.
 */
public class AdminLoginPage extends Page {
    private WebDriver driver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(driver, 5, 1000);

    @FindBy(xpath = "//input[@id='email']")
    private WebElement emailInput;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//button[@class='btn-main']")
    private WebElement submitButton;

    public AdminLoginPage() throws Exception {
    }
    public void openInNewWindow(String url) {
        ((JavascriptExecutor) driver)
                .executeScript("window.open(arguments[0])", url);
    }
    public void switchToANewWindow(){
        Set< String > newHandles = driver.getWindowHandles();
        driver.switchTo().window((String) newHandles.toArray()[1]);
    }
    @Step
    public void writeEmail(String email){
        wait.until(ExpectedConditions.visibilityOf(emailInput));
        emailInput.sendKeys(email);
    }
    @Step
    public void writePassword(String password){
        passwordInput.sendKeys(password);
    }
    @Step
    public void clickOnSubmitButton(){
        submitButton.click();
    }
}
