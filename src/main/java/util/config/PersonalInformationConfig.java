package util.config;


import ru.qatools.properties.Property;
import ru.qatools.properties.Resource;

/**
 * Created by M.Malyus on 4/11/2018.
 */
@Resource.File("src/main/resources/stepsproperties/personalinformation/personalinformation.properties")
public interface PersonalInformationConfig {

    @Property("SelectedMonth")
    String getSelectedMonth();

    @Property("SelectedDay")
    String getSelectedDay();

    @Property("SelectedYear")
    String getSelectedYear();

    @Property("SelectedMaritalStatus")
    String getSelectedMaritalStatus();

    @Property("MarriedStatus")
    String getMarriedStatus();

    @Property("SingleStatus")
    String getSingleStatus();

    @Property("DivorcedStatus")
    String getDivorcedStatus();

    @Property("WidowedStatus")
    String getWidowedStatus();
}
