package util.config;

import ru.qatools.properties.Property;
import ru.qatools.properties.Resource;

/**
 * Created by M.Malyus on 4/27/2018.
 */
@Resource.File("src/main/resources/adminproperties/admin.properties")
public interface AdminConfig {

    @Property("Active")
    String getActive();

    @Property("Disable")
    String getDisable();

}
