package util.config;

import ru.qatools.properties.Property;
import ru.qatools.properties.Resource;

/**
 * Created by M.Malyus on 4/11/2018.
 * Use properties.lib for fast init
 */
@Resource.File("src/main/resources/properties/pricing/pricing.properties")
public interface ServicesConfig {

    @Property("firstService")
    String getFirstService();

    @Property("secondService")
    String getSecondService();

    @Property("thirdService")
    String getThirdService();

    @Property("fourthService")
    String getFourthService();
}
