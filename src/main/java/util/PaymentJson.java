package util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by M.Malyus on 3/30/2018.
 */
public class PaymentJson {

    private JSONObject startParse() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object object = parser.parse(new FileReader(new File("src\\main\\resources\\properties\\creditcard\\creditcard")));
        JSONObject jsonObject = (JSONObject) object;
        return jsonObject;
    }
    public String getCreditCard() throws IOException, ParseException {
        JSONObject jsonObject = startParse();
        String creditCard = jsonObject.get("creditCard").toString();
        return creditCard;
    }
    public String getExpDate() throws IOException, ParseException {
        JSONObject jsonObject = startParse();
        String expDate = jsonObject.get("expDate").toString();
        return expDate;
    }
    public String getCvv() throws IOException, ParseException {
        JSONObject jsonObject = startParse();
        String cvv = jsonObject.get("cvv").toString();
        return cvv;
    }
}
