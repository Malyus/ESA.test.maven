package util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by M.Malyus on 4/13/2018.
 */
public class Date {
    public String getCurrentDate(){
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM//yy");
        LocalDateTime currentDay = LocalDateTime.now();
        String today = currentDay.format(dateFormat);
        return today;
    }
}
