package util;

import ru.qatools.properties.Property;
import ru.qatools.properties.Resource;

/**
 * Created by M.Malyus on 4/11/2018.
 */
@Resource.File("src\\main\\resources\\properties\\links\\links.properties")
public interface LinkConfig {

    @Property("TermsLink")
    String getTermsLink();

    @Property("PrivacyLink")
    String getPrivacyLink();
}
