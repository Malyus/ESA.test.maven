package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 1/26/2018.
 */
public class PersonalInformationReader {
    private Properties properties;

    public PersonalInformationReader(){
        properties = new Properties();

        try{
            properties.load(new FileInputStream(new File("src/main/resources/stepsproperties/personalinformation/personalinformation.properties")));
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public String getSelectedMonthValue(){
        return properties.getProperty("SelectedMonth");
    }
    public String getSelectedDayValue(){
        return properties.getProperty("SelectedDay");
    }
    public String getSelectedYearValue(){
        return properties.getProperty("SelectedYear");
    }
    public String getSelectedMaritalStatus(){
        return properties.getProperty("SelectedMaritalStatus");
    }
    public String getMarriedStatus(){
        return properties.getProperty("MarriedStatus");
    }
    public String getSingleStatus(){
        return properties.getProperty("SingleStatus");
    }
    public String getDivorcedStatus(){
        return properties.getProperty("DivorcedStatus");
    }
    public String getWidowedStatus(){
        return properties.getProperty("WidowedStatus");
    }
}
