package util;

import java.util.ArrayList;

/**
 * Created by M.Malyus on 1/26/2018.
 */
public class Years {

    public static ArrayList<String> create(){
        ArrayList<String > createdList = new ArrayList<>();
        int year = 1928;
        for(int i = 0;i<74;i++){
         createdList.add(i,String.valueOf(year));
         year = year + 1;
        }
        return createdList;
    }
}
