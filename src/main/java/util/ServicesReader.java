package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 1/30/2018.
 */
public class ServicesReader {
    private Properties properties;

    public ServicesReader(){
        properties = new Properties();

        try{
            properties.load(new FileInputStream(new File("src/main/resources/mainproperties/pricing/pricing.properties")));
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public String getFirstService(){
        return properties.getProperty("firstService");
    }
    public String getSecondService(){
        return properties.getProperty("secondService");
    }
    public String getThirdService(){
        return properties.getProperty("thirdService");
    }
    public String getFourthService(){
        return properties.getProperty("fourthService");
    }
}
