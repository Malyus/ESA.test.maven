package util.parsers;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by M.Malyus on 3/23/2018.
 */
public class ExcelParser {
  private List<String> stringCellList = new ArrayList<>();
  private List<Double> numericCellList = new ArrayList<>();
  private OPCPackage opcPackage = OPCPackage.open(new File("src\\main\\resources\\excel\\ShippingAddress.xlsx"));
  private XSSFWorkbook workbook = new XSSFWorkbook(opcPackage);
  private XSSFSheet excelSheet = workbook.getSheetAt(0);

  public ExcelParser() throws InvalidFormatException, IOException {
  }

  public  List<String> getStringExcelList(){
      Iterator<Row> rowIterator = excelSheet.rowIterator();
      while (rowIterator.hasNext()){
          Row row = rowIterator.next();
          Iterator<Cell> cellIterator = row.cellIterator();
          while (cellIterator.hasNext()){
              Cell cell = cellIterator.next();
              if(cell.getCellType()==Cell.CELL_TYPE_STRING){
                  stringCellList.add(cell.getStringCellValue());
              }
          }
      }
      return stringCellList;
  }
  public List<Double> getNumericExcelList(){
      Iterator<Row> rowIterator = excelSheet.rowIterator();
      while (rowIterator.hasNext()){
          Row row = rowIterator.next();
          Iterator<Cell> cellIterator = row.cellIterator();
          while (cellIterator.hasNext()){
              Cell cell = cellIterator.next();
              if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC){
                  numericCellList.add(cell.getNumericCellValue());
              }
          }
      }
      return numericCellList;
  }
}
