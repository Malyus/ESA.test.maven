package webdriver;

import net.sf.cglib.core.Local;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

/**
 * Created by M.Malyus on 5/18/2018.
 */
public class BrowserStackIntegration {
    private com.browserstack.local.Local l;
    public WebDriver driver;
    public void setUp() throws Exception {
        String username = "alex3545";
        String accessKey = "TMz8dvGPQwyJusf2opH5";
        String server = "hub-cloud.browserstack.com";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("os", "OS X");
        capabilities.setCapability("os_version", "Sierra");
        capabilities.setCapability("browser", "Safari");
        capabilities.setCapability("browser_version", "10.1");
        capabilities.setCapability("resolution", "1920x1080");
        capabilities.setCapability("browserstack.debug", "true");
        capabilities.setCapability("build", "First build");

        driver = new RemoteWebDriver(new URL("http://"+username+":"+accessKey+"@"+server+"/wd/hub"), capabilities);
    }
}
