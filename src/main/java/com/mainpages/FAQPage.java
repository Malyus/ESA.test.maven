package com.mainpages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class FAQPage extends Page {

    @FindBy(xpath = "//div[@id='faqaccordion']")
    private WebElement presentsOfFAQPage;

    @FindBy(xpath = "//*[@id='faqaccordion']/div[5]/div[1]/a/span[1]")
    private WebElement thirdQuestionButton;

    @FindBy(xpath = "//div[@id='answer4']")
    private WebElement thirdAnswer;

    @FindBy(xpath = "//*[@id='faqaccordion']/div[5]/div[1]/a/span[2]")
    private WebElement thirdQuestionArrow;

    @FindBy(xpath = "//*[@id='faqaccordion']/div[1]/div[1]/a/span[1]")
    private WebElement firstQuestionButton;

    @FindBy(xpath = "//*[@id='faqaccordion']/div[1]/div[1]/a/span[2]")
    private WebElement firstArrow;

    public FAQPage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public boolean checkPresentsOfFAQPage(){
        return presentsOfFAQPage.isDisplayed();
    }
    @Step
    public void turnBackByBrowserButton(){
        webDriver.navigate().back();
    }
    @Step
    public void clickOnThirdQuestion(){
        thirdQuestionButton.click();
    }
    @Step
    public String getActiveOfThirdAnswer(){
        return thirdAnswer.getAttribute("aria-expanded");
    }
    @Step
    public void clickOnFirstQuestionButton(){
        firstQuestionButton.click();
    }
    @Step
    public String getRotateValueOfThirdAnswer(){
        return thirdQuestionArrow.getCssValue("top");
    }
    @Step
    public String getRotateValueOfFirstAnswer(){
        return firstArrow.getCssValue("transform");
    }
}
