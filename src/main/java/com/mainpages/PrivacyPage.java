package com.mainpages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/29/2018.
 */
public class PrivacyPage extends Page {
    public PrivacyPage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public String getCurrentURL(){
        return webDriver.getCurrentUrl();
    }
    @Step
    public void turnBackByBrowser(){
        webDriver.navigate().back();
    }
}
