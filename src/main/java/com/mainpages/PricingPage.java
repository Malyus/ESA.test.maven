package com.mainpages;

import com.stepspages.PersonalInformationPage;
import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class PricingPage extends Page {

    @FindBy(xpath = "//div[@class='swiper-slide bg-white yellow-plan swiper-slide-active']//h3//a")
    private WebElement esaHousingLetterButton;

    @FindBy(xpath = "//div[@class='swiper-slide bg-white blue-plan swiper-slide-next']//h3//a")
    private WebElement comboPlanButton;

    @FindBy(xpath = "//div[@class='swiper-slide bg-white green-plan']//h3//a")
    private WebElement esaTravelLetterButton;

    @FindAll(@FindBy(xpath = "//div[@class='footer-menu']//ul[@class='nav navbar-nav']//li//a"))
    private List<WebElement> listOfBottomItems;

    @FindBy(xpath = "//div[@class='nav-list']//li[1]")
    private WebElement pricingButtonTopMenu;

    @FindBy(xpath = "//div[@class='nav-list']//li[2]")
    private WebElement faqButtonOfTopMenu;

    @FindBy(xpath = "//div[@class='nav-list']//li[3]")
    private WebElement blogButtonOfTopMenu;

    @FindBy(xpath = "//div[@class='nav-list']//li[4]")
    private WebElement contactUsButtonOfTopMenu;

    @FindBy(xpath = "//div[@class='footer-menu']//ul//li[5]")
    private WebElement termsButtonOfBottomMenu;

    @FindBy(xpath = "//div[@class='footer-menu']//ul//li[6]")
    private WebElement privacyButtonOfBottomMenu;

    @FindAll(@FindBy(xpath = "//div[@class='swiper-slide bg-white yellow-plan swiper-slide-active']//li//span"))
    private List<WebElement> esaHousingLetterServicesList;

    @FindAll(@FindBy(xpath = "//div[@class='swiper-slide bg-white blue-plan swiper-slide-next']//li//span"))
    private List<WebElement> comboPlanServicesList;

    @FindAll(@FindBy(xpath = "//div[@class='swiper-slide bg-white green-plan']//li//span"))
    private List<WebElement> esaTravelLetterServicesList;

    @FindAll(@FindBy(xpath = "//div[@class='swiper-slide bg-white yellow-plan swiper-slide-active']//ul[@class='check-list']//li//span"))
    private List<WebElement> includedServicesOfEsaHousingLetter;

    @FindAll(@FindBy(xpath = "//div[@class='swiper-slide bg-white blue-plan swiper-slide-next']//ul[@class='check-list']//li//span"))
    private List<WebElement> includedServicesOfComboPlan;

    @FindAll(@FindBy(xpath = "//div[@class='swiper-slide bg-white green-plan']//ul[@class='check-list']//li//span"))
    private List<WebElement> includedServicesOfEsaTravelLetter;

    @FindAll(@FindBy(xpath = "//div[@class='price-block']"))
    private List<WebElement> listOfPrices;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']//li[2]")
    private WebElement faqBottomButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']//li[3]")
    private WebElement blogBottomButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']//li[4]")
    private WebElement contactUsBottomButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']//li[5]")
    private WebElement termsBottomButton;

    @FindBy(xpath = "//ul[@class='nav navbar-nav']//li[6]")
    private WebElement privacyBottomButton;

    @FindAll(@FindBy(xpath = "//div[@class='container info-block why']//div[1]//ul//li//span"))
    private List<WebElement> whyChooseEsaLeftList;

    @FindAll(@FindBy(xpath = "//div[@class='container info-block why']//div[2]//ul//li//span"))
    private List<WebElement> whyChooseEsaRightList;

    public PricingPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step
    public PersonalInformationPage clickOnEsaHousingLetterButton(){
        esaHousingLetterButton.click();
        return PageFactory.initElements(webDriver,PersonalInformationPage.class);
    }
    @Step
    public PersonalInformationPage clickOnComboPlanButton(){
        comboPlanButton.click();
        return PageFactory.initElements(webDriver,PersonalInformationPage.class);
    }
    @Step
    public PersonalInformationPage clickOnEsaTravelLetterButton(){
        esaTravelLetterButton.click();
        return PageFactory.initElements(webDriver,PersonalInformationPage.class);
    }
    @Step
    public String getItemOfBottom(int index){
        return listOfBottomItems.get(index).getText();
    }
    @Step
    public PricingPage clickOnPricingButtonOfTopMenu(){
        pricingButtonTopMenu.click();
        return PageFactory.initElements(webDriver,PricingPage.class);
    }
    @Step
    public boolean checkPresentsOfPricingPage(){
        return comboPlanButton.isDisplayed();
    }
    @Step
    public FAQPage clickOnFaqButtonOfTopMenu(){
        faqButtonOfTopMenu.click();
        return PageFactory.initElements(webDriver,FAQPage.class);
    }
    @Step
    public BlogPage clickOnBlogButtonOfTopMenu(){
        blogButtonOfTopMenu.click();
        return PageFactory.initElements(webDriver,BlogPage.class);
    }
    @Step
    public ContactUsPage clickOnContactUsButtonOfTopMenu(){
        contactUsButtonOfTopMenu.click();
        return PageFactory.initElements(webDriver,ContactUsPage.class);
    }
    @Step
    public TermsPage clickOnTermsButtonOfBottomMenu(){
        termsButtonOfBottomMenu.click();
        return PageFactory.initElements(webDriver,TermsPage.class);
    }
    @Step
    public PrivacyPage clickOnPrivacyButtonOfBottomMenu(){
        privacyButtonOfBottomMenu.click();
        return PageFactory.initElements(webDriver,PrivacyPage.class);
    }
    @Step
    public String getEsaHousingLetterService(int index){
        return esaHousingLetterServicesList.get(index).getText();
    }
    @Step
    public String getComboPlanServices(int index){
        return comboPlanServicesList.get(index).getText();
    }
    @Step
    public String getEsaTravelLetterServices(int index){
        return esaTravelLetterServicesList.get(index).getText();
    }
    @Step
    public String getIncludedServicesOfEsaHousingLetter(int index){
        return includedServicesOfEsaHousingLetter.get(index).getText();
    }
    @Step
    public String getIncludedServicesOfComboPlan(int index){
        return includedServicesOfComboPlan.get(index).getText();
    }
    @Step
    public String getIncludedServicesOfEsaTravelLetter(int index){
        return includedServicesOfEsaTravelLetter.get(index).getText();
    }
    @Step
    public String getPriceOfCertificate(int index){
        return listOfPrices.get(index).getText();
    }
    @Step
    public FAQPage clickOnFAQBottomButton(){
        faqBottomButton.click();
        return PageFactory.initElements(webDriver,FAQPage.class);
    }
    @Step
    public BlogPage clickOnBlogBottomButton(){
        blogBottomButton.click();
        return PageFactory.initElements(webDriver,BlogPage.class);
    }
    @Step
    public ContactUsPage clickOnContactUsBottomButton(){
        contactUsBottomButton.click();
        return PageFactory.initElements(webDriver,ContactUsPage.class);
    }
    @Step
    public TermsPage clickOnTermsBottomButton(){
        termsBottomButton.click();
        return PageFactory.initElements(webDriver,TermsPage.class);
    }
    @Step
    public PrivacyPage clickOnPrivacyBottomButton(){
        privacyBottomButton.click();
        return PageFactory.initElements(webDriver,PrivacyPage.class);
    }
    @Step
    public String getLeftTextOfWhyChooseGetEsa(int index){
        return whyChooseEsaLeftList.get(index).getText();
    }
    @Step
    public String getRightTextOfWhyChooseGetEsa(int index){
        return whyChooseEsaRightList.get(index).getText();
    }
}
