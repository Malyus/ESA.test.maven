package com.mainpages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/31/2018.
 */
public class ProfilePage extends Page {

    @FindBy(xpath = "//div[@class='nav-list']//li[1]")
    private WebElement pricingButtonTopMenu;

    public ProfilePage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public PricingPage clickOnPricingButtonTopMenu(){
        pricingButtonTopMenu.click();
        return PageFactory.initElements(webDriver,PricingPage.class);
    }
}
