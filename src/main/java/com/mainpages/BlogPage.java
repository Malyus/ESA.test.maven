package com.mainpages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class BlogPage extends Page {

    @FindBy(xpath = "//aside[@class='col-sm-3 category-menu']")
    private WebElement presentsOfBlogPage;

    public BlogPage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public boolean checkPresentsOfBlogPage(){
        return presentsOfBlogPage.isDisplayed();
    }
    @Step
    public void turnBackByBrowserButton(){
        webDriver.navigate().back();
    }
}
