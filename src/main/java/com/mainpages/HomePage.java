package com.mainpages;

import com.popuppages.SignInPage;
import com.webdriver.Page;
import com.webdriver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class HomePage extends Page {
    private WebDriver driver = WebDriverFactory.getSetDriver();
    private final Wait wait = new WebDriverWait(driver,5,1000);

    @FindBy(xpath = "//div[@class='nav-list']//li[1]")
    private WebElement certificationPricingButton;

    @FindBy(xpath = "//div[@class='nav-list']//li[2]")
    private WebElement faqButton;

    @FindBy(xpath = "//div[@class='nav-list']//li[3]")
    private WebElement blogButton;

    @FindBy(xpath = "//div[@class='nav-list']//li[4]")
    private WebElement contactUsButton;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-singin']")
    private WebElement signInButton;

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public PricingPage clickOnCertificationPricingButton(){
        certificationPricingButton.click();
        return PageFactory.initElements(webDriver,PricingPage.class);
    }
    @Step
    public FAQPage clickOnFaqButton(){
        faqButton.click();
        return PageFactory.initElements(webDriver,FAQPage.class);
    }
    @Step
    public BlogPage clickOnBlogButton(){
        blogButton.click();
        return PageFactory.initElements(webDriver,BlogPage.class);
    }
    @Step
    public ContactUsPage clickOnContactUsButton(){
        contactUsButton.click();
        return PageFactory.initElements(webDriver,ContactUsPage.class);
    }
    @Step
    public SignInPage clickOnSignInButton(){
        signInButton.click();
        return PageFactory.initElements(webDriver,SignInPage.class);
    }
}
