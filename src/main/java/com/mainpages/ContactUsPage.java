package com.mainpages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class ContactUsPage extends Page {

    @FindBy(xpath = "//div[@class='col-sm-6']//div[@class='contact-info'][1]")
    private WebElement presentsOfContactUsPage;

    public ContactUsPage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public boolean checkPresentsOfContactUsPage(){
        return presentsOfContactUsPage.isDisplayed();
    }
    @Step
    public void turnBackToPreviousPage(){
        webDriver.navigate().back();
    }
}
