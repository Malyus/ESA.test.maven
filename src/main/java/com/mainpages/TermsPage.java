package com.mainpages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 1/29/2018.
 */
public class TermsPage extends Page {

    public TermsPage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public String getURLOfTermsPage(){
        return webDriver.getCurrentUrl();
    }
    @Step
    public void turnBackByBrowserButton(){
        webDriver.navigate().back();
    }
}
