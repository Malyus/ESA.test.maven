package com.stepsbo;

import com.questions.QuestionsPage;
import com.stepspages.PetSectionPage;
import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by M.Malyus on 2/6/2018.
 */
public class QuestionsBO extends Page {
    PersonalInformationBO personalInformationBO = PageFactory.initElements(webDriver,PersonalInformationBO.class);
    QuestionsPage questionsPage = PageFactory.initElements(webDriver,QuestionsPage.class);

    public QuestionsBO(WebDriver webDriver) {
        super(webDriver);
    }
    public PetSectionPage goToPetSectionPage(){
        personalInformationBO.fillNotAllFields("test","test");
        questionsPage.analyzeTypeOfQuestions();
        return PageFactory.initElements(webDriver,PetSectionPage.class);
    }
}
