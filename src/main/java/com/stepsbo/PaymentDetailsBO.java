package com.stepsbo;

import com.constants.ShippingAddressConstants;
import com.stepspages.PaymentDetailsPage;
import com.stepspages.ShippingAddressPage;
import com.webdriver.Page;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

/**
 * Created by M.Malyus on 3/26/2018.
 */
public class PaymentDetailsBO extends Page {
    private ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();
    private String street = shippingAddressConstants.getStreet();
    private String city = shippingAddressConstants.getCity();
    private String state = shippingAddressConstants.getState();
    private String zip = shippingAddressConstants.getZip();

    public PaymentDetailsPage goToPaymentDetailsPage(){
        PetSectionBO petSectionBO = PageFactory.initElements(webDriver,PetSectionBO.class);
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        ShippingBO shippingBO = PageFactory.initElements(webDriver,ShippingBO.class);
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnModalContinueButton();
        return PageFactory.initElements(webDriver,PaymentDetailsPage.class);
    }

    public PaymentDetailsBO(WebDriver webDriver) throws IOException, InvalidFormatException {
        super(webDriver);
    }
}
