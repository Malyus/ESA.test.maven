package com.stepsbo;

import com.questions.QuestionsPage;
import com.stepspages.AdditionalServicesPage;
import com.stepspages.PetSectionPage;
import com.stepspages.ShippingAddressPage;
import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by M.Malyus on 2/6/2018.
 */
public class PetSectionBO extends Page {
    protected QuestionsBO questionsBO = PageFactory.initElements(webDriver,QuestionsBO.class);
    protected PetSectionPage petSectionPage;


    public PetSectionBO(WebDriver webDriver) {
        super(webDriver);
    }
    public void fillNotAllFieldsOnePet(String petsBreed,String petsName){
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed(petsBreed);
        petSectionPage.writePetsName(petsName);
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
    }
    public void fillNotAllFieldsTwoPet(String firstBreed,String firstName,String secondName,String secondBreed){
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed(firstBreed);
        petSectionPage.writePetsName(firstName);
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.writeSecondPetsBreed(secondBreed);
        petSectionPage.writeSecondPetsName(secondBreed);
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.clickOnSecondIdCardForPet();
        petSectionPage.uploadSecondPhoto();
        petSectionPage.clickOnSecondCropButton();
    }
    public void fillNotAllFieldsThreePets(String thirdBreed,String thirdName){
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnThreePetsButton();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.writeSecondPetsBreed("cat");
        petSectionPage.writeSecondPetsName("elmo");
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.clickOnSecondIdCardForPet();
        petSectionPage.uploadSecondPhoto();
        petSectionPage.clickOnSecondCropButton();
        petSectionPage.selectThirdPetsType(1);
        petSectionPage.writeThirdPetsBreed(thirdBreed);
        petSectionPage.writeThirdPetsName(thirdName);
        petSectionPage.selectThirdPetsWeight(1);
        petSectionPage.clickOnThirdIdCardForPet();
        petSectionPage.uploadThirdPhoto();
    }
    public void fillSomeFieldsOfAllThreePetsSections(String first,String firstName,String second,String secondName,String third,String thirdName){
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnThreePetsButton();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed(first);
        petSectionPage.writePetsName(firstName);
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.writeSecondPetsBreed(second);
        petSectionPage.writeSecondPetsName(secondName);
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.clickOnSecondIdCardForPet();
        petSectionPage.uploadSecondPhoto();
        petSectionPage.clickOnSecondCropButton();
        petSectionPage.selectThirdPetsType(1);
        petSectionPage.writeThirdPetsBreed(third);
        petSectionPage.writeThirdPetsName(thirdName);
        petSectionPage.selectThirdPetsWeight(1);
        petSectionPage.clickOnThirdIdCardForPet();
        petSectionPage.uploadThirdPhoto();
    }
    public AdditionalServicesPage goToAdditionalServicesPage(){
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        return petSectionPage.goToAdditionalServicesPage();
    }
    public ShippingAddressPage goToShippingPage(){
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        AdditionalServicesPage additionalServicesPage = petSectionPage.goToAdditionalServicesPage();
        return additionalServicesPage.clickOnContinueButton();
    }
}
