package com.stepsbo;

import com.mainbo.SignInBO;
import com.mainpages.HomePage;
import com.mainpages.PricingPage;
import com.stepspages.PersonalInformationPage;
import com.util.UserRandomGenerator;
import com.webdriver.Page;
import com.webdriver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class PersonalInformationBO extends Page {
    private WebDriver driver = WebDriverFactory.getSetDriver();
    HomePage homePage = PageFactory.initElements(driver,HomePage.class);
    PricingPage pricingPage = homePage.clickOnCertificationPricingButton();
    SignInBO signInBO = PageFactory.initElements(driver,SignInBO.class);
    String email = UserRandomGenerator.generateRandomUser() + "@gmail.com";
    String password = UserRandomGenerator.generateRandomUser();

    public PersonalInformationBO(WebDriver webDriver) {
        super(webDriver);
    }
    public void goToPersonalInformationPage(){
        PricingPage pricingPage = signInBO.goToPricingPageAndSignIn(email,password);
        PersonalInformationPage personalInformationPage = pricingPage.clickOnComboPlanButton();
        System.out.println("Email and Password:" + email + ";" +  password);
    }
    public void fillNotAllFields(String firstName, String lastName){
        PricingPage pricingPage = signInBO.goToPricingPageAndSignIn(email,password);
        System.out.println("Email and Password:" + email + ";" +  password);
        PersonalInformationPage personalInformationPage = pricingPage.clickOnComboPlanButton();
        personalInformationPage.selectFirstMonth();
        personalInformationPage.selectFirstDay();
        personalInformationPage.selectFirstYear();
        personalInformationPage.clickOnFemaleCheckBox();
        personalInformationPage.selectMarriedStatus();
        personalInformationPage.writeIntoFirstNameField(firstName);
        personalInformationPage.writeIntoLastNameField(lastName);
        personalInformationPage.clickOnNotValidContinue();
    }
}
