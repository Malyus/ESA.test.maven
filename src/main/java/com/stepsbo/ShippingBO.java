package com.stepsbo;

import com.stepspages.AdditionalServicesPage;
import com.stepspages.ShippingAddressPage;
import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by M.Malyus on 2/21/2018.
 */
public class ShippingBO extends Page {
    protected ShippingAddressPage shippingAddressPage = PageFactory.initElements(webDriver,ShippingAddressPage.class);

    public ShippingBO(WebDriver webDriver) {
        super(webDriver);
    }
    public void writeShippingAddressValues(String street,String city,String state,String zip){
        shippingAddressPage.writeStreetAddress(street);
        shippingAddressPage.writeCityValue(city);
        shippingAddressPage.writeStateValue(state);
        shippingAddressPage.writeZipCode(zip);
    }

}
