package com.stepsbo;

import com.stepspages.PaymentDetailsPage;
import com.webdriver.WebDriverFactory;

/**
 * Created by M.Malyus on 3/27/2018.
 */
public class PlaceBO {
    PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage(WebDriverFactory.getSetDriver());

    public void writeFields(String creditCardNumber,String date,String cvv){
        paymentDetailsPage.writeCardNumber(creditCardNumber);
        paymentDetailsPage.writeExpDate(date);
        paymentDetailsPage.writeCvvCode(cvv);
    }
}
