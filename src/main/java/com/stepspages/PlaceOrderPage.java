package com.stepspages;

import com.webdriver.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 3/26/2018.
 */
public class PlaceOrderPage extends Page {
    private final Wait wait = new WebDriverWait(webDriver,5,1000);

    @FindBy(xpath = "//div[@style='width:100%']")
    private WebElement progressBar;

    @FindBy(xpath = "//label[@id='fullname']")
    private WebElement fullNameValue;

    @FindBy(xpath = "//label[@id='street']")
    private WebElement streetValue;

    @FindBy(xpath = "//label[@id='city']")
    private WebElement cityValue;

    @FindBy(xpath = "//label[@id='state']")
    private WebElement stateValue;

    @FindBy(xpath = "//label[@id='zip']")
    private WebElement zipValue;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButton;

    public PlaceOrderPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step
    public boolean checkPresentsOfPlaceOrderPage(){
        return progressBar.isDisplayed();
    }
    @Step
    public String getFullNameValue(){
        return fullNameValue.getText();
    }
    @Step
    public String getStreetValue(){
        return streetValue.getText();
    }
    @Step
    public String getCityValue(){
        return cityValue.getText();
    }
    @Step
    public String getStateValue(){
        return stateValue.getText();
    }
    @Step
    public String getZipValue(){
        return zipValue.getText();
    }
    @Step
    public void clickOnBackButton(){
        backButton.click();
    }
    @Step
    public void clickOnContinueButton(){
        continueButton.click();
    }
    @Step
    public void waitForPageValuesInitialization() throws InterruptedException {
        waitForTextPresent(cityValue,"LOS ANGELES");
    }
}
