package com.stepspages;

import com.webdriver.Page;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.util.List;

/**
 * Created by M.Malyus on 2/6/2018.
 */
public class PetSectionPage extends Page {
    private final Wait wait = new WebDriverWait(webDriver,5,1000);

    @FindBy(xpath = "//div[@class='progress-bar']")
    private WebElement progressBar;

    @FindBy(xpath = "//div[@id='pet1type']")
    private WebElement petsTypeOpenButton;

    @FindAll(@FindBy(xpath = "//div[@id='pet1type']//ul//li"))
    private List<WebElement> listOfPetsType;

    @FindBy(xpath = "//input[@id='pet1breed']")
    private WebElement petsBreedInput;

    @FindBy(xpath = "//input[@id='pet1name']")
    private WebElement petsNameInput;

    @FindBy(xpath = "//div[@id='pet1weight']")
    private WebElement petsWeightOpenButton;

    @FindAll(@FindBy(xpath = "//div[@id='pet1weight']//ul//li"))
    private List<WebElement> listOfPetsWeight;

    @FindBy(xpath = "//input[@id='pet1idcheck']")
    private WebElement idCardForPetButton;

    @FindBy(xpath = "//input[@id='pet1upload']")
    private WebElement uploadPhotoInput;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//div[@style='width:56%']")
    private WebElement presentsOfNextStep;

    @FindAll(@FindBy(xpath = "//button[@class='btn-crop vanilla-result']"))
    private List<WebElement> cropButton;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButtonOfNextStep;

    @FindBy(xpath = "//img[@class='result-img']")
    private WebElement croppedPhoto;

    @FindBy(xpath = "//input[@id='radio02']")
    private WebElement twoPetsButton;

    @FindBy(xpath = "//div[@id='pet2type']")
    private WebElement secondPetsTypeOpenButton;

    @FindAll(@FindBy(xpath = "//div[@id='pet2type']//ul//li"))
    private List<WebElement> secondListOfPetsType;

    @FindBy(xpath = "//input[@id='pet2name']")
    private WebElement secondPetsNameInput;

    @FindBy(xpath = "//input[@id='pet2breed']")
    private WebElement secondPetsBreedInput;

    @FindBy(xpath = "//div[@id='pet2weight']")
    private WebElement secondPetsWeightOpenButton;

    @FindAll(@FindBy(xpath = "//div[@id='pet2weight']//ul//li"))
    private List<WebElement> secondListOfPetsWeight;

    @FindBy(xpath = "//input[@id='pet2upload']")
    private WebElement secondUploadPhotoInput;

    @FindBy(xpath = "//input[@id='radio03']")
    private WebElement threePetsOpenButton;

    @FindBy(xpath = "//div[@id='pet3type']")
    private WebElement thirdPetsTypeOpenButton;

    @FindAll(@FindBy(xpath = "//div[@id='pet3type']//ul//li"))
    private List<WebElement> thirdListOfPetsType;

    @FindBy(xpath = "//input[@id='pet3breed']")
    private WebElement thirdPetsBreedInput;

    @FindBy(xpath = "//input[@id='pet3name']")
    private WebElement thirdPetsNameInput;

    @FindBy(xpath = "//div[@id='pet3weight']")
    private WebElement thirdPetsWeightOpenButton;

    @FindAll(@FindBy(xpath = "//div[@id='pet3weight']//ul//li"))
    private List<WebElement> thirdListOfPetsWeight;

    @FindBy(xpath = "//input[@id='pet2idcheck']")
    private WebElement secondIdCardForPetButton;

    @FindBy(xpath = "//input[@id='pet3idcheck']")
    private WebElement thirdIdCardForPetButton;

    @FindBy(xpath = "//input[@id='pet3upload']")
    private WebElement thirdPhotoUploadInput;

    @FindBy(xpath = "//input[@id='radio01']")
    private WebElement onePetButton;

    @FindBy(xpath = "//input[@class='ng-valid ng-not-empty ng-dirty ng-touched ng-valid-parse' and @id='radio01']")
    private WebElement activeOnePetButton;

    @FindAll(@FindBy(xpath = "//input[@id='pet2name']"))
    private List<WebElement> presentsOfTwoPetsSection;

    @FindAll(@FindBy(xpath = "//input[@id='pet3name']"))
    private List<WebElement> presentsOfThreePetsSection;

    @FindBy(xpath = "//input[@id='pet1name']")
    private WebElement presentsOfOnePetSection;

    @FindAll(@FindBy(xpath = "//button[@class='btn-crop vanilla-result']"))
    private List<WebElement> firstCropButton;

    @FindBy(xpath = "//div[@id='pet1type']//span")
    private WebElement selectedPetsType;

    @FindBy(xpath = "//div[@id='pet1weight']//span")
    private WebElement selectedPetsWeight;

    @FindBy(xpath = "//label[@for='imgcrop']")
    private WebElement uploadPhotoButtonText;

    public PetSectionPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step
    public String checkPresentsOfPetSection(){
        return progressBar.getAttribute("style");
    }
    @Step
    public void clickOnPetsTypeOpenButton(){
        petsTypeOpenButton.click();
    }
    @Step
    public void selectPetsType(int index){
        petsTypeOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(listOfPetsType.get(index)));
        listOfPetsType.get(index).click();
    }
    @Step
    public void writePetsBreed(String petsBreed){
        petsBreedInput.sendKeys(petsBreed);
    }
    @Step
    public void writePetsName(String petsName){
        petsNameInput.sendKeys(petsName);
    }
    @Step
    public void clickOnPetsWeightOpenButton(){
        petsWeightOpenButton.click();
    }
    @Step
    public void selectPetsWeight(int index){
        petsWeightOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(listOfPetsWeight.get(index)));
        listOfPetsWeight.get(index).click();
    }
    @Step
    public void clickOnIDCardForPet(){
        idCardForPetButton.click();
    }
    @Step
    public void uploadPhoto(){
        uploadPhotoInput.sendKeys((new File("./1.jpg").getAbsolutePath()));
    }
    @Step
    public void clickOnBackButton(){
        backButton.click();
    }
    @Step
    public void clickOnContinueButton(){
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();", continueButton);
    }
    @Step
    public void clickOnCropButton(){
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click",cropButton.get(0));
        cropButton.get(0).click();
    }
    @Step
    public String checkPresentsOfNextStep(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfNextStep));
        return presentsOfNextStep.getAttribute("style");
    }
    @Step
    public void clickOnBackButtonOfNextStep(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfNextStep));
        backButtonOfNextStep.click();
    }
    @Step
    public String getPetsBreedValue(){
        return petsBreedInput.getAttribute("value");
    }
    @Step
    public String getPetsNameValue(){
        return petsNameInput.getAttribute("value");
    }
    @Step
    public void uploadJpeg(){
        uploadPhotoInput.sendKeys((new File("./2.0.jpeg").getAbsolutePath()));
    }
    @Step
    public boolean checkPresentsOfCroppedPhoto(){
        return croppedPhoto.isDisplayed();
    }
    @Step
    public void clearPetsBreedInput(){
        petsBreedInput.clear();
    }
    @Step
    public void clearPetsNameInput(){
        petsNameInput.clear();
    }
    @Step
    public void clickOnTwoPetsButton(){
        wait.until(ExpectedConditions.visibilityOf(petsNameInput));
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();", twoPetsButton);
    }
    @Step
    public void clickOnSecondPetsTypeOpenButton(){
        secondPetsTypeOpenButton.click();
    }
    @Step
    public void selectSecondTypeOfPet(int index){
        secondPetsTypeOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(secondListOfPetsType.get(index)));
        secondListOfPetsType.get(index).click();
    }
    @Step
    public void writeSecondPetsName(String petsName){
        secondPetsNameInput.clear();
        secondPetsNameInput.sendKeys(petsName);
    }
    @Step
    public void writeSecondPetsBreed(String petsBreed){
        secondPetsBreedInput.clear();
        secondPetsBreedInput.sendKeys(petsBreed);
    }
    @Step
    public void clickOnSecondPetsWeightOpenButton(){
        secondPetsWeightOpenButton.click();
    }
    @Step
    public void selectSecondPetsWeight(int index){
        secondPetsWeightOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(secondListOfPetsWeight.get(index)));
        secondListOfPetsWeight.get(index).click();
    }
    @Step
    public void uploadSecondPhoto(){
        secondUploadPhotoInput.sendKeys((new File("./1.jpg").getAbsolutePath()));
    }
    @Step
    public void clickOnSecondCropButton(){
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click",cropButton.get(1));
    }
    @Step
    public String getValueOfSecondPetsName(){
        return secondPetsNameInput.getAttribute("value");
    }
    @Step
    public String getValueOfSecondPetsBreed(){
        return secondPetsBreedInput.getAttribute("value");
    }
    @Step
    public void clickOnThreePetsButton(){
        wait.until(ExpectedConditions.visibilityOf(petsNameInput));
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();", threePetsOpenButton);
    }
    @Step
    public void clickOnThirdPetsTypeOpenButton(){
        thirdPetsTypeOpenButton.click();
    }
    @Step
    public void selectThirdPetsType(int index){
        thirdPetsTypeOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(thirdListOfPetsType.get(index)));
        thirdListOfPetsType.get(index).click();
    }
    @Step
    public void writeThirdPetsBreed(String petsBreed){
        thirdPetsBreedInput.clear();
        thirdPetsBreedInput.sendKeys(petsBreed);
    }
    @Step
    public void writeThirdPetsName(String petsName){
        thirdPetsNameInput.clear();
        thirdPetsNameInput.sendKeys(petsName);
    }
    @Step
    public void clickOnThirdPetsWeightOpenButton(){
        thirdPetsWeightOpenButton.click();
    }
    @Step
    public void selectThirdPetsWeight(int index){
        thirdPetsWeightOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(thirdListOfPetsWeight.get(index)));
        thirdListOfPetsWeight.get(index).click();
    }
    @Step
    public void clickOnThirdIdCardForPet(){
        thirdIdCardForPetButton.click();
    }
    @Step
    public void uploadThirdPhoto(){
        thirdPhotoUploadInput.sendKeys((new File("./2.0.jpeg").getAbsolutePath()));
    }
    @Step
    public void clickOnSecondIdCardForPet(){
        secondIdCardForPetButton.click();
    }
    @Step
    public String getThirdPetsBreedValue(){
        return thirdPetsBreedInput.getAttribute("value");
    }
    @Step
    public String getThirdPetsNameValue(){
        return thirdPetsNameInput.getAttribute("value");
    }
    @Step
    public String getActiveOfOnePetButton() {
        return onePetButton.getAttribute("class");
    }
    @Step
    public void clickOnOnePetButton(){
        onePetButton.click();
    }
    @Step
    public String getActiveOfSecondPetButton(){
        return twoPetsButton.getAttribute("class");
    }
    @Step
    public String getActiveOfThreePetButton(){
        return threePetsOpenButton.getAttribute("class");
    }
    @Step
    public boolean checkIfTwoPetsSectionIsDisplayed(){
        return secondPetsNameInput.isDisplayed();
    }
    @Step
    public boolean checkIfTwoPetsSectionIsNotDisplayed(){
        boolean presents = false;
        if (presentsOfTwoPetsSection.size()==0){
            presents = true;
        }
        return presents;
    }
    @Step
    public boolean checkIfThreePetsSectionIsDisplayed(){
        return thirdPetsNameInput.isDisplayed();
    }
    @Step
    public boolean checkIfThreePetsSectionIsNotDisplayed(){
        boolean presents = false;
        if(presentsOfThreePetsSection.size() == 0){
            presents = true;
        }
        return presents;
    }
    @Step
    public boolean checkPresentsOfOnePetSection(){
        return presentsOfOnePetSection.isDisplayed();
    }
    @Step
    public void clickOnFirstCropButton(){
      firstCropButton.get(0).click();
    }
    @Step
    public String getNameOfSelectedPetsType(){
        return selectedPetsType.getText();
    }
    @Step
    public String getNameOfSelectedPetsWeight(){
        return selectedPetsWeight.getText();
    }
    @Step
    public String getTextOfUploadPhotoButton(){
        return uploadPhotoButtonText.getText();
    }
    @Step
    public String getDisabledValueOfContinueButton(){
        return continueButton.getAttribute("disabled");
    }
    @Step
    public AdditionalServicesPage goToAdditionalServicesPage(){
        continueButton.click();
        return PageFactory.initElements(webDriver,AdditionalServicesPage.class);
    }

}
