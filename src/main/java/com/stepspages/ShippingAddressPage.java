package com.stepspages;

import com.webdriver.Page;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

/**
 * Created by M.Malyus on 2/15/2018.
 */
public class ShippingAddressPage extends Page {
    private final Wait wait = new WebDriverWait(webDriver, 5, 1000);

    @FindBy(xpath = "//div[@style='width:70%']")
    private WebElement progressBarLevel;

    @FindBy(xpath = "//input[@name='address']")
    private WebElement streetAddressInput;

    @FindBy(xpath = "//input[@id='city']")
    private WebElement cityInput;

    @FindBy(xpath = "//input[@id='state']")
    private WebElement stateInput;

    @FindBy(xpath = "//input[@name='zipcode']")
    private WebElement zipCodeInput;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue btn-top']")
    private WebElement continueButton;

    @FindBy(xpath = "//div[@class='group row button-area']//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//div[@class='modal-content']")
    private WebElement modalWindow;

    @FindBy(xpath = "//div[@class='modal-content']//div[@class='modal-footer']//button[@class='btn btn-bordered btn-back']")
    private WebElement modalEditButton;

    @FindBy(xpath = "//div[@class='modal-content']//div[@class='modal-footer']//button[@class='btn btn-filled btn-continue']")
    private WebElement modalContinueButton;

    @FindBy(xpath = "//div[@class='modal-header']//button[@class='close']")
    private WebElement modalCloseButton;

    @FindAll(@FindBy(xpath = "//div[@class='form-inner ng-scope']//div//label"))
    private List<WebElement> listOfShippingElements;

    @FindAll(@FindBy(xpath = "//div[@class='modal-header']//button[@class='close']"))
    private List<WebElement> presentsOfConfirmShippingAddressPopUp;

    @FindBy(xpath = "//div[@ng-if='!error_address&&address']//p[@class]")
    private WebElement confirmShippingAddressField;

    @FindBy(xpath = "//div[@ng-if='!error_address&&city']//p[@class]")
    private WebElement confirmShippingCityField;

    @FindBy(xpath = "//div[@ng-if='!error_address&&state']//p[@class]")
    private WebElement confirmShippingStateField;

    @FindBy(xpath = "//div[@ng-if='!error_address&&zip']//p[@class]")
    private WebElement confirmShippingZipField;

    @FindBy(xpath = "//p[@ng-if='!error_address']")
    private WebElement confirmAddressErrorMessage;

    @FindBy(xpath = "//div[@style='width:56%']")
    private WebElement presentsOfPreviousPage;

    @FindBy(xpath = "//div[3]/button")
    private WebElement presentsOfModalWindow;

    @FindBy(xpath = "//p[@class='error ng-binding']")
    private WebElement errorMessageNotFound;

    public ShippingAddressPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step
    public boolean checkPresentsOfShippingAddressPage() {
        return progressBarLevel.getAttribute("style").equals("width: 70%;");
    }

    @Step
    public void writeStreetAddress(String streetAddress) {
        wait.until(ExpectedConditions.visibilityOf(streetAddressInput));
        streetAddressInput.sendKeys(streetAddress);
    }

    @Step
    public void writeCityValue(String city) {
        cityInput.sendKeys(city);
    }

    @Step
    public void writeStateValue(String state) {
        stateInput.sendKeys(state);
    }

    @Step
    public void writeZipCode(String zip) {
        zipCodeInput.sendKeys(zip);
    }

    @Step
    public void clickOnContinueButton() {
        wait.until(ExpectedConditions.visibilityOf(progressBarLevel));
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click();", continueButton);
    }

    @Step
    public AdditionalServicesPage clickOnBackButton() {
        wait.until(ExpectedConditions.visibilityOf(progressBarLevel));
        backButton.click();
        return PageFactory.initElements(webDriver, AdditionalServicesPage.class);
    }

    @Step
    public boolean checkPresentsOfModalWindow() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfModalWindow));
        return modalWindow.isDisplayed();
    }

    @Step
    public void clickOnEditButton() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfModalWindow));
        modalEditButton.click();
    }

    @Step
    public PaymentDetailsPage clickOnModalContinueButton() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfModalWindow));
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click()", modalContinueButton);
        return PageFactory.initElements(webDriver, PaymentDetailsPage.class);
    }

    @Step
    public void clickOnModalCloseButton() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfModalWindow));
        modalCloseButton.click();
    }

    @Step
    public String getNameOfShippingPageElements(int index) {
        return listOfShippingElements.get(index).getText();
    }

    @Step
    public void clearCityInput() {
        cityInput.clear();
    }

    @Step
    public void clearStateInput() {
        stateInput.clear();
    }

    @Step
    public void clearZipInput() {
        zipCodeInput.clear();
    }

    @Step
    public String getActiveOfContinueButton() {
        return continueButton.getAttribute("disabled");
    }

    @Step
    public String getValueOfStreetAddress() {
        return streetAddressInput.getAttribute("value");
    }

    @Step
    public String getValueOfCityInput() {
        return cityInput.getAttribute("value");
    }

    @Step
    public String getValueOfStateInput() {
        return stateInput.getAttribute("value");
    }

    @Step
    public String getValueOfZipInput() {
        return zipCodeInput.getAttribute("value");
    }

    @Step
    public int checkPresentsOfConfirmAddressPopUp() {
        return presentsOfConfirmShippingAddressPopUp.size();
    }

    @Step
    public boolean checkPresentsOfModalWindowWithoutWait() throws InterruptedException {
        Thread.sleep(2000);
        return modalWindow.isDisplayed();
    }

    @Step
    public String getActiveOfConfirmAddressContinueButton() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfConfirmShippingAddressPopUp.get(0)));
        return modalContinueButton.getAttribute("disabled");
    }

    @Step
    public String getValueOfConfirmShippingAddressField() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfConfirmShippingAddressPopUp.get(0)));
        return confirmShippingAddressField.getText();
    }

    @Step
    public String getValueOfConfirmShippingCityField() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfConfirmShippingAddressPopUp.get(0)));
        return confirmShippingCityField.getText();
    }

    @Step
    public String getValueOfConfirmShippingStateField() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfConfirmShippingAddressPopUp.get(0)));
        return confirmShippingStateField.getText();
    }

    @Step
    public String getValueOfConfirmShippingZipField() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfConfirmShippingAddressPopUp.get(0)));
        return confirmShippingZipField.getText();
    }

    @Step
    public void clearStreet() {
        streetAddressInput.click();
        streetAddressInput.clear();
    }

    @Step
    public void clearCity() {
        cityInput.click();
        cityInput.clear();
    }

    @Step
    public void clearState() {
        stateInput.click();
        stateInput.clear();
    }

    @Step
    public void clearZip() {
        zipCodeInput.click();
        zipCodeInput.clear();
    }

    @Step
    public String getErrorMessageText() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfModalWindow));
        return confirmAddressErrorMessage.getText();
    }

    @Step
    public boolean checkPresentsOfPreviousPage() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfPreviousPage));
        return presentsOfPreviousPage.isDisplayed();
    }

    @Step
    public void clearAndWriteNewCity(String city) {
        cityInput.click();
        int i = 0;
        while (i < 15) {
            cityInput.sendKeys(Keys.BACK_SPACE);
            i++;
        }
        cityInput.sendKeys(city);
    }

    @Step
    public void clearAndWriteStreetAddress(String street) {
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click();", streetAddressInput);
        int i = 0;
        while (i < 15) {
            streetAddressInput.sendKeys(Keys.BACK_SPACE);
            i++;
        }
        streetAddressInput.sendKeys(street);
    }

    @Step
    public void clearAndWriteState(String state) {
        stateInput.click();
        int i = 0;
        while (i < 5) {
            cityInput.sendKeys(Keys.BACK_SPACE);
            i++;
        }
        cityInput.sendKeys(state);
    }

    @Step
    public void clearAndWriteZip(String zip) {
        zipCodeInput.click();
        int i = 0;
        while (i < 15) {
            zipCodeInput.sendKeys(Keys.BACK_SPACE);
            i++;
        }
        zipCodeInput.sendKeys(zip);
    }

    @Step
    public String getSizeOfProgressBar() {
        return progressBarLevel.getAttribute("style");
    }

    @Step
    public String getErrorMessageNotFound() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfModalWindow));
        return errorMessageNotFound.getText();
    }

}
