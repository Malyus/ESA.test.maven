package com.stepspages;

import com.webdriver.Page;
import com.webdriver.WebDriverFactory;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class PersonalInformationPage extends Page {
    private WebDriver driver = WebDriverFactory.getSetDriver();
    private final Wait wait = new WebDriverWait(driver,5,1000);
    final static Logger LOG = Logger.getLogger(PersonalInformationPage.class);

    @FindBy(xpath = "//div[@class='progress-bar']")
    private WebElement progressBar;

    @FindBy(xpath = "//input[@id='fname']")
    private WebElement firstNameInput;

    @FindBy(xpath = "//input[@id='lname']")
    private WebElement lastNameInput;

    @FindBy(xpath = "//div[@id='birth-month']")
    private WebElement monthOpenButton;

    @FindBy(xpath = "//div[@id='birth-month']//ul[@class='dropdown']//li[1]")
    private WebElement firstMonthButton;

    @FindBy(xpath = "//div[@id='birth-day']")
    private WebElement dayOpenButton;

    @FindBy(xpath = "//div[@id='birth-day']//ul//li[1]")
    private WebElement firstDayButton;

    @FindBy(xpath = "//div[@id='birth-month']//span")
    private WebElement selectedMonth;

    @FindBy(xpath = "//div[@id='birth-day']//span")
    private WebElement selectedDay;

    @FindBy(xpath = "//div[@id='birth-year']//span")
    private WebElement selectedYear;

    @FindBy(xpath = "//div[@id='maritalstatus']//span")
    private WebElement selectedMaritalStatus;

    @FindBy(xpath = "//div[@id='birth-year']")
    private WebElement yearOpenButton;

    @FindBy(xpath = "//div[@id='birth-year']//ul//li[1]")
    private WebElement firstYearButton;

    @FindBy(xpath = "//input[@value='male']")
    private WebElement maleCheckBox;

    @FindBy(xpath = "//input[@value='female']")
    private WebElement femaleCheckBox;

    @FindBy(xpath = "//div[@id='maritalstatus']")
    private WebElement maritalStatusOpenButton;

    @FindBy(xpath = "//div[@id='maritalstatus']//ul//li[1]")
    private WebElement marriedStatusButton;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButtonNotValid;

    @FindAll(@FindBy(xpath = "//div[@id='birth-month']//ul[@class='dropdown']//li"))
    private List<WebElement> listOfMonths;

    @FindAll(@FindBy(xpath = "//div[@id='birth-day']//ul//li"))
    private List<WebElement> listOfDays;

    @FindAll(@FindBy(xpath = "//div[@id='birth-year']//ul//li//a"))
    private List<WebElement> listOfYears;

    @FindAll(@FindBy(xpath = "//div[@id='maritalstatus']//ul//li//a"))
    private List<WebElement> listOfMaritalStatuses;

    @FindBy(xpath = "//div[@id='birth-day']//ul//li[1]//a")
    private WebElement firstDayValue;

    @FindBy(xpath = "//div[@id='birth-month']//ul//li[1]//a")
    private WebElement firstMonthValue;

    @FindBy(xpath = "//div[@id='birth-month']//ul//li[12]//a")
    private WebElement lastMonthValue;

    @FindBy(xpath = "//div[@id='birth-day']//ul//li[31]//a")
    private WebElement lastDayValue;

    @FindBy(xpath = "//div[@id='birth-year']//ul//li[1]//a")
    private WebElement firstYearValue;

    @FindBy(xpath = "//div[@id='birth-year']//ul//li[74]//a")
    private WebElement lastYearValue;
                                   /*ng-pristine ng-valid ng-not-empty ng-touched*/
    @FindBy(xpath = "//input[@class='ng-pristine ng-valid ng-not-empty ng-touched' and @value='female']")
    private WebElement femaleActiveButton;

    @FindBy(xpath = "//input[@value='male']")
    private WebElement maleActiveButton;

    @FindBy(xpath = "//input[@value='female']")
    private WebElement femaleButtonAfterActivating;

    @FindAll(@FindBy(xpath = "//input[@class='ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched']"))
    private List<WebElement> activeOfGenderButtons;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//div[@class='swiper-slide bg-white blue-plan swiper-slide-next']//h3//a")
    private WebElement presentsOfPreviousPage;

    public PersonalInformationPage(WebDriver webDriver) {
        super(webDriver);
    }
    @Step
    public String checkPresentsOfPersonalInformationScreen(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return progressBar.getAttribute("style");
    }
    @Step
    public void writeIntoFirstNameField(String firstName){
        firstNameInput.sendKeys(firstName);
    }
    @Step
    public boolean checkPresentsOfFirstNameField(){
        wait.until(ExpectedConditions.visibilityOf(firstNameInput));
        return firstNameInput.isDisplayed();
    }
    @Step
    public void writeIntoLastNameField(String lastName){
        lastNameInput.sendKeys(lastName);
    }
    @Step
    public boolean checkPresentsOfLastNameField(){
        wait.until(ExpectedConditions.visibilityOf(lastNameInput));
        return lastNameInput.isDisplayed();
    }
    @Step
    public void selectFirstMonth(){
        monthOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(firstMonthButton));
        firstMonthButton.click();
    }
    @Step
    public void selectFirstDay(){
        dayOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(firstDayButton));
        firstDayButton.click();
    }
    @Step
    public void selectFirstYear(){
        yearOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(firstYearButton));
        firstYearButton.click();
    }
    @Step
    public void clickOnMaleCheckBox(){
        maleCheckBox.click();
    }
    @Step
    public void clickOnFemaleCheckBox(){
        femaleCheckBox.click();
    }
    @Step
    public void selectMarriedStatus(){
        maritalStatusOpenButton.click();
        wait.until(ExpectedConditions.visibilityOf(marriedStatusButton));
        marriedStatusButton.click();
    }
    @Step
    public void clickOnNotValidContinue(){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", continueButtonNotValid);
    }
    @Step
    public String getSelectedMonth(){
        wait.until(ExpectedConditions.textToBePresentInElement(selectedMonth,"Jan"));
        return selectedMonth.getText();
    }
    @Step
    public String getSelectedDay(){
        wait.until(ExpectedConditions.textToBePresentInElement(selectedDay,"1"));
        return selectedDay.getText();
    }
    @Step
    public String getSelectedYear(){
        wait.until(ExpectedConditions.textToBePresentInElement(selectedYear,"1928"));
        return selectedYear.getText();
    }
    @Step
    public String getMaritalStatus(){
        wait.until(ExpectedConditions.textToBePresentInElement(selectedMaritalStatus,"Married"));
        return selectedMaritalStatus.getText();
    }
    @Step
    public int getCountOfMonths(){
        return listOfMonths.size();
    }
    @Step
    public int getCountOfDays(){
        return listOfDays.size();
    }
    @Step
    public int getCountOfYears(){
        return listOfYears.size();
    }
    @Step
    public List<WebElement> getListOfYears(){
        return listOfYears;
    }
    @Step
    public void clickOnYearOpenButton(){
        yearOpenButton.click();
    }
    @Step
    public void clickOnMaritalStatusOpenButton(){
        maritalStatusOpenButton.click();
    }
    @Step
    public String getNameOfStatus(int index){
        return listOfMaritalStatuses.get(index).getText();
    }
    @Step
    public void clickOnDayOpenButton(){
        dayOpenButton.click();
    }
    @Step
    public String getFirstDayValue(){
        return firstDayValue.getText();
    }
    @Step
    public String getLastDayValue(){
        return lastDayValue.getText();
    }
    @Step
    public String getNameOfFirstMonth(){
        return firstMonthValue.getText();
    }
    @Step
    public String getNameOfLastMonth(){
        return lastMonthValue.getText();
    }
    @Step
    public void clickOnMonthOpenButton(){
        monthOpenButton.click();
    }
    @Step
    public String getNameOfFirstYear(){
        return firstYearValue.getText();
    }
    @Step
    public String getNameOfLastYear(){
        return lastYearValue.getText();
    }
    @Step
    public String getActiveOfContinueButton(){
        return continueButtonNotValid.getAttribute("disabled");
    }
    @Step
    public boolean checkIfFemaleButtonIsActive(){
        return femaleActiveButton.isEnabled();
    }
    @Step
    public boolean checkIfMaleButtonIsActive(){
        return maleActiveButton.isEnabled();
    }
    @Step
    public boolean checkIfFemaleButtonIsActiveAfterClicking(){
        return femaleButtonAfterActivating.isEnabled();
    }
    @Step
    public int getActiveOfGenderButtons(){
        return activeOfGenderButtons.size();
    }
    @Step
    public void clickOnBackButton(){
        backButton.click();
    }
    @Step
    public boolean checkPresentsOfPreviousPage(){
        return presentsOfPreviousPage.isEnabled();
    }
}
