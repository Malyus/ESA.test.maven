package com.stepspages;

import com.webdriver.Page;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by M.Malyus on 2/20/2018.
 */
public class PaymentDetailsPage extends Page {
    private final Wait wait = new WebDriverWait(webDriver,5,1000);
    final static Logger LOG = Logger.getLogger(PaymentDetailsPage.class);

    @FindBy(xpath = "//div[@style='width:84%']")
    private WebElement progressBar;

    @FindBy(xpath = "//input[@id='creditcard-number']")
    private WebElement cardNumberInput;

    @FindBy(xpath = "//div[@class='col-xs-6 col-sm-6 exp-date']//input")
    private WebElement expDateInput;

    @FindBy(xpath = "//input[@id='cvv']")
    private WebElement cvvCodeInput;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButton;

    public PaymentDetailsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Step
    public boolean checkPresentsOfPaymentDetailsPage(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return progressBar.getAttribute("style").equals("width: 84%;");
    }
    @Step
    public void writeCardNumber(String cardNumber){
        cardNumberInput.sendKeys(cardNumber);
    }
    @Step
    public void writeExpDate(String expDate){
        expDateInput.sendKeys(expDate);
    }
    @Step
    public void writeCvvCode(String cvvCode){
        cvvCodeInput.sendKeys(cvvCode);
    }
    @Step
    public void clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        webDriver.navigate().refresh();
        backButton.click();
    }
    @Step
    public PlaceOrderPage clickOnContinueButton(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)webDriver;
        javascriptExecutor.executeScript("arguments[0].click();",continueButton);
        return PageFactory.initElements(webDriver,PlaceOrderPage.class);
    }
}
