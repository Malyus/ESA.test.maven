package com.webdriver;

/**
 * Created by M.Malyus on 1/25/2018.
 */

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * A factory that returns a singleton of WebDriver object.
 */
public class WebDriverFactory {
    private static final String CHROME = "chrome";
    private static final String FIREFOX = "firefox";
    private static final String INTERNETEXPLORER = "ie";
    private static final String SAFARI = "safari";
    private static final String BS_CHROME = "chrome_56";
    private static ThreadLocal<WebDriver> webDriverThreadLocal = new ThreadLocal<>();
    private static DesiredCapabilities dc;
    /*Capabilities fro BrowserStack connection*/
    public static final String USERNAME = "alex3545";
    public static final String AUTOMATE_KEY = "N9itYTzAx9Q2XqDWbUox";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    private WebDriverFactory() {

    }
    /**
     * Gets the single instance of WebDriverFactory.
     *
     * @param browser the browser set in properties
     * @return single instance of WebDriverFactory(singleton)
     * @throws Exception the exception of invalid browser property
     */
    public static WebDriver getInstance(String browser) throws Exception {
        WebDriver instance;
        if (webDriverThreadLocal != null) {
            if (CHROME.equals(browser)) {
                setChromeDriver();
                instance = new ChromeDriver();
                TimeUnit.SECONDS.sleep(1);
                webDriverThreadLocal.set(instance);
            } else if (FIREFOX.equals(browser)) {
                setFirefox();
                instance = new FirefoxDriver();
                webDriverThreadLocal.set(instance);
            } else if (INTERNETEXPLORER.equals(browser)) {
                setIeDriver();
                instance = new InternetExplorerDriver();
                webDriverThreadLocal.set(instance);
            } else if (SAFARI.equals(browser)) {
                /*DesiredCapabilities for local grid node set-up*/
                DesiredCapabilities capabilities= DesiredCapabilities.firefox();
                capabilities.setBrowserName("firefox");
                /*capabilities.setPlatform(Platform.WIN10);*/
                webDriverThreadLocal.set(new RemoteWebDriver(new URL("http://10.10.0.88:4444/wd/hub"),capabilities));

               /* DesiredCapabilities dc = new DesiredCapabilities();
//				dc.setCapability("browser", "Safari");
//				dc.setCapability("browser_version", "11.0");
                dc.setCapability("browser", "Chrome");
                dc.setCapability("browser_version", "63.0");
                dc.setCapability("browserstack.selenium_version", "3.5.2");
                dc.setCapability("os", "OS X");
                dc.setCapability("os_version", "High Sierra");
                dc.setCapability("resolution", "1920x1080");
                dc.setCapability("browserstack.safari.enablePopups", "true");
                dc.setCapability("project", "com.IDL");
//				dc.setCapability("browserstack.local","true");
//				dc.setCapability("browserstack.debug","true");*/

                //webDriverThreadLocal.set(new RemoteWebDriver(new URL(URL), capabilities));

            } else if (BS_CHROME.equals(browser)) {

//				DesiredCapabilities dc = new DesiredCapabilities();
//				dc.setCapability("browser", "Chrome");
//				dc.setCapability("browser_version", "56.0");
//				dc.setCapability("os", "Windows");
//				dc.setCapability("os_version", "10");
//				dc.setCapability("resolution", "1920x1080");
//				dc.setCapability("browserstack.safari.enablePopups", "true");
//				dc.setCapability("project", "com.mmjtraine");
//
//				webDriver = new RemoteWebDriver(new URL(URL), dc);

            } else
                throw new Exception(
                        "Invalid browser property set in configuration file");

        }
        return webDriverThreadLocal.get();
    }

    public static WebDriver getSetDriver() {
        if (webDriverThreadLocal.get() == null) {
            throw new RuntimeException("Driver is not set");
        }
        return webDriverThreadLocal.get();
    }
    /**
     * Kill driver instance.
     * Close all browsers
     * @throws Exception
     */
    public static void killDriverInstance() throws Exception {
        if (webDriverThreadLocal.get() != null) {
            try{
                webDriverThreadLocal.get().quit();
            }
            finally {
                webDriverThreadLocal.remove();
            }
        }
    }
    /**
     * Sets the chrome driver path for specific OS.
     *
     * @throws Exception the exception
     */
    public static void setChromeDriver() throws Exception {
        String osName = System.getProperty("os.name").toLowerCase();
        StringBuffer chromeBinaryPath = new StringBuffer(
                "src/main/resources/");

        if (osName.startsWith("win")) {
            chromeBinaryPath.append("chrome-win/chromedriverv2.36.exe");
        } else if (osName.startsWith("lin")) {
            chromeBinaryPath.append("chrome-lin/chromedriver");
        } else if (osName.startsWith("mac")) {
            chromeBinaryPath.append("chrome-mac/chromedriver");
        } else
            throw new Exception("Your OS is invalid for webdriver tests");

        System.setProperty("webdriver.chrome.driver",
                chromeBinaryPath.toString());
    }
    /*IE driver only for windows*/
    public static void setIeDriver() throws Exception {
        String osName = System.getProperty("os.name").toLowerCase();
        StringBuffer ieBinaryPath = new StringBuffer(
                "src/main/resources/");

        if (osName.startsWith("win")) {
            ieBinaryPath.append("ie-win/IEDriverServer.exe");
        }
        else
            throw new Exception("Your OS is invalid for webdriver tests");

        System.setProperty("webdriver.ie.driver",
                ieBinaryPath.toString());
    }
    /**
     * Sets the gecko driver path for specific OS.
     *
     * @throws Exception the exception
     */
    public static void setFirefox() throws Exception {
        String osName = System.getProperty("os.name").toLowerCase();
        StringBuffer firefoxBinaryPath = new StringBuffer("src/main/resources/");

        if(osName.startsWith("win")){
            firefoxBinaryPath.append("gecko-win/geckodriver.exe");
        }
        else if (osName.startsWith("lin")){
            firefoxBinaryPath.append("gecko-lin/geckodriver");
        }
        else if (osName.startsWith("mac")){
            firefoxBinaryPath.append("gecko-mac/geckodriver");
        }
        else
            throw new Exception("Your OS is invalid for webdriver tests");
        System.setProperty("webdriver.gecko.driver",firefoxBinaryPath.toString());
    }
}