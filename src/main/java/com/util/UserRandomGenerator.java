package com.util;

import org.openqa.selenium.JavascriptExecutor;

import java.util.Random;

/**
 * Created by M.Malyus on 2/1/2018.
 */
public class UserRandomGenerator {
    public static String generateRandomUser(){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        Random randomNumber = new Random();

        int  numbers = randomNumber.nextInt(50) * 18;
        String generatedString = buffer.toString() + numbers;
        return generatedString;
    }
}
