package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 1/29/2018.
 */
public class PricingReader {

    private Properties properties;

    public PricingReader(){
        properties = new Properties();

        try{
            properties.load(new FileInputStream(new File("src/main/resources/mainproperties/links.properties")));
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public String getTermsLink(){
        return properties.getProperty("TermsLink");
    }
    public String getPrivacyLink(){
        return properties.getProperty("PrivacyLink");
    }

}
