package com.mainbo;

import com.mainpages.HomePage;
import com.mainpages.PricingPage;
import com.mainpages.ProfilePage;
import com.popuppages.SignInPage;
import com.webdriver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by M.Malyus on 1/31/2018.
 */
public class SignInBO {
    private WebDriver driver = WebDriverFactory.getSetDriver();
    HomePage homePage = PageFactory.initElements(driver,HomePage.class);

    public PricingPage goToPricingPageAndSignIn(String email, String password){
        SignInPage signInPage = homePage.clickOnSignInButton();
        signInPage.clickOnSignUpButton();
        signInPage.writeNewEmail(email);
        signInPage.writeNewPassword(password);
        signInPage.writeConfirmPassword(password);
        ProfilePage profilePage = signInPage.clickOnSignUpButtonToRegister();
        profilePage.clickOnPricingButtonTopMenu();
        return PageFactory.initElements(driver,PricingPage.class);
    }
}
