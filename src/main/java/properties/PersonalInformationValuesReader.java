package properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by M.Malyus on 11/21/2017.
 */
public class PersonalInformationValuesReader {
    private Properties properties;

    public PersonalInformationValuesReader() {
        properties = new Properties();

        try {
            properties.load(new FileInputStream(new File("src/main/resources/personalinformationvalues.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFirstName() {
        return properties.getProperty("FirstName");
    }
    public String getLastName(){
        return properties.getProperty("LastName");
    }
    public String getEmail(){
        return properties.getProperty("Email");
    }
    public String getTelephoneNumber(){
        return properties.getProperty("TelephoneNumber");
    }

}
