package mainpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class HomePage extends Page {
    private WebDriver driver = SingletonDriver.getInstance();

    @FindBy(xpath = "//div[@class='nav-list']//li[1]")
    private WebElement certificationPricingButton;

    @FindBy(xpath = "//div[@class='nav-list']//li[2]")
    private WebElement faqButton;

    @FindBy(xpath = "//div[@class='nav-list']//li[3]")
    private WebElement blogButton;

    @FindBy(xpath = "//div[@class='nav-list']//li[4]")
    private WebElement contactUsButton;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-singin']")
    private WebElement signInButton;

    public HomePage() throws Exception {
    }

    @Step
    public void clickOnCertificationPricingButton() throws Exception {
        certificationPricingButton.click();
    }
    @Step
    public void clickOnFaqButton() throws Exception {
        faqButton.click();
    }
    @Step
    public void clickOnBlogButton() throws Exception {
        blogButton.click();
    }
    @Step
    public void clickOnContactUsButton() throws Exception {
        contactUsButton.click();
    }
    @Step
    public void clickOnSignInButton() throws Exception {
        signInButton.click();
    }
}
