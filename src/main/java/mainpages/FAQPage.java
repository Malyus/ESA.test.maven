package mainpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class FAQPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    @FindBy(xpath = "//h1[text()='Faq']")
    private WebElement presentsOfFAQPage;

    @FindBy(xpath = "//a[@href='#answer5']//span[@class='arrow']")
    private WebElement thirdQuestionButton;

    @FindBy(xpath = "//div[@id='answer4']")
    private WebElement thirdAnswer;

    @FindBy(xpath = "//a[@href='#answer5']//span[@class='arrow']")
    private WebElement thirdQuestionArrow;

    @FindBy(xpath = "//a[@href='#answer1']//span[@class='arrow']")
    private WebElement firstQuestionButton;

    @FindBy(xpath = "//*[@id='faqaccordion']/div[1]/div[1]/a/span[2]")
    private WebElement firstArrow;

    public FAQPage() throws Exception {
    }

    @Step
    public boolean checkPresentsOfFAQPage(){
        return presentsOfFAQPage.isDisplayed();
    }
    @Step
    public void turnBackByBrowserButton(){
        webDriver.navigate().back();
    }
    @Step
    public void clickOnThirdQuestion(){
        thirdQuestionButton.click();
    }
    @Step
    public String getActiveOfThirdAnswer(){
        return thirdAnswer.getAttribute("aria-expanded");
    }
    @Step
    public void clickOnFirstQuestionButton(){
        firstQuestionButton.click();
    }
    @Step
    public String getRotateValueOfThirdAnswer(){
        return thirdQuestionArrow.getCssValue("top");
    }
    @Step
    public String getRotateValueOfFirstAnswer(){
        return firstArrow.getCssValue("transform");
    }
}
