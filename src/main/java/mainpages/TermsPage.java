package mainpages;

import org.openqa.selenium.WebDriver;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/29/2018.
 */
public class TermsPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    public TermsPage() throws Exception {

    }
    @Step
    public String getURLOfTermsPage(){
        return webDriver.getCurrentUrl();
    }
    @Step
    public void turnBackByBrowserButton(){
        webDriver.navigate().back();
    }
}
