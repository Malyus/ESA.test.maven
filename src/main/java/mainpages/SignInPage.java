package mainpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/31/2018.
 */
public  class SignInPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailInputField;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInputField;

    @FindBy(xpath = "//form[@method='post']//button[text()='Sign In']")
    private WebElement signInButton;

    @FindBy(xpath = "//div[@class='button-area-sign signup']//button")
    private WebElement signUpButton;

    @FindBy(xpath = "//input[@name='Signup[email]']")
    private WebElement signUpEmail;

    @FindBy(xpath = "//input[@name='Signup[password]']")
    private WebElement signUpPassword;

    @FindBy(xpath = "//input[@name='Signup[confirmpassword]']")
    private WebElement signUpConfirmPassword;

    @FindBy(xpath = "//div/form[2]/button")
    private WebElement signUpButtonForProfile;

    public SignInPage() throws Exception {
    }

    @Step
    public void writeEmailOfUser(String email){
        emailInputField.sendKeys(email);
    }
    @Step
    public void writePasswordOfUser(String password){
        passwordInputField.sendKeys(password);
    }
    @Step
    public ProfilePage clickOnSignInButton(){
        signInButton.click();
        return PageFactory.initElements(webDriver,ProfilePage.class);
    }
    @Step
    public void clickOnSignUpButton(){
        signUpButton.click();
    }
    @Step
    public void writeNewEmail(String email){
        signUpEmail.sendKeys(email);
    }
    @Step
    public void writeNewPassword(String password){
        signUpPassword.sendKeys(password);
    }
    @Step
    public void writeConfirmPassword(String confirmPassword){
        signUpConfirmPassword.sendKeys(confirmPassword);
    }
    @Step
    public ProfilePage clickOnSignUpButtonToRegister(){
        signUpButtonForProfile.click();
        return PageFactory.initElements(webDriver,ProfilePage.class);
    }
}