package mainpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class BlogPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    @FindBy(xpath = "//aside[@class='col-sm-3 category-menu']")
    private WebElement presentsOfBlogPage;

    public BlogPage() throws Exception {
    }

    @Step
    public boolean checkPresentsOfBlogPage(){
        return presentsOfBlogPage.isDisplayed();
    }
    @Step
    public void turnBackByBrowserButton(){
        webDriver.navigate().back();
    }
}
