package mainpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class ContactUsPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    @FindBy(xpath = "//div[@class='col-sm-6']//div[@class='contact-info'][1]")
    private WebElement presentsOfContactUsPage;

    public ContactUsPage( ) throws Exception {
    }
    @Step
    public boolean checkPresentsOfContactUsPage(){
        return presentsOfContactUsPage.isDisplayed();
    }
    @Step
    public void turnBackToPreviousPage(){
        webDriver.navigate().back();
    }
}
