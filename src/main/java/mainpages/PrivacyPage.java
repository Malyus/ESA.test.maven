package mainpages;

import org.openqa.selenium.WebDriver;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/29/2018.
 */
public class PrivacyPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    public PrivacyPage() throws Exception {
    }
    @Step
    public String getCurrentURL(){
        return webDriver.getCurrentUrl();
    }
    @Step
    public void turnBackByBrowser(){
        webDriver.navigate().back();
    }
}
