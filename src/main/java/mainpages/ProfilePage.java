package mainpages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/31/2018.
 */
public class ProfilePage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();

    @FindBy(xpath = "//div[@class='nav-list']//li[1]")
    private WebElement pricingButtonTopMenu;

    public ProfilePage(WebDriver webDriver) throws Exception {
        super(webDriver);
    }
    @Step
    public PricingPage clickOnPricingButtonTopMenu(){
        pricingButtonTopMenu.click();
        return PageFactory.initElements(webDriver,PricingPage.class);
    }
}
