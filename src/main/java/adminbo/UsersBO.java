package adminbo;

import adminpage.AddNewUserPage;
import adminpage.DashboardPage;
import adminpage.UsersPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 4/27/2018.
 */
public class UsersBO {
    private WebDriver driver = SingletonDriver.getInstance();
    private AdminLoginBO adminLoginBO;
    private DashboardPage dashboardPage;
    private UsersPage usersPage;
    private AddNewUserPage addNewUserPage;

    public UsersBO() throws Exception {
    }

    public UsersPage createUser(String name, String email, String password,String userType) throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeName(name);
        addNewUserPage.writeEmail(email);
        addNewUserPage.writePassword(password);
        addNewUserPage.selectUserType(userType);
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        return PageFactory.initElements(driver,UsersPage.class);
    }
}
