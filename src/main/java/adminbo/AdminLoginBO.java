package adminbo;

import adminpage.AdminLoginPage;
import adminpage.DashboardPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 4/24/2018.
 */
public class AdminLoginBO {
    private WebDriver driver = SingletonDriver.getInstance();
    private AdminLoginPage adminLoginPage;
    private String adminURL = "http://10.10.0.77/esa/admin";
    private String email = "test2@test.com";
    private String password = "pass";

    public AdminLoginBO() throws Exception {
    }

    public DashboardPage loginToAdminPanel() throws Exception {
        adminLoginPage = new AdminLoginPage();
        adminLoginPage.openInNewWindow(adminURL);
        adminLoginPage.switchToANewWindow();
        adminLoginPage.writeEmail(email);
        adminLoginPage.writePassword(password);
        adminLoginPage.clickOnSubmitButton();
        return PageFactory.initElements(driver,DashboardPage.class);
    }
}
