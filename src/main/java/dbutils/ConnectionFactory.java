package dbutils;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by M.Malyus on 2/7/2018.
 */
public class ConnectionFactory {

    private static ConnectionFactory instance = new ConnectionFactory();
    public static final String URL = "jdbc:mariadb://178.63.12.215:3306/";
    public static final String SCHEMA_NAME = "potlala1dev";
    public static final String USER = "NsnUsnBahNdev";
    public static final String PASSWORD = "NsjaaBashakHSYJsjashJAHSJAwhqghg";
    public static final String DRIVER_CLASS = "org.mariadb.jdbc.Driver";

    final static Logger LOG = Logger.getLogger(ConnectionFactory.class);

    private ConnectionFactory() {
        try {
            Class.forName(DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private Connection createConnection(){
        Connection connection = null;
        try{
            connection = DriverManager.getConnection(URL+SCHEMA_NAME,USER, PASSWORD);
            LOG.info("Connected to the database!");
        }catch (SQLException e){
            LOG.error("Error:Unable to Connect to Database!");
        }
        return connection;
    }
    public static Connection getConnection(){
        return instance.createConnection();
    }

}
