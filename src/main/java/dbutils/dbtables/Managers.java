package dbutils.dbtables;

import dbutils.DataBaseExecute;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Created by M.Malyus on 5/18/2018.
 */
public class Managers {
    final static Logger LOG = Logger.getLogger(Managers.class);

    public static void deleteManager(String email) throws SQLException {
        DataBaseExecute db = new DataBaseExecute();
        String query = String.format("DELETE FROM managers WHERE email = '%s'", email);
        db.connect(query);
        db.close();
    }
}
