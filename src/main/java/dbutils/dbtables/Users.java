package dbutils.dbtables;

import dbutils.DataBaseExecute;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Created by M.Malyus on 5/18/2018.
 */
public class Users {
    final static Logger LOG = Logger.getLogger(Users.class);
    /*It's good to delete every customer
    after creating new order*/
    public static void deleteCustomer(String email) throws SQLException {
        DataBaseExecute db = new DataBaseExecute();
        String query = String.format("DELETE FROM customers WHERE email = '%s'", email);
        db.connect(query);
        db.close();
    }


}
