package stepsbo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.Page;
import stepspage.ShippingAddressPage;

/**
 * Created by M.Malyus on 2/21/2018.
 */
public class ShippingBO extends Page {
    protected ShippingAddressPage shippingAddressPage = new ShippingAddressPage();

    public ShippingBO() throws Exception {
    }

    public void writeShippingAddressValues(String street,String city,String state,String zip){
        shippingAddressPage.writeStreetAddress(street);
        shippingAddressPage.writeCityValue(city);
        shippingAddressPage.writeStateValue(state);
        shippingAddressPage.writeZipCode(zip);
    }

}
