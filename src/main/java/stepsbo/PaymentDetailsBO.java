package stepsbo;

import constants.ShippingAddressConstants;
import page.Page;
import stepspage.PaymentDetailsPage;
import stepspage.ShippingAddressPage;

import java.io.IOException;

/**
 * Created by M.Malyus on 3/26/2018.
 */
public class PaymentDetailsBO extends Page {
    private ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();
    private  PetSectionBO petSectionBO;
    private PaymentDetailsPage paymentDetailsPage;
    private String street = shippingAddressConstants.getStreet();
    private String city = shippingAddressConstants.getCity();
    private String state = shippingAddressConstants.getState();
    private String zip = shippingAddressConstants.getZip();

    public PaymentDetailsBO( ) throws Exception {
    }

    public PaymentDetailsPage goToPaymentDetailsPage() throws Exception {
        petSectionBO = new PetSectionBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        ShippingBO shippingBO = new ShippingBO();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnModalContinueButton();
        return new PaymentDetailsPage();
    }
    public void fillFieldsOfPaymentDetailsPage(String cardNumber,String date,String cvv) throws Exception {
        paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsPage.writeCardNumber(cardNumber);
        paymentDetailsPage.writeExpDate(date);
        paymentDetailsPage.writeCvvCode(cvv);
    }
    public PaymentDetailsPage goToPaymentDetailsWithThreePets() throws Exception {
        petSectionBO = new PetSectionBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingWithThreePets();
        ShippingBO shippingBO = new ShippingBO();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnModalContinueButton();
        return new PaymentDetailsPage();
    }
    public PaymentDetailsPage goToPaymentDetailsWithAllServices() throws Exception {
        petSectionBO = new PetSectionBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingWithAllServices();
        ShippingBO shippingBO = new ShippingBO();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnModalContinueButton();
        return new PaymentDetailsPage();
    }


}
