package stepsbo;

import org.json.simple.parser.ParseException;
import stepspage.PlaceOrderPage;
import stepspage.PrintDetailsPage;

import java.io.IOException;

/**
 * Created by M.Malyus on 4/13/2018.
 */
public class PrintDetailsBO {
    private PlaceOrderBO placeOrderBO;
    private PlaceOrderPage placeOrderPage;
    private PrintDetailsPage printDetailsPage;

    public PrintDetailsPage goToPrintDetailsPage() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithAllServices();
        printDetailsPage = placeOrderPage.clickOnContinueButton();
        printDetailsPage.waitForTextPageInit();
        return printDetailsPage;
    }
}
