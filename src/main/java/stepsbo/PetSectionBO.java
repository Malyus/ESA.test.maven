package stepsbo;

import page.Page;
import stepspage.AdditionalServicesPage;
import stepspage.PetSectionPage;
import stepspage.ShippingAddressPage;

/**
 * Created by M.Malyus on 2/6/2018.
 */
public class PetSectionBO extends Page {
    protected QuestionsBO questionsBO = new QuestionsBO();
    protected PetSectionPage petSectionPage;


    public PetSectionBO() throws Exception {
    }
    public void fillNotAllFieldsOnePet(String petsBreed,String petsName) throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed(petsBreed);
        petSectionPage.writePetsName(petsName);
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
    }
    public void fillNotAllFieldsTwoPet(String firstBreed,String firstName,String secondName,String secondBreed) throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed(firstBreed);
        petSectionPage.writePetsName(firstName);
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.writeSecondPetsBreed(secondBreed);
        petSectionPage.writeSecondPetsName(secondBreed);
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.clickOnSecondIdCardForPet();
        petSectionPage.uploadSecondPhoto();
        petSectionPage.clickOnSecondCropButton();
    }
    public void fillNotAllFieldsThreePets(String thirdBreed,String thirdName) throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.writeSecondPetsBreed("cat");
        petSectionPage.writeSecondPetsName("elmo");
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.clickOnSecondIdCardForPet();
        petSectionPage.uploadSecondPhoto();
        petSectionPage.clickOnSecondCropButton();
//        petSectionPage.selectThirdPetsType(1);
//        petSectionPage.writeThirdPetsBreed(thirdBreed);
//        petSectionPage.writeThirdPetsName(thirdName);
//        petSectionPage.selectThirdPetsWeight(1);
//        petSectionPage.clickOnThirdIdCardForPet();
//        petSectionPage.uploadThirdPhoto();
    }
    public void fillSomeFieldsOfAllThreePetsSections(String first,String firstName,String second,String secondName,String third,String thirdName) throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.selectPetsType(1);
        petSectionPage.writePetsBreed(first);
        petSectionPage.writePetsName(firstName);
        petSectionPage.selectPetsWeight(1);
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.writeSecondPetsBreed(second);
        petSectionPage.writeSecondPetsName(secondName);
        petSectionPage.selectSecondTypeOfPet(1);
        petSectionPage.clickOnSecondIdCardForPet();
        petSectionPage.uploadSecondPhoto();
        petSectionPage.clickOnSecondCropButton();
//        petSectionPage.selectThirdPetsType(1);
//        petSectionPage.writeThirdPetsBreed(third);
//        petSectionPage.writeThirdPetsName(thirdName);
//        petSectionPage.selectThirdPetsWeight(1);
//        petSectionPage.clickOnThirdIdCardForPet();
//        petSectionPage.uploadThirdPhoto();
    }
    public AdditionalServicesPage goToAdditionalServicesPage() throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        return petSectionPage.goToAdditionalServicesPage();
    }
    public ShippingAddressPage goToShippingPage() throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        AdditionalServicesPage additionalServicesPage = petSectionPage.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        return additionalServicesPage.clickOnContinueButton();
    }
    public ShippingAddressPage goToShippingWithThreePets() throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.writePetsBreed("cat#1");
        petSectionPage.writePetsName("name#1");
        petSectionPage.writeSecondPetsBreed("cat#2");
        petSectionPage.writeSecondPetsName("name#2");
//        petSectionPage.writeThirdPetsBreed("cat#3");
//        petSectionPage.writeThirdPetsName("name#3");
        AdditionalServicesPage additionalServicesPage = petSectionPage.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        return additionalServicesPage.clickOnContinueButton();
    }
    public ShippingAddressPage goToShippingWithAllServices() throws Exception {
        petSectionPage = questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.writePetsBreed("cat#1");
        petSectionPage.writePetsName("name#1");
        petSectionPage.writeSecondPetsBreed("cat#2");
        petSectionPage.writeSecondPetsName("name#2");
        AdditionalServicesPage additionalServicesPage = petSectionPage.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        additionalServicesPage.clickOnUnitedSpecialForm();
        return additionalServicesPage.clickOnContinueButton();
    }
}
