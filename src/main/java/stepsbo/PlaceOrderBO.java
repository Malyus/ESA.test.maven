package stepsbo;

import org.json.simple.parser.ParseException;
import stepspage.PaymentDetailsPage;
import stepspage.PlaceOrderPage;
import util.parsers.PaymentJson;

import java.io.IOException;

/**
 * Created by M.Malyus on 4/3/2018.
 */
public class PlaceOrderBO {
    private PaymentJson paymentJson = new PaymentJson();
    private PaymentDetailsPage paymentDetailsPage;
    private PaymentDetailsBO paymentDetailsBO;
    private PlaceOrderPage placeOrderPage;
    private String validCreditCard = paymentJson.getCreditCard();
    private String validDate = paymentJson.getExpDate();
    private String validCvv = paymentJson.getCvv();

    public PlaceOrderBO() throws IOException, ParseException {
    }

    public PlaceOrderPage goToPlaceOrderPage() throws Exception {
        paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        placeOrderPage.checkPresentsOfPlaceOrderPage();
        placeOrderPage.waitForPageValuesInitialization();
        return placeOrderPage;
    }
    public PlaceOrderPage goToPlaceOrderWithTreePets() throws Exception {
        paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsWithThreePets();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        placeOrderPage.checkPresentsOfPlaceOrderPage();
        placeOrderPage.waitForPageValuesInitialization();
        return placeOrderPage;
    }
    public PlaceOrderPage goToPlaceOrderWithAllServices() throws Exception {
        paymentDetailsPage = new PaymentDetailsPage();
        paymentDetailsBO = new PaymentDetailsBO();
        paymentDetailsBO.goToPaymentDetailsWithAllServices();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        placeOrderPage.checkPresentsOfPlaceOrderPage();
        placeOrderPage.waitForPageValuesInitialization();
        return placeOrderPage;
    }
}
