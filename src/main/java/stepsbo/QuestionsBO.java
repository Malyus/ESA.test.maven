package stepsbo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import page.Page;
import questions.QuestionsPage;
import stepspage.PetSectionPage;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 2/6/2018.
 */
public class QuestionsBO extends Page {
    private PersonalInformationBO personalInformationBO;
    private QuestionsPage questionsPage;

    public QuestionsBO( ) throws Exception {
    }
    public PetSectionPage goToPetSectionPage() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        questionsPage = new QuestionsPage();
        personalInformationBO.fillNotAllFields("Alex","Mason");
        //questionsPage.analyzeTypeOfQuestions();
        questionsPage.quickAnswers();
        return new PetSectionPage();
    }
}
