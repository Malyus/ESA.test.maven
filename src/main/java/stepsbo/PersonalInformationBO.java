package stepsbo;

import mainpages.HomePage;
import mainpages.PricingPage;
import page.Page;
import stepspage.PersonalInformationPage;
import util.testdatautils.UserRandomGenerator;

/**
 * Created by M.Malyus on 1/25/2018.
 */
public class PersonalInformationBO extends Page {
    private HomePage homePage;
    SignInBO signInBO;
    String email = UserRandomGenerator.generateRandomUser() + "@gmail.com";
    String password = UserRandomGenerator.generateRandomUser();

    public PersonalInformationBO( ) throws Exception {
    }
    public void goToPersonalInformationPage() throws Exception {
        signInBO = new SignInBO();
        signInBO.goToPricingPageAndSignIn(email,password);
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnComboPlanButton();
        System.out.println("Email and Password:" + email + ";" +  password);
    }
    public void fillNotAllFields(String firstName, String lastName) throws Exception {
        homePage = new HomePage();
        homePage.clickOnSignInButton();
        signInBO = new SignInBO();
        signInBO.goToPricingPageAndSignIn(email,password);
        PricingPage pricingPage = new PricingPage();
        System.out.println("Email and Password:" + email + ";" +  password);
        pricingPage.clickOnComboPlanButton();
        PersonalInformationPage personalInformationPage = new PersonalInformationPage();
        personalInformationPage.selectFirstMonth();
        personalInformationPage.selectFirstDay();
        personalInformationPage.selectFirstYear();
        personalInformationPage.clickOnFemaleCheckBox();
        personalInformationPage.selectMarriedStatus();
        personalInformationPage.writeIntoFirstNameField(firstName);
        personalInformationPage.writeIntoLastNameField(lastName);
        personalInformationPage.clickOnNotValidContinue();
    }
    public String getEmail(){
        return email;
    }
}
