package stepsbo;

import mainpages.HomePage;
import mainpages.PricingPage;
import mainpages.ProfilePage;
import mainpages.SignInPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 1/31/2018.
 */
public class SignInBO {
    HomePage homePage;

    public SignInBO() throws Exception {
    }

    public  void goToPricingPageAndSignIn(String email, String password) throws Exception {
        homePage = new HomePage();
        homePage.clickOnSignInButton();
        SignInPage signInPage = new SignInPage();
        signInPage.clickOnSignUpButton();
        signInPage.writeNewEmail(email);
        signInPage.writeNewPassword(password);
        signInPage.writeConfirmPassword(password);
        ProfilePage profilePage = signInPage.clickOnSignUpButtonToRegister();
        profilePage.clickOnPricingButtonTopMenu();
    }
}
