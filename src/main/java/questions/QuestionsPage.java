package questions;

import groovy.lang.Singleton;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import stepspage.PersonalInformationPage;
import webdriver.SingletonDriver;

import java.util.List;

/**
 * Created by M.Malyus on 2/5/2018.
 */
public class QuestionsPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(webDriver,5,1000);

    @FindBy(xpath = "//div[@class='group row button-area']")
    private WebElement buttonGroup;

    @FindBy(xpath = "//div[@class='progress-bar']")
    private WebElement progressBar;

    @FindBy(xpath = "//input[@type='text']")
    private WebElement answerInput;

    @FindAll(@FindBy(xpath = "//input[@type='radio']"))
    private List<WebElement> questionsButton;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//div[@style='width:28%']")
    private WebElement presentsOfQuestions;

    @FindAll(@FindBy(xpath = "//input[@type='text']"))
    private List<WebElement> inputQuestions;

    @FindAll(@FindBy(xpath = "//input[@type='radio']"))
    private List<WebElement> buttonQuestions;

    @FindBy(xpath = "//input[@id='o1']")
    private WebElement firstQuestionButton;

    @FindBy(xpath = "//input[@id='o3']")
    private WebElement secondQuestionButton;

    @FindBy(xpath = "//input[@value='yes']")
    private WebElement tempOne;

    @FindBy(xpath = "//input[@id='q1']")
    private WebElement tempTwo;

    @FindBy(xpath = "//input[@id='q3']")
    private WebElement tempThree;

    @FindBy(xpath = "//input[@id='o1']")
    private WebElement tempFour;

    @FindBy(xpath = "//input[@id='o3']")
    private WebElement tempFive;

    @FindBy(xpath = "//*[@id='o2']")
    private WebElement no;

    public QuestionsPage() throws Exception {
    }

    @Step
    public void writeAnswer(){
        System.out.println("Class:" + answerInput.getAttribute("class"));
        System.out.println("Id:" + answerInput.getAttribute("id"));
        System.out.println("Name:" + answerInput.getAttribute("name"));
        answerInput.click();
        answerInput.sendKeys("test");
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();", continueButton);

    }
    /*This method is temp*/
    @Step
    public void quickAnswers(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfQuestions));
//        no.click();
        webDriver.get("http://10.10.0.77/esa/order#/pet-section");
    }

    @Step
    public void analyzeTypeOfQuestions(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfQuestions));
        System.out.println("ProgressBarLevel:" + progressBar.getAttribute("style"));
        /*progressBar.getAttribute("style").equalsIgnoreCase("width: 28%;")*/
        while (webDriver.getCurrentUrl().equalsIgnoreCase("http://10.10.0.77/esa/order#/questionnaire")){
            System.out.println("We are in method!");
            if(inputQuestions.size()>0 && inputQuestions.size()<2){
                System.out.println("Size of input list:" + inputQuestions.size());
                inputQuestions.get(0).sendKeys("test");
                continueButton.click();
            }
            else {
                buttonQuestions.get(0).click();
            }
        }
        /*double size = buttonGroup.getSize().getHeight() * buttonGroup.getSize().getWidth();
        System.out.println("Size:" + size);
           while (webDriver.getCurrentUrl().equalsIgnoreCase("http://10.10.0.77/esa/order#!/order/form/step2")){
            if(size != 0){
                answerInput.sendKeys("test");
                JavascriptExecutor executor = (JavascriptExecutor)webDriver;
                executor.executeScript("arguments[0].click();", continueButton);
            }
            else {
                questionsButton.get(0).click();
            }
        }*/
    }
    @Step
    public void checkPresentsOfQuestionsPage(){
        wait.until(ExpectedConditions.visibilityOf(presentsOfQuestions));
    }
    @Step
    public void turnBackByBrowserButton() throws Exception {
        webDriver.navigate().back();
    }


/*    @Step
    public void waitWhenAttributeChange(By locator,String attribute,String value){
        WebDriverWait wait = new WebDriverWait(webDriver, 5);

        wait.until(new ExpectedCondition<Boolean>() {
            private By locator;
            private String attr;
            private String initialValue;

            private ExpectedCondition<Boolean> init(By locator, String attr, String initialValue ) {
                this.locator = locator;
                this.attr = attr;
                this.initialValue = initialValue;
                return this;
            }

            public Boolean apply(WebDriver driver) {
                WebElement button = driver.findElement(this.locator);
                String enabled = button.getAttribute(this.attr);
                if(enabled.equals(this.initialValue))
                    return false;
                else
                    return true;
            }
        }.init(locator, attribute, value));
    }*/
}
