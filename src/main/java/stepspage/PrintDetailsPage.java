package stepspage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 4/13/2018.
 */
public class PrintDetailsPage extends Page {
    private final WebDriver webDriver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(webDriver,5,1000);

    @FindBy(xpath = "//div[@style='width:100%']")
    private WebElement progressBar;

    @FindBy(xpath = "//label[@id='orderid']")
    private WebElement orderId;

    @FindBy(xpath = "//label[@id='orderdate']")
    private WebElement orderDate;

    @FindBy(xpath = "//label[@id='fullname']")
    private WebElement fullNameField;

    @FindBy(xpath = "//label[@id='street']")
    private WebElement streetAddressField;

    @FindBy(xpath = "//label[@id='city']")
    private WebElement cityField;

    @FindBy(xpath = "//label[@id='state']")
    private WebElement stateField;

    @FindBy(xpath = "//label[@id='zip']")
    private WebElement zipField;

    @FindBy(xpath = "//label[1]/span[2]")
    private WebElement esaPackagePrice;

    @FindBy(xpath = "//div[5]/label[4]/span[2]")
    private WebElement firstServicePrice;

    @FindBy(xpath = "//label[5]/span[2]")
    private WebElement secondServicePrice;

    @FindBy(xpath = "//label[6]/span[2]")
    private WebElement thirdServicePrice;

    @FindBy(xpath = "//label[7]/span[2]")
    private WebElement fourthServicePrice;

    @FindBy(xpath = "//label[8]/span[2]")
    private WebElement fifthServicePrice;

    public PrintDetailsPage() throws Exception {
    }

    @Step
    public void waitForTextPageInit() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        waitForTextPresent(fullNameField,"Alex Mason");
    }
    @Step
    public boolean checkPresentsOfPage(){
        return progressBar.isDisplayed();
    }
    @Step
    public boolean checkPresentsOfOrderId(){
        return orderId.getText().isEmpty();
    }
    @Step
    public String getOrderDateValue() throws InterruptedException {
        return orderDate.getText();
    }
    @Step
    public String getFullNameText() throws InterruptedException {
        return fullNameField.getText();
    }
    @Step
    public String getStreetAddressText() throws InterruptedException {
        return streetAddressField.getText();
    }
    @Step
    public String getCityText(){
        return cityField.getText();
    }
    @Step
    public String getStateField(){
        return stateField.getText();
    }
    @Step
    public String getZipField(){
        return zipField.getText();
    }
    @Step
    public String getEsaPackagePrice(){
        return esaPackagePrice.getText();
    }
    @Step
    public String getFirstServicePrice(){
        return firstServicePrice.getText();
    }
    @Step
    public String getSecondServicePrice(){
        return secondServicePrice.getText();
    }
    @Step
    public String getThirdServicePrice(){
        return thirdServicePrice.getText();
    }
    @Step
    public String getFourthServicePrice(){
        return fourthServicePrice.getText();
    }
    @Step
    public String getFifthServicePrice(){
        return fifthServicePrice.getText();
    }
}
