package stepspage;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import stepsbo.ShippingBO;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 2/15/2018.
 */
public class AdditionalServicesPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(webDriver,5,1000);
    final static Logger LOG = Logger.getLogger(AdditionalServicesPage.class);

    @FindBy(xpath = "//div[@style='width:56%']")
    private WebElement progressBar;

    @FindBy(xpath = "//input[@id='check1']")
    private WebElement americanAirlinesSpecialForm;

    @FindBy(xpath = "//input[@id='check2']")
    private WebElement hawaiianAirlinesSpecialForm;

    @FindBy(xpath = "//input[@id='check3']")
    private WebElement jetBlueForm;

    @FindBy(xpath = "//input[@id='check4']")
    private WebElement spiritAirlinesForm;

    @FindBy(xpath = "//input[@id='check5']")
    private WebElement unitedAirwaysForm;

    @FindBy(xpath = "//span[@class='form-price ng-binding']")
    private WebElement subTotalValue;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//label[@for='check1']//span[2]")
    private WebElement firstServiceName;

    @FindBy(xpath = "//div[@class='group row']//span")
    private WebElement subtotalField;

    @FindBy(xpath = "//img[@src='./img/airlines/American_Airlines_logo.png']")
    private WebElement firstIcon;

    @FindBy(xpath = "//input[@id='check1' and @value='true']")
    private WebElement activeFirstService;

    @FindBy(xpath = "//input[@id='check1' and @value='false']")
    private WebElement disabledFirstService;

    @FindBy(xpath = "//label[@for='check2']//span[2]")
    private WebElement secondServiceName;

    @FindBy(xpath = "//label[@for='check2']//span[3]")
    private WebElement secondServicePrice;

    @FindBy(xpath = "//img[@src='./img/airlines/Hawaiian_Airlines_logo.png']")
    private WebElement secondServiceLogo;

    @FindBy(xpath = "//input[@id='check2' and @value='true']")
    private WebElement activeSecondSection;

    @FindBy(xpath = "//input[@id='check2' and @value='false']")
    private WebElement disabledSecondService;

    @FindBy(xpath = "//label[@for='check3']//span[2]")
    private WebElement thirdServiceName;

    @FindBy(xpath = "//label[@for='check3']//span[3]")
    private WebElement thirdServicePrice;

    @FindBy(xpath = "//img[@src='./img/airlines/Hawaiian_Airlines_logo.png']")
    private WebElement thirdServiceLogo;

    @FindBy(xpath = "//input[@id='check3' and @value='true']")
    private WebElement activeThirdService;

    @FindBy(xpath = "//input[@id='check3' and @value='false']")
    private WebElement disableThirdSection;

    @FindBy(xpath = "//label[@for='check4']//span[2]")
    private WebElement fourthServiceName;

    @FindBy(xpath = "//label[@for='check4']//span[3]")
    private WebElement fourthServicePrice;

    @FindBy(xpath = "//img[@src='./img/airlines/Spirit_Airways_logo.png']")
    private WebElement fourthServiceLogo;

    @FindBy(xpath = "//input[@id='check4' and @value='true']")
    private WebElement activeFourthService;

    @FindBy(xpath = "//input[@id='check4' and @value='false']")
    private WebElement disableFourthService;

    @FindBy(xpath = "//label[@for='check5']//span[2]")
    private WebElement fifthServiceName;

    @FindBy(xpath = "//label[@for='check5']//span[3]")
    private WebElement fifthServicePrice;

    @FindBy(xpath = "//img[@src='./img/airlines/United_Airways_logo.png']")
    private WebElement fifthServiceLogo;

    @FindBy(xpath = "//input[@id='check5' and @value='true']")
    private WebElement activeFifthService;

    @FindBy(xpath = "//input[@id='check5' and @value='false']")
    private WebElement disableFifthService;

    public AdditionalServicesPage() throws Exception {
    }

    @Step
    public boolean checkPresentsOfAdditionalServicesPage(){
        return progressBar.getAttribute("style").equals("width: 56;");
    }
    @Step
    public void clickOnAmericanAirlinesForm(){
        americanAirlinesSpecialForm.click();
    }
    @Step
    public void clickOnHawaiianAirlinesSpecialForm(){
        hawaiianAirlinesSpecialForm.click();
    }
    @Step
    public void clickOnJetBlueAirwaysSpecialForm(){
        jetBlueForm.click();
    }
    @Step
    public void clickOnSpiritAirwaysForm(){
        spiritAirlinesForm.click();
    }
    @Step
    public void clickOnUnitedSpecialForm(){
        unitedAirwaysForm.click();
    }
    @Step
    public ShippingAddressPage clickOnContinueButton(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        JavascriptExecutor js = (JavascriptExecutor)webDriver;
        js.executeScript("arguments[0].click();",continueButton);
        return PageFactory.initElements(webDriver,ShippingAddressPage.class);
    }
    @Step
    public PetSectionPage clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        backButton.click();
        return PageFactory.initElements(webDriver,PetSectionPage.class);
    }
    @Step
    public ShippingBO clickOnContinueButtonForShippingBO(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click();",continueButton);
        return PageFactory.initElements(webDriver,ShippingBO.class);
    }
    @Step
    public String getFirstServiceName(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return firstServiceName.getText();
    }
    @Step
    public String getSubtotalFieldValue(){
        return subtotalField.getText();
    }
    @Step
    public String checkFirstIconForValid(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return firstIcon.getAttribute("src");
    }
    @Step
    public String getActiveOfFirstService(){
        return activeFirstService.getAttribute("value");
    }
    @Step
    public String getDisabledOfFirstService(){
        return disabledFirstService.getAttribute("value");
    }
    @Step
    public String getSecondServiceName(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return secondServiceName.getText();
    }
    @Step
    public String getSecondServicePrice(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return secondServicePrice.getText();
    }
    @Step
    public String checkSecondServiceLogo(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return secondServiceLogo.getAttribute("src");
    }
    @Step
    public String getActiveOfSecondService(){
        return activeSecondSection.getAttribute("value");
    }
    @Step
    public String getDisabledOfSecondService(){
        return disabledSecondService.getAttribute("value");
    }
    @Step
    public String getNameOfThirdService(){
        wait.until(ExpectedConditions.visibilityOf(thirdServiceName));
        return thirdServiceName.getText();
    }
    @Step
    public String getThirdServicePrice(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return thirdServicePrice.getText();
    }
    @Step
    public String checkThirdServiceLogo(){
        wait.until(ExpectedConditions.visibilityOf(thirdServiceLogo));
        return thirdServiceLogo.getAttribute("src");
    }
    @Step
    public String getActiveOfThirdService(){
        return activeThirdService.getAttribute("value");
    }
    @Step
    public String getDisabledOfThirdService(){
        return disableThirdSection.getAttribute("value");
    }
    @Step
    public String getNameOfFourthService(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return fourthServiceName.getText();
    }
    @Step
    public String getPriceOfFourthService(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return fourthServicePrice.getText();
    }
    @Step
    public String checkFourthServiceLogo(){
        wait.until(ExpectedConditions.visibilityOf(fourthServiceLogo));
        return fourthServiceLogo.getAttribute("src");
    }
    @Step
    public String getActiveOfFourthService(){
        return activeFourthService.getAttribute("value");
    }
    @Step
    public String getDisabledOfFourthService(){
        return disableFourthService.getAttribute("value");
    }
    @Step
    public String getNameOfFifthService(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return fifthServiceName.getText();
    }
    @Step
    public String getPriceOfFifthService(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return fifthServicePrice.getText();
    }
    @Step
    public String checkFifthServiceLogo(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        return fifthServiceLogo.getAttribute("src");
    }
    @Step
    public String getActiveOfFifthService(){
        return activeFifthService.getAttribute("value");
    }
    @Step
    public String getDisabledOfFifthService(){
        return disableFifthService.getAttribute("value");
    }
}
