package stepspage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

import java.util.List;

/**
 * Created by M.Malyus on 2/15/2018.
 */
public class ShippingAddressPage extends Page {
    private WebDriver webDriver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(webDriver, 5, 1000);

    @FindBy(xpath = "//div[@style='width:70%']")
    private WebElement progressBarLevel;

    @FindBy(xpath = "//input[@name='address']")
    private WebElement streetAddressInput;

    @FindBy(xpath = "//input[@id='city']")
    private WebElement cityInput;

    @FindBy(xpath = "//input[@id='state']")
    private WebElement stateInput;

    @FindBy(xpath = "//input[@name='zipcode']")
    private WebElement zipCodeInput;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue btn-top']")
    private WebElement continueButton;

    @FindBy(xpath = "//div[@class='group row button-area']//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//div[@class='modal-content']")
    private WebElement modalWindow;

    @FindBy(xpath = "//div[@class='modal-content']//div[@class='modal-footer']//button[@class='btn btn-bordered btn-back']")
    private WebElement modalEditButton;

    @FindBy(xpath = "//div[@class='modal-content']//div[@class='modal-footer']//button[@class='btn btn-filled btn-continue']")
    private WebElement modalContinueButton;

    @FindBy(xpath = "//div[@class='modal-header']//button[@class='close']")
    private WebElement modalCloseButton;

    @FindAll(@FindBy(xpath = "//div[@class='form-inner ng-scope']//div//label"))
    private List<WebElement> listOfShippingElements;

    @FindAll(@FindBy(xpath = "//div[@class='modal-header']//button[@class='close']"))
    private List<WebElement> presentsOfConfirmShippingAddressPopUp;

    @FindBy(xpath = "//div[2]/div[1]/p[2]")
    private WebElement confirmShippingAddressField;

    @FindBy(xpath = "//div[2]/div[2]/p[2]")
    private WebElement confirmShippingCityField;

    @FindBy(xpath = "//div[3]/p[2]")
    private WebElement confirmShippingStateField;

    @FindBy(xpath = "//div[4]/p[2]")
    private WebElement confirmShippingZipField;

    @FindBy(xpath = "//*[@id='responseAddress']/div/div/div[2]/p[2]")
    private WebElement confirmAddressErrorMessage;

    @FindBy(xpath = "//p[@class='error ng-binding']")
    private WebElement invalidCityErrorMessage;

    @FindBy(xpath = "//div[@style='width:56%']")
    private WebElement presentsOfPreviousPage;

    @FindBy(xpath = "//h4[@class='modal-title']")
    private WebElement presentsOfModalWindow;

    @FindBy(xpath = "//p[@class='error ng-binding']")
    private WebElement errorMessageNotFound;

    public ShippingAddressPage() throws Exception {
    }

    @Step
    public boolean checkPresentsOfShippingAddressPage() {
        return progressBarLevel.getAttribute("style").equals("width: 70%;");
    }

    @Step
    public void writeStreetAddress(String streetAddress) {
        wait.until(ExpectedConditions.visibilityOf(streetAddressInput));
        streetAddressInput.sendKeys(streetAddress);
    }

    @Step
    public void writeCityValue(String city) {
        cityInput.sendKeys(city);
    }

    @Step
    public void writeStateValue(String state) {
        stateInput.sendKeys(state);
    }

    @Step
    public void writeZipCode(String zip) {
        zipCodeInput.sendKeys(zip);
    }

    @Step
    public void clickOnContinueButton() {
        wait.until(ExpectedConditions.visibilityOf(progressBarLevel));
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click();", continueButton);
    }

    @Step
    public AdditionalServicesPage clickOnBackButton() {
        wait.until(ExpectedConditions.visibilityOf(progressBarLevel));
        backButton.click();
        return PageFactory.initElements(webDriver, AdditionalServicesPage.class);
    }

    @Step
    public boolean checkPresentsOfModalWindow() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        return modalWindow.isDisplayed();
    }

    @Step
    public void clickOnEditButton() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        modalEditButton.click();
    }

    @Step
    public PaymentDetailsPage clickOnModalContinueButton() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        JavascriptExecutor executor = (JavascriptExecutor) webDriver;
        executor.executeScript("arguments[0].click()", modalContinueButton);
        return PageFactory.initElements(webDriver, PaymentDetailsPage.class);
    }

    @Step
    public void clickOnModalCloseButton() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        modalCloseButton.click();
    }

    @Step
    public String getNameOfShippingPageElements(int index) {
        return listOfShippingElements.get(index).getText();
    }

    @Step
    public void clearCityInput() {
        cityInput.clear();
    }

    @Step
    public void clearStateInput() {
        stateInput.clear();
    }

    @Step
    public void clearZipInput() {
        zipCodeInput.clear();
    }

    @Step
    public String getActiveOfContinueButton() {
        return continueButton.getAttribute("disabled");
    }

    @Step
    public String getValueOfStreetAddress() {
        return streetAddressInput.getAttribute("value");
    }

    @Step
    public String getValueOfCityInput() {
        return cityInput.getAttribute("value");
    }

    @Step
    public String getValueOfStateInput() {
        return stateInput.getAttribute("value");
    }

    @Step
    public String getValueOfZipInput() {
        return zipCodeInput.getAttribute("value");
    }

    @Step
    public int checkPresentsOfConfirmAddressPopUp() {
        return presentsOfConfirmShippingAddressPopUp.size();
    }

    @Step
    public boolean checkPresentsOfModalWindowWithoutWait() throws InterruptedException {
        Thread.sleep(2000);
        return modalWindow.isDisplayed();
    }

    @Step
    public String getActiveOfConfirmAddressContinueButton() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        return modalContinueButton.getAttribute("disabled");
    }

    @Step
    public String getValueOfConfirmShippingAddressField() throws InterruptedException {
        waitForTextPresent(confirmShippingStateField,"CA");
        return confirmShippingAddressField.getText();
    }

    @Step
    public String getValueOfConfirmShippingCityField() throws InterruptedException {
        waitForTextPresent(confirmShippingStateField,"CA");
        return confirmShippingCityField.getText();
    }

    @Step
    public String getValueOfConfirmShippingStateField() throws InterruptedException {
        waitForTextPresent(confirmShippingStateField,"CA");
        return confirmShippingStateField.getText();
    }

    @Step
    public String getValueOfConfirmShippingZipField() throws InterruptedException {
        waitForTextPresent(confirmShippingStateField,"CA");
        return confirmShippingZipField.getText();
    }

    @Step
    public void clearStreet() throws InterruptedException {
        Thread.sleep(2500);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", streetAddressInput);
    }

    @Step
    public void clearCity() throws InterruptedException {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", cityInput);
    }

    @Step
    public void clearState() throws InterruptedException {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", stateInput);
    }

    @Step
    public void clearZip() throws InterruptedException {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", stateInput);
    }

    @Step
    public String getErrorMessageText() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        return confirmAddressErrorMessage.getText();
    }

    @Step
    public boolean checkPresentsOfPreviousPage() {
        wait.until(ExpectedConditions.visibilityOf(presentsOfPreviousPage));
        return presentsOfPreviousPage.isDisplayed();
    }

    @Step
    public void clearAndWriteNewCity(String city) throws InterruptedException {
        Thread.sleep(2500);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", cityInput);
        cityInput.sendKeys(city);
    }

    @Step
    public void clearAndWriteStreetAddress(String street) {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", streetAddressInput);
        streetAddressInput.sendKeys(street);
    }

    @Step
    public void clearAndWriteState(String state) throws InterruptedException {
        Thread.sleep(2500);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", stateInput);
        stateInput.sendKeys(state);
    }

    @Step
    public void clearAndWriteZip(String zip) throws InterruptedException {
        Thread.sleep(2500);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].value='" +
                "" + "';", zipCodeInput);
        zipCodeInput.sendKeys(zip);
    }

    @Step
    public String getSizeOfProgressBar() {
        return progressBarLevel.getAttribute("style");
    }

    @Step
    public String getErrorMessageNotFound() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        return errorMessageNotFound.getText();
    }
    @Step
    public void waitForPageValuesInitialization() throws InterruptedException {
        waitForTextPresent(confirmShippingStateField,"CA");
    }
    @Step
    public String getInvalidCityErrorMessage() throws InterruptedException {
        waitForTextPresent(presentsOfModalWindow,"Modal Header");
        return invalidCityErrorMessage.getText();
    }

}
