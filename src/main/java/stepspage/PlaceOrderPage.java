package stepspage;

import com.google.common.annotations.VisibleForTesting;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Page;
import ru.yandex.qatools.allure.annotations.Step;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 3/26/2018.
 */
public class PlaceOrderPage extends Page {
    private final WebDriver webDriver = SingletonDriver.getInstance();
    private final Wait wait = new WebDriverWait(webDriver,5,1000);

    @FindBy(xpath = "//div[@style='width:100%']")
    private WebElement progressBar;

    @FindBy(xpath = "//label[@id='fullname']")
    private WebElement fullNameValue;

    @FindBy(xpath = "//label[@id='street']")
    private WebElement streetValue;

    @FindBy(xpath = "//label[@id='city']")
    private WebElement cityValue;

    @FindBy(xpath = "//label[@id='state']")
    private WebElement stateValue;

    @FindBy(xpath = "//label[@id='zip']")
    private WebElement zipValue;

    @FindBy(xpath = "//label[@id='pet1type']")
    private WebElement firstPetsType;

    @FindBy(xpath = "//label[@id='pet1breed']")
    private WebElement firstPetsBreed;

    @FindBy(xpath = "//label[@id='pet1name']")
    private WebElement firstPetsName;

    @FindBy(xpath = "//label[@id='pet1weight']")
    private WebElement firstPetsWeight;

    @FindBy(xpath = "//label[@id='pet2type']")
    private WebElement secondPetsType;

    @FindBy(xpath = "//label[@id='pet2breed']")
    private WebElement secondPetsBreed;

    @FindBy(xpath = "//label[@id='pet2name']")
    private WebElement secondPetsName;

    @FindBy(xpath = "//label[@id='pet2weight']")
    private WebElement secondPetsWeight;

    @FindBy(xpath = "//label[@id='pet3type']")
    private WebElement thirdPetsType;

    @FindBy(xpath = "//label[@id='pet3breed']")
    private WebElement thirdPetsBreed;

    @FindBy(xpath = "//label[@id='pet3name']")
    private WebElement thirdPetsName;

    @FindBy(xpath = "//label[@id='pet3weight']")
    private WebElement thirdPetsWeight;

    @FindBy(xpath = "//input[@id='radio01']")
    private WebElement standardDeliveryButton;

    @FindBy(xpath = "//input[@id='radio02']")
    private WebElement expressDeliveryButton;

    @FindBy(xpath = "//div[4]/label[1]/span[1]")
    private WebElement certificateName;

    @FindBy(xpath = "//label[1]/span[2]")
    private WebElement certificatePrice;

    @FindBy(xpath = "//div[4]/label[2]/span[1]")
    private WebElement countOfPets;

    @FindBy(xpath = "//div[4]/label[2]/span[2]")
    private WebElement petsPrice;

    @FindBy(xpath = "//div[4]/label[3]/span[1]")
    private WebElement idCardForPetCount;

    @FindBy(xpath = "//div[4]/label[3]/span[2]")
    private WebElement idCardPrice;

    @FindBy(xpath = "//div[4]/label[4]/span[1]")
    private WebElement firstServiceName;

    @FindBy(xpath = "//div[4]/label[4]/span[2]")
    private WebElement firstServicePrice;

    @FindBy(xpath = "//div[4]/label[5]/span[1]")
    private WebElement secondServiceName;

    @FindBy(xpath = "//div[4]/label[5]/span[2]")
    private WebElement secondServicePrice;

    @FindBy(xpath = "//div[4]/label[6]/span[1]")
    private WebElement thirdServiceName;

    @FindBy(xpath = "//div[4]/label[6]/span[2]")
    private WebElement thirdServicePrice;

    @FindBy(xpath = "//div[4]/label[7]/span[1]")
    private WebElement fourthServiceName;

    @FindBy(xpath = "//div[4]/label[7]/span[2]")
    private WebElement fourthServicePrice;

    @FindBy(xpath = "//div[4]/label[8]/span[1]")
    private WebElement fifthServiceName;

    @FindBy(xpath = "//div[4]/label[8]/span[2]")
    private WebElement fifthServicePrice;

    @FindBy(xpath = "//div[4]/label[9]/span[1]")
    private WebElement deliveryName;

    @FindBy(xpath = "//div[4]/label[9]/span[2]")
    private WebElement deliveryPrice;

    @FindBy(xpath = "//div[5]/label/span")
    private WebElement orderTotal;

    @FindBy(xpath = "//div[4]//label[1]/span[1]")
    private WebElement esaPackageName;

    @FindBy(xpath = "//div[4]//label[1]/span[2]")
    private WebElement esaPackagePrice;

    @FindBy(xpath = "//span[text()='American Airlines Special Form']")
    private WebElement firstService;

    @FindBy(xpath = "//span[text()='Hawaiian Airlines Special Form']")
    private WebElement secondService;

    @FindBy(xpath = "//span[text()='JetBlue Airways Special Form']")
    private WebElement thirdService;

    @FindBy(xpath = "//button[@class='btn btn-bordered btn-back']")
    private WebElement backButton;

    @FindBy(xpath = "//button[@class='btn btn-filled btn-continue']")
    private WebElement continueButton;

    @FindBy(xpath = "//div//label[4]")
    private WebElement presentsOfAdditionalService;

    public PlaceOrderPage() throws Exception {
    }

    @Step
    public boolean checkPresentsOfPlaceOrderPage(){
        return progressBar.isDisplayed();
    }
    @Step
    public String getFullNameValue(){
        return fullNameValue.getText();
    }
    @Step
    public String getStreetValue(){
        return streetValue.getText();
    }
    @Step
    public String getCityValue(){
        return cityValue.getText();
    }
    @Step
    public String getStateValue(){
        return stateValue.getText();
    }
    @Step
    public String getZipValue(){
        return zipValue.getText();
    }
    @Step
    public void clickOnBackButton(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();",backButton);
    }
    @Step
    public PrintDetailsPage clickOnContinueButton(){
        wait.until(ExpectedConditions.visibilityOf(progressBar));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)webDriver;
        javascriptExecutor.executeScript("arguments[0].click();",continueButton);
        return PageFactory.initElements(webDriver,PrintDetailsPage.class);
    }
    @Step
    public void waitForPageValuesInitialization() throws InterruptedException {
        waitForTextPresent(cityValue,"LOS ANGELES");
    }
    @Step
    public String getFirstPetsType(){
        return firstPetsType.getText();
    }
    @Step
    public String getFirstPetsBreed(){
        return firstPetsBreed.getText();
    }
    @Step
    public String getFirstPetsName(){
        return firstPetsName.getText();
    }
    @Step
    public String getFirstPetsWeight(){
        return firstPetsWeight.getText();
    }
    @Step
    public String getSecondPetsType(){
        return secondPetsType.getText();
    }
    @Step
    public String getSecondPetsBreed(){
        return secondPetsBreed.getText();
    }
    @Step
    public String getSecondPetsName(){
        return secondPetsName.getText();
    }
    @Step
    public String getSecondPetsWeight(){
        return secondPetsWeight.getText();
    }
    @Step
    public String getThirdPetsType(){
        return thirdPetsType.getText();
    }
    @Step
    public String getThirdPetsBreed(){
        return thirdPetsBreed.getText();
    }
    @Step
    public String getThirdPetsName(){
        return thirdPetsName.getText();
    }
    @Step
    public String getThirdPetsWeight(){
        return thirdPetsWeight.getText();
    }
    @Step
    public void clickOnStandardDeliveryButton(){
        standardDeliveryButton.click();
    }
    @Step
    public void clickOnExpressDeliveryButton(){
        expressDeliveryButton.click();
    }
    @Step
    public String getNameOfCertificate(){
        return certificateName.getText();
    }
    @Step
    public String getPriceOfCertificate(){
        return certificatePrice.getText();
    }
    @Step
    public String getCountOfPets(){
        return countOfPets.getText();
    }
    @Step
    public String getPetsPrice(){
        return petsPrice.getText();
    }
    @Step
    public String getFirstServiceName(){
        return firstServiceName.getText();
    }
    @Step
    public String getFirstServicePrice(){
        return firstServicePrice.getText();
    }
    @Step
    public String getSecondServiceName(){
        return secondServiceName.getText();
    }
    @Step
    public String getSecondServicePrice(){
        return secondServicePrice.getText();
    }
    @Step
    public String getThirdServiceName(){
        return thirdServiceName.getText();
    }
    @Step
    public String getThirdServicePrice(){
        return thirdServicePrice.getText();
    }
    @Step
    public String getFourthServiceName(){
        return fourthServiceName.getText();
    }
    @Step
    public String getFourthServicePrice(){
        return fourthServicePrice.getText();
    }
    @Step
    public String getFifthServiceName(){
        return fifthServiceName.getText();
    }
    @Step
    public String getFifthServicePrice(){
        return fifthServicePrice.getText();
    }
    @Step
    public String getOrderTotal(){
        return orderTotal.getText();
    }
    @Step
    public String getEsaPackageName(){
        return esaPackageName.getText();
    }
    @Step
    public String getEsaPackagePrice(){
        return esaPackagePrice.getText();
    }
    @Step
    public boolean getPresentsOfAdditionalService(){
        return presentsOfAdditionalService.isDisplayed();
    }
    @Step
    public String getProgressBarSize(){
        return progressBar.getAttribute("style");
    }
}
