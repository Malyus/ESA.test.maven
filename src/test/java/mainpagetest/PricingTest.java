package mainpagetest;

import allure.TestExecutionListener;
import mainpages.*;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import ru.qatools.properties.PropertyLoader;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import util.config.ServicesConfig;
import webdriver.SingletonDriver;

import static constants.PricingConstants.*;


/**
 * Created by M.Malyus on 1/29/2018.
 */
/*Test data in PricingConstants.class*/
@Listeners({ TestExecutionListener.class })
public class PricingTest {
    final static Logger LOG = Logger.getLogger(PricingTest.class);
    protected HomePage homePage;
    private ServicesConfig servicesConfig = PropertyLoader.newInstance().populate(ServicesConfig.class);
    final String firstService = servicesConfig.getFirstService();
    final String secondService = servicesConfig.getSecondService();
    final String thirdService = servicesConfig.getThirdService();
    final String fourthService = servicesConfig.getFourthService();

    public PricingTest() throws Exception {
    }

    @TestCaseId("1.1487")
    @Test
    public void checkBottomItemsForValid() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(pricingPage.getItemOfBottom(0),"ESA Certification Pricing", "FirstBottomItem is not ESA Certification Pricing!");
        softAssert.assertEquals(pricingPage.getItemOfBottom(1),"FAQ", "SecondBottomItem is not FAQ!");
        softAssert.assertEquals(pricingPage.getItemOfBottom(2),"Blog","ThirdBottomItem is not Blog!");
        softAssert.assertEquals(pricingPage.getItemOfBottom(3),"Contact Us","FourthBottomItem is not ContactUs!");
        softAssert.assertEquals(pricingPage.getItemOfBottom(4),"Terms","FifthBottomItem is not Terms!");
        softAssert.assertEquals(pricingPage.getItemOfBottom(5),"Privacy","SixthBottomItem is not Privacy!");
        softAssert.assertAll();
    }
    @TestCaseId("1.1488")
    @Test
    public void checkIfUserIsStayingOnPricingPageByClickingPricingButtonTopMenuOfPricingPage() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnPricingButtonOfTopMenu();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not on PricingPage after clicking on PricingButton of TopMenu PricingPage!");
    }
    @TestCaseId("1.1489")
    @Test
    public void checkIfUserCanBeRedirectedToFAQPageByFAQTopMenuButtonOfPricingPage() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnFaqButtonOfTopMenu();
        FAQPage faqPage = new FAQPage();
        Assert.assertTrue(faqPage.checkPresentsOfFAQPage(),"User is not on FAQPage after clicking on FAQButton of top menu!");
    }
    @TestCaseId("1.1490")
    @Test
    public void checkIfUserCanReturnBackByBrowserButtonOFFAQPAgeToPricing() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        FAQPage faqPage = new FAQPage();
        pricingPage.clickOnFaqButtonOfTopMenu();
        faqPage.turnBackByBrowserButton();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turned back from FAQPage by browser back button!");
    }
    @TestCaseId("1.491")
    @Test
    public void checkIfUserCanGoToBlogPageByBlogButtonOfTopMenu() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnBlogButtonOfTopMenu();
        BlogPage blogPage = new BlogPage();
        Assert.assertTrue(blogPage.checkPresentsOfBlogPage(),"User is not on BlogPage by BlogButton of top menu PricingPage!");
    }
    @TestCaseId("1.491")
    @Test
    public void checkIfUserCanReturnToPricingFromBlogByBackButtonOfBrowser() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnBlogButtonOfTopMenu();
        BlogPage blogPage = new BlogPage();
        blogPage.turnBackByBrowserButton();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not redirected to PricingPage by back button of browser on BlogPage!");
    }
    @TestCaseId("1.1493")
    @Test
    public void checkIfUserCanBeRedirectedToContactUsPageByClickingOnContactUsOfTopMenuPricingPage() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnContactUsButtonOfTopMenu();
        ContactUsPage contactUsPage = new ContactUsPage();
        Assert.assertTrue(contactUsPage.checkPresentsOfContactUsPage(),"User is not redirected to ContactUsPage by ContactUsButton of TopMenu of PricingPage!");
    }
    @TestCaseId("1.1494")
    @Test
    public void checkIfUserCanTurnBackFromContactUsPageToPricingByBackButtonOfBrowser() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnContactUsButtonOfTopMenu();
        ContactUsPage contactUsPage = new ContactUsPage();
        contactUsPage.turnBackToPreviousPage();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turn back from ContactUsPage to PricingPage by back button of browser!");
    }
    @TestCaseId(" 1.1494.1")
    @Test
    public void checkIfUserCanBeRedirectedToTermsPageByTermsButtonOfBottomMenu() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnTermsButtonOfBottomMenu();
        TermsPage termsPage = new TermsPage();
        LOG.info("TermsLink is:" + TERMS_LINK);
        Assert.assertEquals(termsPage.getURLOfTermsPage(),TERMS_LINK,"User is not redirected to TermsPage!");
    }
    @TestCaseId("1.1494.2")
    @Test
    public void checkIfUserCanBeTurnedBackToPricingPageFromTermsPage() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnTermsButtonOfBottomMenu();
        TermsPage termsPage = new TermsPage();
        termsPage.turnBackByBrowserButton();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turned back from TermsPage to PricingPage by browser button!");
    }
    @TestCaseId("1.1494.3")
    @Test
    public void checkIfUserCanBeRedirectedToPrivacyPageByPrivacyButtonOfBottomMenu() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnPrivacyButtonOfBottomMenu();
        PrivacyPage privacyPage = new PrivacyPage();
        LOG.info("PrivacyLink is:" + PRIVACY_LINK);
        Assert.assertEquals(privacyPage.getCurrentURL(),PRIVACY_LINK,"User is not redirected to PrivacyPage!");
    }
    @TestCaseId("1.1494.4")
    @Test
    public void checkIfUserCanTurnBackByBrowserFromPrivacyToPricingPage() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnPrivacyButtonOfBottomMenu();
        PrivacyPage privacyPage = new PrivacyPage();
        privacyPage.turnBackByBrowser();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turned back from PrivacyPage to PricingPage! ");
    }
     @TestCaseId(" 1.1647")
     @Test
     public void checkIfEsaHousingLetterContainsValidServices() throws Exception {
         homePage = new HomePage();
         homePage.clickOnCertificationPricingButton();
         PricingPage pricingPage = new PricingPage();
         Assert.assertEquals(pricingPage.getEsaHousingLetterService(0),firstService,"FirstService of ESAHousingLetter is not valid!");
         Assert.assertEquals(pricingPage.getEsaHousingLetterService(1),secondService,"SecondService of ESAHousingLetter is not valid!");
         Assert.assertEquals(pricingPage.getEsaHousingLetterService(2),thirdService,"ThirdService of ESAHousingLetter is not valid!");
         Assert.assertEquals(pricingPage.getEsaHousingLetterService(3),fourthService,"FourthService of ESAHousingLetter is not valid!");
     }
     @TestCaseId("1.1648")
     @Test
     public void checkIfComboPlanContainsValidServices() throws Exception {
         homePage = new HomePage();
         homePage.clickOnCertificationPricingButton();
         PricingPage pricingPage = new PricingPage();
         Assert.assertEquals(pricingPage.getComboPlanServices(0),firstService,"FirstService of ComboPlan is not valid!");
         Assert.assertEquals(pricingPage.getComboPlanServices(1),secondService,"SecondService of ComboPlan is not valid!");
         Assert.assertEquals(pricingPage.getComboPlanServices(2),thirdService,"ThirdService of ComboPlan is not valid!");
         Assert.assertEquals(pricingPage.getComboPlanServices(3),fourthService,"FourthService of ComboPlan is not valid!");
     }
     @TestCaseId("1.1649")
     @Test
     public void checkIfEsaTravelLetterContainsValidServices() throws Exception {
         homePage = new HomePage();
         homePage.clickOnCertificationPricingButton();
         PricingPage pricingPage = new PricingPage();
         Assert.assertEquals(pricingPage.getEsaTravelLetterServices(0),firstService,"FirstService of ESATravelLetter is not valid!");
         Assert.assertEquals(pricingPage.getEsaTravelLetterServices(1),secondService,"SecondService of ESATravelLetter is not valid!");
         Assert.assertEquals(pricingPage.getEsaTravelLetterServices(2),thirdService,"ThirdService of ESATravelLetter is not valid!");
         Assert.assertEquals(pricingPage.getEsaTravelLetterServices(3),fourthService,"FourthService of ESATravelLetter is not valid!");
     }
    @TestCaseId("1.1649")
    @Test
    public void checkForValidIncludedServicesOfHousingLetter() throws Exception {
         homePage = new HomePage();
         homePage.clickOnCertificationPricingButton();
         PricingPage pricingPage = new PricingPage();
         Assert.assertEquals(pricingPage.getIncludedServicesOfEsaHousingLetter(0),firstService,"FirstService is not included!");
         Assert.assertEquals(pricingPage.getIncludedServicesOfEsaHousingLetter(1),secondService,"SecondService is not included!");
    }
    @TestCaseId("1.1650")
    @Test
    public void checkIfAllServicesAreIncludedForComboPlan() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        Assert.assertEquals(pricingPage.getIncludedServicesOfComboPlan(0),firstService,"FirstService is not included!");
        Assert.assertEquals(pricingPage.getIncludedServicesOfComboPlan(1),secondService,"SecondService is not included!");
        Assert.assertEquals(pricingPage.getIncludedServicesOfComboPlan(2),thirdService,"ThirdService is not included!");
        Assert.assertEquals(pricingPage.getIncludedServicesOfComboPlan(3),fourthService,"FourthService is not included!");
    }
    @TestCaseId("1.1651")
    @Test
    public void checkForValidIncludedServicesOfEsaTravelLetter() throws Exception {
      homePage = new HomePage();
      homePage.clickOnCertificationPricingButton();
      PricingPage pricingPage = new PricingPage();
      Assert.assertEquals(pricingPage.getIncludedServicesOfEsaTravelLetter(0),firstService,"FirstService is not included!");
      Assert.assertEquals(pricingPage.getIncludedServicesOfEsaTravelLetter(1),thirdService,"ThirdService is not included!");
    }
    @TestCaseId("1.1652")
    @Test
    public void checkIfEsaHousingLetterPriceIsValid() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        LOG.info("FirstPrice:" + pricingPage.getPriceOfCertificate(0));
        Assert.assertEquals(pricingPage.getPriceOfCertificate(0),ESA_HOUSING_LETTER_PRICE,"EsaHousingLetterPrice is not valid!");

    }
    @TestCaseId("1.1653")
    @Test
    public void checkIfComboPlanPriceIsValid() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        LOG.info("SecondPrice:" + pricingPage.getPriceOfCertificate(1));
        Assert.assertEquals(pricingPage.getPriceOfCertificate(1),ESA_COMBO_PLAN_PRICE,"ComboPlanPrice is not valid!");
    }
    @TestCaseId("1.1654")
    @Test
    public void checkIfEsaTravelLetterPriceIsValid() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        LOG.info("ThirdPrice:" + pricingPage.getPriceOfCertificate(2));
        Assert.assertEquals(pricingPage.getPriceOfCertificate(2),ESA_TRAVEL_LETTER_PRICE,"EsaTravelLetterPrice is not valid!");
    }
    @TestCaseId("1.1527")
    @Test
    public void checkIfUserCanBeRedirectedToFAQPageByFAQBottomButton() throws Exception {
     homePage = new HomePage();
     homePage.clickOnCertificationPricingButton();
     PricingPage pricingPage = new PricingPage();
     pricingPage.clickOnFAQBottomButton();
     FAQPage faqPage = new FAQPage();
     Assert.assertTrue(faqPage.checkPresentsOfFAQPage(),"User is not redirected to FAQPage by FAQBottomButton!");
    }
    @TestCaseId("1.1528")
    @Test
    public void checkIfUserCanTurnBackFromFAQByBackButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnFAQBottomButton();
        FAQPage faqPage = new FAQPage();
        faqPage.turnBackByBrowserButton();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turnedback to PricingPage from FAQPage!");
    }
    @TestCaseId("1.1529")
    @Test
    public void checkIfUserCanBeRedirectedToBlogPageByBlogBottomButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnBlogBottomButton();
        BlogPage blogPage = new BlogPage();
        Assert.assertTrue(blogPage.checkPresentsOfBlogPage(),"User is not redirected to BlogPage by BlogBottomButton!");
    }
    @TestCaseId("1.1530")
    @Test
    public void checkIfUserCanTurnBackFromBlogPageByBackButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnBlogBottomButton();
        BlogPage blogPage = new BlogPage();
        blogPage.turnBackByBrowserButton();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turnedBack from BlogPage to PricingPage by browser back button!");
    }
    @TestCaseId("1.1531")
    @Test
    public void checkIfUserCanBeRedirectedToContactUsPageByContactUsBottomButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnContactUsBottomButton();
        ContactUsPage contactUsPage = new ContactUsPage();
        Assert.assertTrue(contactUsPage.checkPresentsOfContactUsPage(),"User is not redirected from PricingPage to ContactUsPage by contactUsBottomButton!");
    }
    @TestCaseId("1.1532")
    @Test
    public void checkIFUserCanTurnBackFromContactUsPageByBackButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnContactUsBottomButton();
        ContactUsPage contactUsPage = new ContactUsPage();
        contactUsPage.turnBackToPreviousPage();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turnedBack from ContactUsPage to PricingPage by browser back button!");
    }
    @TestCaseId("1.1533")
    @Test
    public void checkIfUserCanBeRedirectedToTermsPageByTermsBottomButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnTermsBottomButton();
        TermsPage termsPage = new TermsPage();
        Assert.assertEquals(termsPage.getURLOfTermsPage(),TERMS_LINK,"User is not redirected from PricingPage to TermsPage by termsBottomButton!");
    }
    @TestCaseId("1.1534")
    @Test
    public void checkIfUserCanTurnBackFromTermsPageByBackButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnTermsBottomButton();
        TermsPage termsPage = new TermsPage();
        termsPage.turnBackByBrowserButton();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turned back from TermsPage to PricingPage by browser back button!");
    }
    @TestCaseId("1.1535")
    @Test
    public void checkIfUserCanBeRedirectedToPrivacyPageByPrivacyBottomButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnPrivacyBottomButton();
        PrivacyPage privacyPage = new PrivacyPage();
        Assert.assertEquals(privacyPage.getCurrentURL(),PRIVACY_LINK,"User is not returned back from PricingPage to PrivacyPage!");
    }
    @TestCaseId("1.1536")
    @Test(groups={"pricing"})
    public void checkIfUserCanTurnBackFromPrivacyPageToPricingPageByBackButton() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickOnPrivacyBottomButton();
        PrivacyPage privacyPage = new PrivacyPage();
        privacyPage.turnBackByBrowser();
        Assert.assertTrue(pricingPage.checkPresentsOfPricingPage(),"User is not turned back from PrivacyPage to PricingPage by browser back button!");
    }
    @TestCaseId("1.1537")
    @Test
    public void checkForValidLeftColumnOfWhyChooseGetEsa() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        Assert.assertEquals(pricingPage.getLeftTextOfWhyChooseGetEsa(0),LEFT_COLUMN_FIRST,"LeftColumn first sentence is not valid!");
        Assert.assertEquals(pricingPage.getLeftTextOfWhyChooseGetEsa(1),LEFT_COLUMN_SECOND,"LeftColumn second sentence is not valid!");
        Assert.assertEquals(pricingPage.getLeftTextOfWhyChooseGetEsa(2),LEFT_COLUMN_THIRD,"LeftColumn third sentence is not valid!");
    }
    @TestCaseId("1.538")
    @Test
    public void checkForValidRightColumnOfWhyChooseGetEsa() throws Exception {
        homePage = new HomePage();
        homePage.clickOnCertificationPricingButton();
        PricingPage pricingPage = new PricingPage();
        Assert.assertEquals(pricingPage.getRightTextOfWhyChooseGetEsa(0),RIGHT_COLUMN_FIRST,"RightColumn first sentence is not valid!");
        Assert.assertEquals(pricingPage.getRightTextOfWhyChooseGetEsa(1),RIGHT_COLUMN_SECOND,"RightColumn second sentence is not valid!");
        Assert.assertEquals(pricingPage.getRightTextOfWhyChooseGetEsa(2),RIGHT_COLUMN_THIRD,"RightColumn third sentence is not valid!");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
