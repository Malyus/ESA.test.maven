package mainpagetest;

import allure.TestExecutionListener;
import com.browserstack.local.Local;
import mainpages.FAQPage;
import mainpages.HomePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Issue;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import webdriver.SingletonDriver;


/**
 * Created by M.Malyus on 2/1/2018.
 */
/*Good*/
@Listeners({ TestExecutionListener.class })
public class FAQTest {
    final static Logger LOG = Logger.getLogger(FAQTest.class);
    private HomePage homePage;

    public FAQTest() throws Exception {
    }
    @TestCaseId("1741")
    @Test
    public void checkIfAnswerIsDisplayedAfterClickingOnQuestion() throws Exception {
        homePage = new HomePage();
        homePage.clickOnFaqButton();
        FAQPage faqPage = new FAQPage();
        faqPage.clickOnThirdQuestion();
        Assert.assertEquals(faqPage.getActiveOfThirdAnswer(),null,"ThirdAnswer is not displayed after clicking on AnswerQuestion!");
    }
    @TestCaseId("1742")
    @Test
    public void checkIfAnswerIsHidingWhenUserClicksOnOpenedQuestion() throws Exception {
        homePage = new HomePage();
        homePage.clickOnFaqButton();
        FAQPage faqPage = new FAQPage();
        faqPage.clickOnThirdQuestion();
        Thread.sleep(1000);
        faqPage.clickOnThirdQuestion();
        Assert.assertEquals(faqPage.getActiveOfThirdAnswer(),null,"ThirdQuestion is not hide after clicking on it twice!");
    }
    @TestCaseId("1743")
    @Test
    public void checkIfAnswerIsHidingAfterClickingOnAnotherQuestion() throws Exception {
        homePage = new HomePage();
        homePage.clickOnFaqButton();
        FAQPage faqPage = new FAQPage();
        faqPage.clickOnThirdQuestion();
        Thread.sleep(1000);
        faqPage.clickOnFirstQuestionButton();
        Assert.assertEquals(faqPage.getActiveOfThirdAnswer(),null,"ThirdAnswer is not hide after clicking on another question!");
    }
    @TestCaseId("1744")
    @Test
    public void checkIfRoleWorksAfterClickingOnQuestion() throws Exception {
        homePage = new HomePage();
        homePage.clickOnFaqButton();
        FAQPage faqPage = new FAQPage();
        LOG.info("Before RotateValue:" + faqPage.getRotateValueOfThirdAnswer());
        faqPage.clickOnThirdQuestion();
        LOG.info("After RotateValue:" + faqPage.getRotateValueOfThirdAnswer());
        Thread.sleep(2000);
        Assert.assertEquals(faqPage.getRotateValueOfThirdAnswer(),"39px","Rotate does not work correctly!!");
    }
    @TestCaseId("1745")
    @Test
    public void checkIfRoleWorksAfterClickingOnOpenedQuestion() throws Exception {
        homePage = new HomePage();
        homePage.clickOnFaqButton();
        FAQPage faqPage = new FAQPage();
        faqPage.clickOnThirdQuestion();
        Thread.sleep(1000);
        faqPage.clickOnThirdQuestion();
        Thread.sleep(2000);
        Assert.assertEquals(faqPage.getRotateValueOfThirdAnswer(),"45px","Rotate does not work correctly!");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
            SingletonDriver.quit();
        }
}
