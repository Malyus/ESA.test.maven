package testbase;


import com.allure.TestExecutionListener;
import com.browserstack.local.Local;
import com.mainbo.SignInBO;
import com.mainpages.HomePage;
import com.questions.QuestionsPage;
import com.stepsbo.PersonalInformationBO;
import com.stepsbo.PetSectionBO;
import com.stepsbo.QuestionsBO;
import com.stepsbo.ShippingBO;
import com.stepspages.AdditionalServicesPage;
import com.stepspages.PersonalInformationPage;
import com.stepspages.PetSectionPage;
import com.webdriver.Page;
import com.webdriver.WebDriverFactory;
import com.webdriver.WebWindow;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

/**
 * Created by M.Malyus on 1/25/2018.
 */
@Listeners({ TestExecutionListener.class })
public class TestBase {

    protected WebDriver webDriver;
    protected Local l;
    static EventFiringWebDriver eventDriver;
    protected HomePage homePage;
    protected PersonalInformationBO personalInformationBO;
    protected PersonalInformationPage personalInformationPage;
    protected QuestionsBO questionsBO;
    protected QuestionsPage questionsPage;
    protected PetSectionPage petSectionPage;
    protected PetSectionBO petSectionBO;
    protected ShippingBO shippingBO;
    protected SignInBO signInBO;
    protected AdditionalServicesPage additionalServicesPage;
    public String handleHost;
    final static Logger LOG = org.apache.log4j.Logger.getLogger(TestExecutionListener.class);

    @BeforeMethod(alwaysRun = true)
    @Parameters({"browserName"})
    public void setup(String browserName) throws Exception {
        webDriver = WebDriverFactory.getInstance(browserName);
        webDriver.manage().window().maximize();
//		eventDriver = new EventFiringWebDriver(webDriver);
//		eventDriver.register(new EventHandler());
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        navigateTo(Page.WEB_URL);
//		webDriver.manage().deleteAllCookies();
        homePage = PageFactory.initElements(webDriver, HomePage.class);
        personalInformationPage = PageFactory.initElements(webDriver,PersonalInformationPage.class);
        personalInformationBO = PageFactory.initElements(webDriver,PersonalInformationBO.class);
        signInBO = PageFactory.initElements(webDriver,SignInBO.class);
        questionsPage = PageFactory.initElements(webDriver,QuestionsPage.class);
        questionsBO = PageFactory.initElements(webDriver,QuestionsBO.class);
        petSectionPage = PageFactory.initElements(webDriver,PetSectionPage.class);
        petSectionBO = PageFactory.initElements(webDriver,PetSectionBO.class);
        shippingBO = PageFactory.initElements(webDriver,ShippingBO.class);
        //loginPage = PageFactory.initElements(webDriver, LoginPage.class);
        handleHost = webDriver.getWindowHandle();
    }

    public void setUp(final ITestContext context) {
        context.setAttribute("driverKey", webDriver);
    }

    public void onTestFailure(final ITestResult result) {
        WebDriver driver = (WebDriver) result.getTestContext().getAttribute("driverKey");
    }

    protected void navigateTo(String URL) {
        webDriver.get(URL);
    }

//	protected void CreateNewTab() {
//		try {
//			WebWindow tab2 = new WebWindow(webDriver, "https://88.99.251.182/admin312423423423534534534534534534534534534543");
//			LOG.info("New tab is created");
//		} catch (Exception e) {
//			System.err.println(e.getMessage());
//			System.err.println("Couldn't load second page");
//			LOG.info("Couldn't load second page");
//		}
//	}

    protected void switchFromSecondTabToFirst() {
        try {
            webDriver.switchTo().window(handleHost);
            webDriver.switchTo().activeElement();
            LOG.info("Switch from second tab to first");
        } catch (Exception e) {
            System.err.println("Couldn't get back to first page");
            LOG.info("Couldn't get back to first page");
        }
    }

    protected void switchFromFirstPageToSecond() {
        try {
            for (String handle : webDriver.getWindowHandles()) {
                if (handle != handleHost) {
                    webDriver.switchTo().window(handle);
                    webDriver.switchTo().activeElement();
                    LOG.info("Switch from first tab to second");
                } // смотрим все вкладки (а их две всего); если і-тая вкладка не равна первой handleHost (инициализированой в пункте (а), тогда переключаемся на нее).
            }
        } catch (Exception e) {
            System.err.println("Couldn't get to second page");
            LOG.info("Couldn't get to second page");
        }
    }
    protected void createAndSwitchToNewTab(String url,int numberOfTab){
        WebWindow webWindow = new WebWindow(webDriver,"https://www.google.com/");
        webWindow.createAndOpenNewWindow(url,numberOfTab);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
		/*Close browser and kill instance*/
        if (webDriver != null) {
            WebDriverFactory.killDriverInstance();
        }
        if (l != null)
            l.stop();


//		if (webDriver != null) {
//			webDriver.quit();
//		}
    }
}


