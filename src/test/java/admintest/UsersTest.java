package admintest;

import adminbo.AdminLoginBO;
import adminbo.UsersBO;
import adminpage.AddNewUserPage;
import adminpage.AdminLoginPage;
import adminpage.DashboardPage;
import adminpage.UsersPage;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import ru.qatools.properties.PropertyLoader;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import util.config.AdminConfig;
import util.testdatautils.UserRandomGenerator;
import webdriver.SingletonDriver;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 4/24/2018.
 */
public class UsersTest {
    final static Logger LOG = Logger.getLogger(UsersTest.class);
    private AdminConfig adminConfig = PropertyLoader.newInstance().populate(AdminConfig.class);
    private UsersBO usersBO;
    private AdminLoginBO adminLoginBO;
    private DashboardPage dashboardPage;
    private UsersPage usersPage;
    private AddNewUserPage addNewUserPage;
    private String active = adminConfig.getActive();
    private String disable = adminConfig.getDisable();

    @TestCaseId("2.456")
    @Test
    public void checkIfAddUserScreenIsDisplayedAfterOpening() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        assertTrue(usersPage.checkPresentsOfUsersPage(),
                "Users Page is not displayed after opening");
    }
    @TestCaseId("2.457")
    @Test
    public void checkIfUserWithAllEmptyFieldsCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.clickOnSaveButton();
        assertTrue(addNewUserPage.checkPresentsOfAddNewUserPage(),
                "User can not be created when all fields are empty");
    }
    @TestCaseId("2.458")
    @Test
    public void checkIfUserWithEmptyPasswordAndEmailCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        int countOfUsers = usersPage.getCountOfUsers();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeName("Admin");
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        assertEquals(usersPage.getCountOfUsers(),countOfUsers,
                "User can not be created with empty password and email");
    }
    @TestCaseId("2.459")
    @Test
    public void checkIfUserWithEmptyNameAndPasswordCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        int countOfUsers = usersPage.getCountOfUsers();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeEmail("test@gmail.com");
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        assertEquals(usersPage.getCountOfUsers(),countOfUsers,
                "User can not be created with empty password and name");
    }
    @TestCaseId("2.460")
    @Test
    public void checkIfUserWithEmptyEmailAndNameCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        int countOfUsers = usersPage.getCountOfUsers();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writePassword("qwerty");
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        assertEquals(usersPage.getCountOfUsers(),countOfUsers,
                "User can not be created with empty email and name");
    }
    @TestCaseId("2.461")
    @Test
    public void checkIfUserWithEmptyPasswordCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        int countOfUsers = usersPage.getCountOfUsers();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeName("test");
        addNewUserPage.writeEmail(UserRandomGenerator.generateRandomUser() + "@test.com");
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        assertEquals(usersPage.getCountOfUsers(),countOfUsers,
                "User can not be created with empty password");
    }
    @TestCaseId("2.462")
    @Test
    public void checkIfUserWithEmptyEmailCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        int countOfUsers = usersPage.getCountOfUsers();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeName("test");
        addNewUserPage.writePassword(UserRandomGenerator.generateRandomUser());
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        assertEquals(usersPage.getCountOfUsers(),countOfUsers,
                "User can not be created with empty email");
    }
    @TestCaseId("2.463")
    @Test
    public void checkIfUserWithEmptyNameCanNotBeSaved() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        int countOfUsers = usersPage.getCountOfUsers();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeEmail(UserRandomGenerator.generateRandomUser() + "@test.com");
        addNewUserPage.writePassword(UserRandomGenerator.generateRandomUser());
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        assertEquals(usersPage.getCountOfUsers(),countOfUsers,
                "User can not be created with empty name");
    }
    @TestCaseId("2.464-2.465")
    @Test
    public void checkIfUserWithValidDataCanBeCreated() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        String userEmail = UserRandomGenerator.generateRandomUser() + "@test.com";
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeName("test");
        addNewUserPage.writeEmail(userEmail);
        addNewUserPage.writePassword(UserRandomGenerator.generateRandomUser());
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        softAssert.assertEquals(usersPage.getLastUserName(),userEmail,
                "User is not created wen all data is valid");
        usersPage.deleteCreatedUser();
    }
    @TestCaseId("2.464-2.466")
    @Test
    public void checkIfUserEmailThatWasAddedDuringCreationIsOnUsersPAge() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        String userEmail = UserRandomGenerator.generateRandomUser() + "@test.com";
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        usersPage = dashboardPage.clickOnUsersMenuButton();
        addNewUserPage = usersPage.clickOnAddNewUser();
        addNewUserPage.writeName("test");
        addNewUserPage.writeEmail(userEmail);
        addNewUserPage.writePassword(UserRandomGenerator.generateRandomUser());
        addNewUserPage.clickOnSaveButton();
        dashboardPage.clickOnUsersMenuButton();
        softAssert.assertEquals(usersPage.getLastUserName(),userEmail,
                "User is not created wen all data is valid");
        usersPage.deleteCreatedUser();
    }
    /*Today*/
    @TestCaseId("2.467")
    @Test
    public void checkIfTypeIsAdminOnUsersPageAfterCreatingUserWithAdminType() throws Exception {
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","admin");
        LOG.info("User admin active:" + usersPage.getActiveOfCreatedUserType("admin"));
        assertEquals(usersPage.getActiveOfCreatedUserType("admin"),active,
                "User type is not admin after creating");
    }
    @TestCaseId("2.468")
    @Test
    public void checkIfTypeIsManagerOnUsersPageAfterCreatingUserWithManagerType() throws Exception {
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","manager");
        LOG.info("User manager active:" + usersPage.getActiveOfCreatedUserType("manager"));
        assertEquals(usersPage.getActiveOfCreatedUserType("manager"),active,
                "User type is not manager after creating");
    }
    @TestCaseId("2.469")
    @Test
    public void checkIfTypeIsShipperOnUsersPageAfterCreatingUserWithShipperType() throws Exception {
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","shipper");
        LOG.info("User shipper active:" + usersPage.getActiveOfCreatedUserType("shipper"));
        assertEquals(usersPage.getActiveOfCreatedUserType("shipper"),active,
                "User type is not shipper after creating");
    }
    @TestCaseId("2.476")
    @Test
    public void checkIfUserCanEditAndSaveEmail() throws Exception {
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","admin");
        addNewUserPage = usersPage.editCreatedUser();
        String newEmail = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        addNewUserPage.writeEmail(newEmail);
        addNewUserPage.writeName("test");
        addNewUserPage.clickOnSaveButton();
        assertEquals(usersPage.getLastUserName(),newEmail,
                "User can not edit and save email");
    }
    @TestCaseId("2.477")
    @Test
    public void checkIfUserCanLogInWithNewEmail() throws Exception {
        dashboardPage = new DashboardPage();
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","admin");
        addNewUserPage = usersPage.editCreatedUser();
        String newEmail = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        addNewUserPage.writeEmail(newEmail);
        addNewUserPage.writeName("test");
        addNewUserPage.clickOnSaveButton();
        AdminLoginPage adminLoginPage = dashboardPage.clickOnLogOutButton();
        adminLoginPage.writeEmail(newEmail);
        adminLoginPage.writePassword("pass");
        adminLoginPage.clickOnSubmitButton();
        assertTrue(dashboardPage.checkPresentsOfDashboardPage(),
                "User can not log in with new email");
    }
    /*Today*/
    @TestCaseId("2.478")
    @Test
    public void checkIfUserCanLogInWithNewPassword() throws Exception {
        dashboardPage = new DashboardPage();
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","admin");
        addNewUserPage = usersPage.editCreatedUser();
        String newPassword = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        addNewUserPage.writePassword(newPassword);
        addNewUserPage.writeName("test");
        addNewUserPage.clickOnSaveButton();
        AdminLoginPage adminLoginPage = dashboardPage.clickOnLogOutButton();
        adminLoginPage.writeEmail(email);
        adminLoginPage.writePassword(newPassword);
        adminLoginPage.clickOnSubmitButton();
        assertTrue(dashboardPage.checkPresentsOfDashboardPage(),
                "User can not log in with new email");
    }
    @TestCaseId("2.485")
    @Test
    public void checkIfUserWithManagerTypeCanLogIn() throws Exception {
        dashboardPage = new DashboardPage();
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","manager");
        dashboardPage.clickOnLogOutButton();
        AdminLoginPage adminLoginPage = new AdminLoginPage();
        adminLoginPage.writeEmail(email);
        adminLoginPage.writePassword("pass");
        adminLoginPage.clickOnSubmitButton();
        assertTrue(dashboardPage.checkPresentsOfDashboardPage(),
                "User can not log in with manager type");
    }
    @TestCaseId("2.486")
    @Test
    public void checkIfUserWithShipperTypeCanLogIn() throws Exception {
        dashboardPage = new DashboardPage();
        String email = UserRandomGenerator.generateRandomUser() + "test@gmail.com";
        usersBO = new UsersBO();
        usersPage = usersBO.createUser("test",email,"pass","shipper");
        dashboardPage.clickOnLogOutButton();
        AdminLoginPage adminLoginPage = new AdminLoginPage();
        adminLoginPage.writeEmail(email);
        adminLoginPage.writePassword("pass");
        adminLoginPage.clickOnSubmitButton();
        assertTrue(dashboardPage.checkPresentsOfOrdersPage(),
                "User can not log in with shipper type");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
