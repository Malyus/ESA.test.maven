package admintest;

import adminbo.AdminLoginBO;
import adminpage.ClientsPage;
import adminpage.DashboardPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import webdriver.SingletonDriver;

import static org.testng.Assert.assertEquals;

/**
 * Created by M.Malyus on 5/10/2018.
 */
public class ClientsTest {
    private AdminLoginBO adminLoginBO;
    private DashboardPage dashboardPage;
    private ClientsPage clientsPage;

    @TestCaseId("2.228")
    @Test
    public void checkIfTwentyIsSelectedByDefaultInItemsInListMenu() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        assertEquals(clientsPage.getSelectedOptionOfItemsListMenu(),"20",
                "20 is not selected by default in items in list menu");
    }
    @TestCaseId("2.229")
    @Test
    public void checkIfItemsInListMenuContainsAllFiveValues() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        softAssert.assertEquals(clientsPage.getOptionItemOfListMenu(0),"20",
                "20 option is not displayed in items in list menu");
        softAssert.assertEquals(clientsPage.getOptionItemOfListMenu(1), "50",
                "50 option is not displayed in items in list menu");
        softAssert.assertEquals(clientsPage.getOptionItemOfListMenu(2),"75",
                "75 option is not displayed in items in list menu");
        softAssert.assertEquals(clientsPage.getOptionItemOfListMenu(3),"100",
                "100 option is not displayed in items in list menu");
        softAssert.assertEquals(clientsPage.getOptionItemOfListMenu(4),"200",
                "200 option is not displayed in items in list menu");
        softAssert.assertAll();
    }
    @TestCaseId("2.233")
    @Test
    public void checkIfAfterSelectingTwentyOptionItBecomesSelected() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        /*Select first fifty because twenty
        * is selected by default*/
        clientsPage.selectItemsInListByValue("50");
        clientsPage.selectItemsInListByValue("20");
        assertEquals(clientsPage.getSelectedOptionOfItemsListMenu(),"20",
                "20 is not displayed in items list menu after selecting");
    }
    @TestCaseId("2.234")
    @Test
    public void checkIfAfterSelectingFiftyOptionItBecomesSelected() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("50");
        assertEquals(clientsPage.getSelectedOptionOfItemsListMenu(),"50",
                "50 is not displayed in items list menu after selecting");
    }
    @TestCaseId("2.235")
    @Test
    public void checkIfAfterSelectingSeventyFiveOptionItBecomesSelected() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("75");
        assertEquals(clientsPage.getSelectedOptionOfItemsListMenu(),"75",
                "75 is not displayed in items list menu after selecting");
    }
    @TestCaseId("2.236")
    @Test
    public void checkIfAfterSelectingOneHundredOptionItBecomesSelected() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("100");
        assertEquals(clientsPage.getSelectedOptionOfItemsListMenu(),"100",
                "100 is not displayed in items list menu after selecting");
    }
    @TestCaseId("2.237")
    @Test
    public void checkIfAfterSelectingTwoHundredOptionItBecomesSelected() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("200");
        assertEquals(clientsPage.getSelectedOptionOfItemsListMenu(),"200",
                "200 is not displayed in items list menu after selecting");
    }
    /*Assert need update*/
    @TestCaseId("2.238")
    @Test
    public void checkIfSelectedItemsIsDisplayedTwentyAfterClickingOnSelectAll() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.clickOnSelectAllButton();
        assertEquals(clientsPage.getCountOfSelectedItems(),"24",
                "Selected items count is not 20 after selecting all when items in list is 20");
    }
    /*Assert need update*/
    @TestCaseId("2.239")
    @Test
    public void checkIFSelectedItemsIsDisplayedFiftyAfterClickingOnSelectAll() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("50");
        clientsPage.clickOnSelectAllButton();
        assertEquals(clientsPage.getCountOfSelectedItems(),"54",
                "Selected items count is not 50 after selecting all when items in list is 50");
    }
    /*Assert need update*/
    @TestCaseId("2.240")
    @Test
    public void checkIfSelectedItemsIsDisplayedSeventyFiveAfterClickingOnSelectAll() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("75");
        clientsPage.clickOnSelectAllButton();
        assertEquals(clientsPage.getCountOfSelectedItems(),"79",
                "Selected items count is not 75 after selecting all when items in list is 75");
    }
    @TestCaseId("2.241")
    @Test
    public void checkIfSelectedItemsIsDisplayedOneHundredAfterClickingOnSelectAll() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("100");
        clientsPage.clickOnSelectAllButton();
        assertEquals(clientsPage.getCountOfSelectedItems(),"104",
                "Selected items count is not 100 after selecting all when items in list is 100");
    }
    @TestCaseId("2.242")
    @Test
    public void checkIfSelectedItemsIsDisplayedTwoHundredAfterClickingOnSelectAll() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectItemsInListByValue("200");
        clientsPage.clickOnSelectAllButton();
        assertEquals(clientsPage.getCountOfSelectedItems(),"204",
                "Selected items count is not 200 after selecting all when items in list is 200");
    }
    @TestCaseId("2.260")
    @Test
    public void checkIfAfterSelectingFirstClientSelectedItemsIsDisplayedOne() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectFirstClient();
        assertEquals(clientsPage.getCountOfSelectedItems(),"1",
                "Selected items count is not 200 after selecting one user");
    }
    @TestCaseId("2.261")
    @Test
    public void checkIfAfterSelectingTwoClientsSelectedItemsIsDisplayedTwo() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectClients(2);
        assertEquals(clientsPage.getCountOfSelectedItems(),"2",
                "Selected items count is not 2");
    }
    @TestCaseId("2.262")
    @Test
    public void checkIfAfterSelectingTenClientsSelectedItemsIsDisplayedTen() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectClients(10);
        assertEquals(clientsPage.getCountOfSelectedItems(),"10",
                "Selected items count is not 10");

    }
    @TestCaseId("2.267")
    @Test
    public void checkIfAfterSelectingFiveOddClients() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectClients(5);
        assertEquals(clientsPage.getCountOfSelectedItems(),"5",
                "Selected items count is not 5 after selecting first five odd clients");
    }
    @TestCaseId("2.268")
    @Test
    public void checkIfAfterSelectingFivePairClients() throws Exception {
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        clientsPage.selectPairedClients(5);
        assertEquals(clientsPage.getCountOfSelectedItems(),"5",
                "Selected items count is not 5 after selecting first five pair clients");
    }
    @TestCaseId("2.269")
    @Test
    public void checkISortByDropDownListContainsTwoParametersAZAndZA() throws Exception {
        SoftAssert softAssert = new SoftAssert();
        adminLoginBO = new AdminLoginBO();
        dashboardPage = adminLoginBO.loginToAdminPanel();
        clientsPage = dashboardPage.clickOnClientsMenuButton();
        softAssert.assertEquals(clientsPage.getOptionsOfSortByLetters(0),"Name A-Z",
                "First option of sort by letters is not valid");
        softAssert.assertEquals(clientsPage.getOptionsOfSortByLetters(1), "Name Z-A",
                "Second option of sort by letters is not valid");
        softAssert.assertAll();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
