package stepstest;

import allure.TestExecutionListener;
import constants.ShippingAddressConstants;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PetSectionBO;
import stepsbo.ShippingBO;
import stepspage.AdditionalServicesPage;
import stepspage.PaymentDetailsPage;
import stepspage.ShippingAddressPage;
import webdriver.SingletonDriver;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 2/20/2018.
 */
/*Good*/
@Listeners({TestExecutionListener.class})
public class ShippingAddressTest {
    private PetSectionBO petSectionBO;
    private ShippingBO shippingBO;

    final static Logger LOG = Logger.getLogger(ShippingAddressTest.class);
    private ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();
    private String street = shippingAddressConstants.getStreet();
    private String city = shippingAddressConstants.getCity();
    private String state = shippingAddressConstants.getState();
    private String zip = shippingAddressConstants.getZip();
    private String streetChico = shippingAddressConstants.getChicoStreet();
    private String cityChico = shippingAddressConstants.getChicoCity();
    private String stateChico = shippingAddressConstants.getChicoState();
    private String zipChico = shippingAddressConstants.getChicoZip().toString();

    public ShippingAddressTest() throws Exception {
    }

    /*Check order of shipping fields for valid*/
    @TestCaseId("1.466")
    @Test
    public void checkIfFieldsOrderIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        assertEquals(shippingAddressPage.getNameOfShippingPageElements(0), "Street Address", "First field is not valid");
        assertEquals(shippingAddressPage.getNameOfShippingPageElements(1), "City", "Second field is not valid");
        assertEquals(shippingAddressPage.getNameOfShippingPageElements(2), "State", "Third field is not valid");
        assertEquals(shippingAddressPage.getNameOfShippingPageElements(3), "Zip Code", "Fourth field is not valid");
    }

    /*This test need only update entry data for city 100 symbols*/
    @TestCaseId("1.467")
    @Test
    public void checkIfAddressFieldCanContainsOneHundredSymbols() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(), "System does not allow 100 symbols in address field");
    }

    /*This test need only update entry data for city 20 symbols*/
    @TestCaseId("1.478")
    @Test
    public void checkIfCityCanContainsTwentySymbols() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        Assert.assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(), "System does not allow city with 20 symbols");
    }
    @TestCaseId("1.480")
    @Test
    public void checkIfUserCanEditAlreadySavedCityField() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        /*Find skip bug of TestNG*/
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        assertTrue(true);
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        shippingAddressPage.clearAndWriteNewCity("NewCity");
        Assert.assertEquals(shippingAddressPage.getActiveOfContinueButton(),null,
            "System does not allow to edit city field");
    }

    /*Test need edit only for state entry data with 20 symbols*/
    @TestCaseId("1.487")
    @Test
    public void checkIfUserCanEnterTwentySymbolsInStateField() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(), "System does not allow to enter 20 symbols in state field");
    }
    @TestCaseId("1.489")
    @Test
    public void checkIfUserCanEditAlreadySavedState() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        shippingAddressPage.clearAndWriteState("NewState");
        assertEquals(shippingAddressPage.getActiveOfContinueButton(),null,"System does not allow to edit saved state");
    }
    @TestCaseId("1.496")
    @Test
    public void checkIfSystemAllowTwelveDigitsInZipField() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(streetChico, cityChico, stateChico, zipChico);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(), "System does not allow twelve symbols in zip field");
    }
    @TestCaseId("1.498")
    @Test
    public void checkIfUserCanEditSavedZipCode() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        assertTrue(true);
        shippingBO.writeShippingAddressValues(streetChico,cityChico,stateChico,zipChico);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        shippingAddressPage.clearAndWriteZip("123");
        assertEquals(shippingAddressPage.getActiveOfContinueButton(),null,"System does not allow twelve symbols in zip field");
    }
    @TestCaseId("1.504")
    @Test
    public void checkIfAddressValueIsSavedAfterTurningBack() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        LOG.info("StreetValue:" + shippingAddressPage.getValueOfStreetAddress());
        assertEquals(shippingAddressPage.getValueOfStreetAddress(), "", "ShippingAddress is not saved after turning back");
    }
    @TestCaseId("1.508")
    @Test
    public void checkIfCityIsSavedAfterTurningBack() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        LOG.info("CityValue:" + shippingAddressPage.getValueOfCityInput());
        assertEquals(shippingAddressPage.getValueOfCityInput(), "", "ShippingCity is not saved after turning back");
    }
    @TestCaseId("1.512")
    @Test
    public void checkIfStateIsSavedAfterTurningBack() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        LOG.info("StateValue:" + shippingAddressPage.getValueOfStateInput());
        assertEquals(shippingAddressPage.getValueOfStateInput(), "", "ShippingState is not saved after turning back");
    }
    @TestCaseId("1.516")
    @Test
    public void checkIfZipIsSavedAfterTurningBack() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        paymentDetailsPage.clickOnBackButton();
        LOG.info("ZipValue:" + shippingAddressPage.getValueOfZipInput());
        assertEquals(shippingAddressPage.getValueOfZipInput(), "", "ShippingZip is not saved after turning back");
    }
    @TestCaseId("1.525")
    @Test
    public void checkIfContinueIsNotActiveWhenAddressIsEmpty() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues("", city, state, zip);
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when address is empty");
    }
    @TestCaseId("1.526")
    @Test
    public void checkIfContinueIsNotActiveWhenAddressIsFilledWithSpace() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(" ", city, state, zip);
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when address is with space");
    }
    @TestCaseId("1.533")
    @Test
    public void checkIfContinueButtonIsNotActiveWhenCityIsEmpty() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, "", state, zip);
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when city is empty");
    }
    @TestCaseId("1.534")
    @Test
    public void checkIfContinueButtonIsNotActiveWhenCityIsWithSpace() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, " ", state, zip);
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when city is empty");
    }
    @TestCaseId("1.540")
    @Test
    public void checkIfContinueButtonIsNotActiveWhenStateIsEmpty() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, "", zip);
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when state is empty");
    }
    @TestCaseId("1.541")
    @Test
    public void checkIfContinueButtonIsNotActiveWhenStateIsWithSpace() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, " ", zip);
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when state is with space");
    }
    @TestCaseId("1.548")
    @Test
    public void checkIfContinueButtonIsNotActiveWhenZipIsEmpty() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, "");
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when zip is empty");
    }
    @TestCaseId("1.549")
    @Test
    public void checkIfContinueButtonIsNotActiveWhenZipIsWithSpace() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, " ");
        assertEquals(shippingAddressPage.getActiveOfContinueButton(), "true", "Continue button is active when zip is filled with sapce");
    }
    @TestCaseId("1.552")
    @Test
    public void checkIfConfirmShippingAddressIsDisplayedAfterClickOnContinueButton() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        assertTrue(shippingAddressPage.checkPresentsOfModalWindow(), "ConfirmShippingAddress is not displayed after clicking onb Continue button");
    }
    @TestCaseId("1.555")
    @Test
    public void checkIfAfterClickingOnEditButtonShippingAddressPageIsDisplayed() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnEditButton();
        assertTrue(shippingAddressPage.checkPresentsOfShippingAddressPage(), "ShippingAddressPage is not displayed after click on Edit button");
    }
    @TestCaseId("1.556")
    @Test
    public void checkIfAfterClickingOnEditButtonConfirmShippingAddressPopUpIsClosed() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnEditButton();
        Thread.sleep(1500);
        assertEquals(shippingAddressPage.checkPresentsOfConfirmAddressPopUp(), 1, "ConfirmShippingAddressPopUp is displayed after click on Edit button");
    }
    @TestCaseId("1.558")
    @Test
    public void checkIfAfterClickingOnCloseButtonShippingAddressPageIsDisplayed() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnModalCloseButton();
        assertTrue(shippingAddressPage.checkPresentsOfShippingAddressPage(), "ShippingAddressPage is not displayed after click on Close button of ConfirmAddressPopUp");
    }
    //Bug or new requirements
    @TestCaseId("1.591")
    @Test
    public void checkIfEnteredDataIsNotSavedAfterBackButton() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street, city, state, zip);
        AdditionalServicesPage additionalServicesPage = shippingAddressPage.clickOnBackButton();
        additionalServicesPage.clickOnContinueButton();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(shippingAddressPage.getValueOfStreetAddress(), "8727 S HARVARD BLVD",
                "StreetAddress is not empty after turning back");
        softAssert.assertEquals(shippingAddressPage.getValueOfCityInput(), "LOS ANGELES",
                "City is not empty after turning back");
        softAssert.assertEquals(shippingAddressPage.getValueOfStateInput(), "CA",
                "State is not empty after turning back");
        softAssert.assertEquals(shippingAddressPage.getValueOfZipInput(), "95928-5432",
                "Zip is not empty after turning back");
        softAssert.assertAll();
    }
    @TestCaseId("1.593")
    @Test
    public void checkIfSizeOfProgressBarOfShippingAddressIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingAddressPage.checkPresentsOfShippingAddressPage();
        assertEquals(shippingAddressPage.getSizeOfProgressBar(), "width: 70%;");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
