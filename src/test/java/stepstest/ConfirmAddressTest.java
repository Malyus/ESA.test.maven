package stepstest;

import allure.TestExecutionListener;
import constants.ShippingAddressConstants;
import org.apache.log4j.Logger;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PetSectionBO;
import stepsbo.ShippingBO;
import stepspage.PaymentDetailsPage;
import stepspage.ShippingAddressPage;
import webdriver.SingletonDriver;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 2/28/2018.
 */
/*Good*/
@Listeners({ TestExecutionListener.class })
public class ConfirmAddressTest {
    protected PetSectionBO petSectionBO;
    protected ShippingBO shippingBO;

    final static Logger LOG = Logger.getLogger(ConfirmAddressTest.class);
    private ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();
    private String street = shippingAddressConstants.getStreet();
    private String city = shippingAddressConstants.getCity();
    private String state = shippingAddressConstants.getState();
    private String zip = shippingAddressConstants.getZip();
    private String streetChico = shippingAddressConstants.getChicoStreet();
    private String cityChico = shippingAddressConstants.getChicoCity();
    private String stateChico = shippingAddressConstants.getChicoState();
    private String zipChico = shippingAddressConstants.getChicoZip().toString();
    private String addressErrorMessage = "Address Not Found.";
    private String houseErrorMessage = "Default address: The address you entered was found but more information is needed (such as an apartment, suite, or box number)" +
            " to match to a specific address.";
    private String allInvalidErrorMessage = "Invalid City.";

    public ConfirmAddressTest() throws Exception {
    }

    @TestCaseId("1.559")
    @Test
    public void checkIfAfterClickingOnXButtonConfirmShippingAddressIsClosed() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnModalCloseButton();
        assertFalse(shippingAddressPage.checkPresentsOfModalWindowWithoutWait());
    }
    @TestCaseId("1.561")
    @Test
    public void checkIfContinueButtonOfShippingAddressConfirmPopUpIsActiveWhenAllValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getActiveOfConfirmAddressContinueButton(),null,
                "Continue button of ConfirmAddress is not active when all valid");
    }
    @TestCaseId("1.565")
    @Test
    public void checkIfValidAddressIsInAddressFieldOfShippingConfirmPopUp() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingAddressField(),street,
                "Value of ConfirmShippingAddressField is not valid");
    }
    @TestCaseId("1.566")
    @Test
    public void checkIfValidCityInCityFieldOfShippingConfirmPopUp() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingCityField(),city,
                "Value of ConfirmShippingCityField is not valid");
    }
    @TestCaseId("1.567")
    @Test
    public void checkIfValidStateInStateFieldOfShippingConfirmPopUp() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingStateField(),state,
                "Value of ConfirmShippingStateField is not valid");
    }
    @TestCaseId("1.568")
    @Test
    public void checkIfValidZipInZipFieldOfShippingConfirmPopUp() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.waitForPageValuesInitialization();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingZipField(),"90047-3316",
                "Value of ConfirmShippingZipField is not valid");
    }
    @TestCaseId("1.569")
    @Test
    public void checkIfEditedConfirmShippingAddressFieldIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnEditButton();
        shippingAddressPage.clearStreet();
        shippingAddressPage.clearCity();
        shippingAddressPage.clearState();
        shippingAddressPage.clearZip();
        shippingBO.writeShippingAddressValues(streetChico,cityChico,stateChico,zipChico);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingAddressField(),streetChico,
                "Edited ConfirmShippingAddress is not valid");
    }
    @TestCaseId("1.570")
    @Test
    public void checkIfEditedConfirmShippingCityFieldIsValid() throws Exception {
       petSectionBO = new PetSectionBO();
       shippingBO = new ShippingBO();
       ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
       shippingBO.writeShippingAddressValues(street,city,state,zip);
       shippingAddressPage.clickOnContinueButton();
       shippingAddressPage.clickOnEditButton();
       shippingAddressPage.clearStreet();
       shippingAddressPage.clearCity();
       shippingAddressPage.clearState();
       shippingAddressPage.clearZip();
       shippingBO.writeShippingAddressValues(streetChico,cityChico,stateChico,zipChico);
       shippingAddressPage.clickOnContinueButton();
       assertEquals(shippingAddressPage.getValueOfConfirmShippingCityField().toLowerCase(),cityChico.toLowerCase(),
               "Edited ConfirmShippingCity is not valid");
    }
    @TestCaseId("1.571")
    @Test
    public void checkIfEditedConfirmShippingStateFieldIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnEditButton();
        shippingAddressPage.clearStreet();
        shippingAddressPage.clearCity();
        shippingAddressPage.clearState();
        shippingAddressPage.clearZip();
        shippingBO.writeShippingAddressValues(streetChico,cityChico,stateChico,zipChico);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingStateField().toLowerCase(),
                stateChico.toLowerCase(),
                "Edited ConfirmShippingState is not valid");
    }
    @TestCaseId("1.572")
    @Test
    public void checkIfEditedConfirmShippingZipFieldIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage  = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        shippingAddressPage.clickOnEditButton();
        shippingAddressPage.clearStreet();
        shippingAddressPage.clearCity();
        shippingAddressPage.clearState();
        shippingAddressPage.clearZip();
        shippingBO.writeShippingAddressValues(streetChico,cityChico,stateChico,zipChico);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getValueOfConfirmShippingZipField(),"95928-5432",
                "Edited ConfirmShippingZip is not valid");
    }
    @TestCaseId("1.575")
    @Test
    public void checkIfValidErrorMessageIsDisplayedWhenAddressIsNotValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues("asd",city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getInvalidCityErrorMessage(),"Address Not Found.",
                "AddressErrorMessage is not valid");
    }
    //City is auto-complete we can write anything in City field
    @TestCaseId("1.576")
    @Test
    public void checkIfValidErrorMessageIsDisplayedWhenCityIsNotValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,"asd",state,zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getInvalidCityErrorMessage(),"Address Not Found.",
                "CityErrorMessage is not valid");
    }
    @TestCaseId("1.577")
    @Test
    //Autocomplete for state
    public void checkIfValidErrorMessageIsDisplayedWhenStateIsNotValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,"asd",zip);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getInvalidCityErrorMessage(),"Address Not Found.",
                "StateErrorMessage is not valid");
    }
    @TestCaseId("1.578")
    @Test
    //Autocomplete for zip
    public void checkIfValidErrorMessageIsDisplayedWhenZipIsNotValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,"234234234");
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getErrorMessageText(),"",
                "ZipErrorMessage is not valid");
    }
    @TestCaseId("1.579")
    @Test
    public void checkIfValidErrorMessageIsDisplayedWhenAddressWithoutNumberOfHouse() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues("400 MISSION RANCH BLVD APT",cityChico,stateChico,zipChico);
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getErrorMessageText(),houseErrorMessage,
            "Error message is invalid when address without number of house");
}
    @TestCaseId("1.580")
    @Test
    public void checkIfValidErrorMessageIsDisplayedWhenAllFieldsAreNotValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues("asd","asd","asd","asd");
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getInvalidCityErrorMessage(),allInvalidErrorMessage,
                "AllInvalidErrorMessage is not valid");
    }
    @TestCaseId("1.581")
    @Test
    public void checkIfValidErrorMessageIsDisplayedWhenOnlyStateIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues("asd","asd","CA","asd");
        shippingAddressPage.clickOnContinueButton();
        assertEquals(shippingAddressPage.getInvalidCityErrorMessage(),allInvalidErrorMessage,
                "AllInvalidErrorMessage is not valid when only state is valid");
    }
    @TestCaseId("1.582")
    @Test
    public void checkIfValidErrorMessageIsDisplayedWhenStateAndCityIsValid() throws Exception {
       petSectionBO = new PetSectionBO();
       shippingBO = new ShippingBO();
       ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
       shippingBO.writeShippingAddressValues("asd","Los Angels","CA","asd");
       shippingAddressPage.clickOnContinueButton();
       assertEquals(shippingAddressPage.getErrorMessageNotFound(), addressErrorMessage,
            "AddressErrorMessage is not valid when state and city is valid");
}
    @TestCaseId("1.587")
    @Test
    public void checkIfPaymentDetailsIsDisplayedWhenAllShippingValuesAreValid() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingBO.writeShippingAddressValues(street,city,state,zip);
        shippingAddressPage.clickOnContinueButton();
        PaymentDetailsPage paymentDetailsPage = shippingAddressPage.clickOnModalContinueButton();
        assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),"System does not redirect to PaymentDetailsPage when all valid");
    }
    @TestCaseId("1.589")
    @Test
    public void checkIfAdditionalServicesIsDisplayedAfterClickingBackButton() throws Exception {
        petSectionBO = new PetSectionBO();
        shippingBO = new ShippingBO();
        ShippingAddressPage shippingAddressPage = petSectionBO.goToShippingPage();
        shippingAddressPage.clickOnBackButton();
        assertTrue(shippingAddressPage.checkPresentsOfPreviousPage(),
                "System does not redirect to AdditionalServicesPage after clicking on back button");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }

}
