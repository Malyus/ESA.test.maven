package stepstest;

import constants.ShippingAddressConstants;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PrintDetailsBO;
import stepspage.PrintDetailsPage;
import util.testdatautils.Date;
import webdriver.SingletonDriver;

import java.io.IOException;

import static constants.PricingConstants.ESA_COMBO_PLAN_PRICE;
import static constants.ServicesConstants.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

/**
 * Created by M.Malyus on 4/13/2018.
 */
public class PrintDetailsTest {
    private Date date = new Date();
    private PrintDetailsPage printDetailsPage;
    private PrintDetailsBO printDetailsBO;
    private String orderDate = date.getCurrentDate();
    private ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();

    public PrintDetailsTest() throws IOException, InvalidFormatException {
    }

    /*Update assert after fixing, change it for orderDate var*/
    @TestCaseId("1.898")
    @Test
    public void checkOrderDateForValid() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getOrderDateValue(),"09/12/17",
                "Date is not valid on Print Details Page");
    }
    @TestCaseId("1.899")
    @Test
    public void checkIfOrdeIdIsCreated() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        /*We check if order id field is not empty*/
        assertFalse(printDetailsPage.checkPresentsOfOrderId(),
                "Order id is not generated");
    }
    @TestCaseId("1.900")
    @Test
    public void checkIfFullNameIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getFullNameText(),"Alex Mason",
                "Full Name on Print Details Page is not correct");

    }
    @TestCaseId("1.902-1.903")
    @Test
    public void checkIfStreetIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getStreetAddressText(),shippingAddressConstants.getStreet(),
                "Street is not correct on Print Details Page");
    }
    @TestCaseId("1.904")
    @Test
    public void checkIfCityIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getCityText(),shippingAddressConstants.getCity(),
                "City is not correct on Print Details Page");
    }
    @TestCaseId("1.905")
    @Test
    public void checkIfStateIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getStateField(),shippingAddressConstants.getState(),
                "State is not correct on Print Details Page");
    }
    @TestCaseId("1.906")
    @Test
    public void checkIfZipIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getZipField(),"90047-3316",
                "Zip is not correct on Print Details Page");
    }
    @TestCaseId("PrintDetails")
    @Test
    public void checkIfComboPlanPriceIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getEsaPackagePrice(),ESA_COMBO_PLAN_PRICE + ".00",
                "Combo plan price is not valid on Print Details Page");
    }
    @TestCaseId("PrintDetails")
    @Test
    public void checkIfFirstServicePriceIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getFirstServicePrice(),FIRST_SERVICE_PRICE + ".00",
                "First service price is not valid on Print Details Page");
    }
    @TestCaseId("PrintDetails")
    @Test
    public void checIfSecondServicePriceIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getSecondServicePrice(), SECOND_SERVICE_PRICE + ".00",
                "Second service price is not valid on Print Details Page");
    }
    @TestCaseId("Print Details")
    @Test
    public void checkIfThirdServicePriceIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getThirdServicePrice(),THIRD_SERVICE_PRICE + ".00",
                "Third service price is not valid on Print Details Page");
    }
    @TestCaseId("Print Details")
    @Test
    public void checkIfFourthServicePriceIsCorrect() throws Exception {
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getFourthServicePrice(),FOURTH_SERVICE_PRICE + ".00",
                "Fourth service price is not valid on Print Details Page");
    }
    @TestCaseId("Print Details")
    @Test
    public void checkIfFifthServicePriceIsCorrect() throws Exception {
        assertEquals(true,true);
        printDetailsBO = new PrintDetailsBO();
        printDetailsPage = printDetailsBO.goToPrintDetailsPage();
        assertEquals(printDetailsPage.getFifthServicePrice(),FIFTH_SERVICE_PRICE + ".00",
                "Fifth service price is not valid on Print Details Page");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
