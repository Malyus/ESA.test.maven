package stepstest;

import allure.TestExecutionListener;
import org.testng.annotations.*;
import questions.QuestionsPage;
import stepsbo.PersonalInformationBO;
import webdriver.SingletonDriver;

/**
 * Created by M.Malyus on 2/5/2018.
 */
/*Good*/
@Listeners({ TestExecutionListener.class })
public class QuestionTest {

    public QuestionTest() throws Exception {
    }
    @Test
    public void questionTest() throws Exception {
     PersonalInformationBO personalInformationBO = new PersonalInformationBO();
     QuestionsPage questionsPage = new QuestionsPage();
     personalInformationBO.fillNotAllFields("test","test");
     questionsPage.checkPresentsOfQuestionsPage();
     questionsPage.analyzeTypeOfQuestions();
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
