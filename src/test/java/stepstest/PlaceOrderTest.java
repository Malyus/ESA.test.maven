package stepstest;

import allure.TestExecutionListener;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PlaceOrderBO;
import stepspage.PaymentDetailsPage;
import stepspage.PlaceOrderPage;
import stepspage.PrintDetailsPage;
import webdriver.SingletonDriver;

import static constants.PlaceOrderConstants.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 4/3/2018.
 */
@Listeners({ TestExecutionListener.class })
public class PlaceOrderTest {
    final static Logger LOG = Logger.getLogger(PlaceOrderTest.class);
    private PlaceOrderPage placeOrderPage;
    private PlaceOrderBO placeOrderBO;

    @TestCaseId("1.655")
    @Test
    public void checkIfOrderPriceIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.waitForPageValuesInitialization();
        LOG.info("Order price:" + placeOrderPage.getFullNameValue());
        assertEquals(placeOrderPage.getPriceOfCertificate(),COMBO_PLAN_PRICE,
                "Order price is not valid on Place Order");
    }
    @TestCaseId("1.662-1.663")
    @Test
    public void checkIfNameOfUserIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        LOG.info("Full name:" + placeOrderPage.getFullNameValue());
        assertEquals(placeOrderPage.getFullNameValue(),FULL_NAME,
                "User full name is not valid on Place Order");
    }
    @TestCaseId("1.664")
    @Test
    public void checkIfStreetAddressIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        LOG.info("Street address:" + placeOrderPage.getStreetValue());
        assertEquals(placeOrderPage.getStreetValue(),LA_STREET,
                "Street is not valid on Place Order");
    }
    @TestCaseId("1.665")
    @Test
    public void checkIfCityIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        LOG.info("City:" + placeOrderPage.getCityValue());
        assertEquals(placeOrderPage.getCityValue(),CA_CITY,
                "City is not valid on Place Order");
    }
    @TestCaseId("1.666")
    @Test
    public void checkIfStateIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        LOG.info("State:" + placeOrderPage.getStateValue());
        assertEquals(placeOrderPage.getStateValue(),CA_STATE,
                "State is not valid on Place Order");
    }
    @TestCaseId("1.667")
    @Test
    public void checkIfZipIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        LOG.info("Zip:" + placeOrderPage.getZipValue());
        assertEquals(placeOrderPage.getZipValue(),LA_ZIP,
                "Zip is not valid on Pace Order");
    }
    @TestCaseId("1.668")
    @Test
    public void checkIfFirstPetTypeIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        LOG.info("Pets type:" + placeOrderPage.getFirstPetsType());
        assertEquals(placeOrderPage.getFirstPetsType().toLowerCase(),FIRST_PETS_TYPE,
                "First pet type is not valid on Place Order");
    }
    @TestCaseId("1.669-1.671")
    @Test
    public void checkFirstPetValues() throws Exception {
       placeOrderBO = new PlaceOrderBO();
       placeOrderPage = placeOrderBO.goToPlaceOrderPage();
       LOG.info("First pet:" + placeOrderPage.getFirstPetsBreed()+";"+placeOrderPage.getFirstPetsName()
       +placeOrderPage.getFirstPetsWeight());
       SoftAssert softAssert = new SoftAssert();
       softAssert.assertEquals(placeOrderPage.getFirstPetsBreed(),FIRST_PETS_BREED,
               "First pets breed is not valid");
       softAssert.assertEquals(placeOrderPage.getFirstPetsName(),FIRST_PETS_NAME,
               "First pets name is not valid");
       softAssert.assertEquals(placeOrderPage.getFirstPetsWeight(),FIRST_PETS_WEIGHT,
               "First pets weight is not valid");
       softAssert.assertAll();
    }
    @TestCaseId("1.686")
    @Test
    public void checkIfSecondPetTypeIsValid() throws Exception {
      placeOrderBO = new PlaceOrderBO();
      placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
      LOG.info("First pets type:" + placeOrderPage.getSecondPetsType());
      assertEquals(placeOrderPage.getSecondPetsType(),SECOND_PETS_TYPE,
              "Second pet type is not valid");
    }
    @TestCaseId("1.687")
    @Test
    public void checkSecondPetBreed() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Second pet breed:" + placeOrderPage.getSecondPetsBreed());
        assertEquals(placeOrderPage.getSecondPetsBreed(),SECOND_PETS_BREED,
                "Second pet breed is not valid");
    }
    @TestCaseId("1.688")
    @Test
    public void checkSecondPetsName() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Second pets name:" + placeOrderPage.getSecondPetsName());
        assertEquals(placeOrderPage.getSecondPetsName(),SECOND_PETS_NAME,
                "Second pet name is not valid");
    }
    @TestCaseId("1.689")
    @Test
    public void checkSecondPetsWeight() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Second pet weight:" + placeOrderPage.getSecondPetsWeight());
        assertEquals(placeOrderPage.getSecondPetsWeight(),SECOND_PETS_WEIGHT,
                "Second pets weight is not valid");
    }
    @TestCaseId("1.690")
    @Test
    public void checkSecondPetsType() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Second pet type:" + placeOrderPage.getSecondPetsType());
        assertEquals(placeOrderPage.getSecondPetsType(),SECOND_PETS_TYPE,
                "Second pets type is not valid");
    }
    @TestCaseId("1.691")
    @Test
    public void checkSecondPetsBreed() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Second pet breed:" + placeOrderPage.getSecondPetsBreed());
        assertEquals(placeOrderPage.getSecondPetsBreed(),SECOND_PETS_BREED,
                "Second pet breed is not valid");
    }
    /*Requirements changes, only 2 pets are available now*/
   /* @TestCaseId("1.692")
    @Test
    public void checkThirdPetName() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Third pet name:" + placeOrderPage.getThirdPetsName());
        assertEquals(placeOrderPage.getThirdPetsName(),THIRD_PETS_NAME,
                "Third pet name is not valid");
    }
    @TestCaseId("1.693")
    @Test
    public void checkThirdPetsWeight() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Third pet weight:" + placeOrderPage.getThirdPetsWeight());
        assertEquals(placeOrderPage.getThirdPetsWeight(),THIRD_PETS_WEIGHT,
                "Third pets weight is not valid");
    }*/
    @TestCaseId("1.694")
    @Test
    public void checkIfEsaPackageIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("EsaPackage:" + placeOrderPage.getEsaPackageName());
        assertEquals(placeOrderPage.getEsaPackageName(),COMBO_PLAN_NAME,
                "Selected esa package name is not valid");
    }
    @TestCaseId("1.695")
    @Test
    public void checkIfAllSelectedAdditionalServicesAreDisplayed() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithTreePets();
        LOG.info("Selected additional service:" + placeOrderPage.getFirstServiceName());
        assertEquals(placeOrderPage.getFirstServiceName(),FIRST_SERVICE_NAME,
                "Selected additional service is not valid");
    }
    @TestCaseId("1.696")
    @Test
    public void checkIfAllServicesAreDisplayedAfterSelecting() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithAllServices();
        assertEquals(placeOrderPage.getFirstServiceName(),FIRST_SERVICE_NAME);
        assertEquals(placeOrderPage.getSecondServiceName(),SECOND_SERVICE_NAME);
        assertEquals(placeOrderPage.getThirdServiceName(),THIRD_SERVICE_NAME);
        assertEquals(placeOrderPage.getFourthServiceName(),FOURTH_SERVICE_NAME);
        assertEquals(placeOrderPage.getFifthServiceName(),FIFTH_SERVICE_NAME);
    }
    @TestCaseId("1.697")
    @Test
    public void checkIfSubtotalIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithAllServices();
        assertEquals(placeOrderPage.getOrderTotal(),"$237.00",
                "Order total is not valid");
    }
    @TestCaseId("1.698")
    @Test
    public void checkIfPriceForEsaPackageIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithAllServices();
        assertEquals(placeOrderPage.getEsaPackagePrice(),COMBO_PLAN_PRICE,
                "Price for Combo Plan is not valid");
    }
    @TestCaseId("1.699")
    @Test
    public void checkIfPriceForOneAdditionalServiceIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        assertEquals(placeOrderPage.getFirstServicePrice(),FIRST_SERVICE_PRICE,
                "First service price is not valid");
    }

    @TestCaseId("1.700")
    @Test
    public void checkIfPriceForAllAdditionalServicesIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderWithAllServices();
        assertEquals(placeOrderPage.getFirstServicePrice(),FIRST_SERVICE_PRICE,
                "First service price is not valid");
        assertEquals(placeOrderPage.getSecondServicePrice(),SECOND_SERVICE_PRICE,
                "Second service price is not valid");
        assertEquals(placeOrderPage.getThirdServicePrice(),THIRD_SERVICE_PRICE,
                "Third service price is not valid");
        assertEquals(placeOrderPage.getFourthServicePrice(),FOURTH_SERVICE_PRICE,
                "Fourth service price is not valid");
        assertEquals(placeOrderPage.getFifthServicePrice(),FIFTH_SERVICE_PRICE,
                "Fifth service price is not valid");
    }
    /*Today*/
    @TestCaseId("1.653")
    @Test
    public void checkIfAdditionalServiceIsDisplayed() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        assertTrue(placeOrderPage.getPresentsOfAdditionalService(),
                "Additional service is not displayed");
    }
    @TestCaseId("1.655")
    @Test
    public void checkIfPriceForAdditionalServiceIsDisplayed() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        assertEquals(placeOrderPage.getFirstServicePrice(),FIRST_SERVICE_PRICE,
                "First service price is not valid");
    }
    @TestCaseId("1.864")
    @Test
    public void checkIfPrintDetailsIsDisplayedAfterClickingOnContinueButton() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        PrintDetailsPage printDetailsPage = placeOrderPage.clickOnContinueButton();
        assertTrue(printDetailsPage.checkPresentsOfPage(),
        "Print Details Page is not displayed after clicking on Continue Button");
    }
    @TestCaseId("1.865")
    @Test
    public void checkIfPaymentDetailsPageIsDisplayedAfterClickingOnBackButton() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        placeOrderPage.clickOnBackButton();
        PaymentDetailsPage paymentDetailsPage = new PaymentDetailsPage();
        assertTrue(paymentDetailsPage.checkPresentsOfPaymentDetailsPage(),
                "Payment Details Page is not displayed after clicking on Back Button");
    }
    @TestCaseId("1.870")
    @Test
    public void checkIfProgressBardIsValid() throws Exception {
        placeOrderBO = new PlaceOrderBO();
        placeOrderPage = placeOrderBO.goToPlaceOrderPage();
        assertEquals(placeOrderPage.getProgressBarSize(),"width: 100%;",
                "Progress bar size is not valid o Place Order Page");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
