package stepstest;

import allure.TestExecutionListener;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.annotations.*;
import ru.qatools.properties.PropertyLoader;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PersonalInformationBO;
import stepspage.PersonalInformationPage;
import util.config.PersonalInformationConfig;
import util.testdatautils.Years;
import webdriver.SingletonDriver;

import java.util.List;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 1/25/2018.
 */
/*Good*/
@Listeners({ TestExecutionListener.class })
public class PersonalInformationTest {
    private PersonalInformationBO personalInformationBO;
    private PersonalInformationPage personalInformationPage;
    private PersonalInformationConfig personalConfig = PropertyLoader.newInstance().populate(PersonalInformationConfig.class);
    final static Logger LOG = Logger.getLogger(PersonalInformationTest.class);
    final String selectedMonth =  personalConfig.getSelectedMonth();
    final String selectedDay = personalConfig.getSelectedDay();
    final String selectedYear = personalConfig.getSelectedYear();
    final String selectedMaritalStatus = personalConfig.getSelectedMaritalStatus();
    final int countOfMonths = 12;
    final int countOfDays = 31;
    final String marriedStatus = personalConfig.getMarriedStatus();
    final String singleStatus = personalConfig.getSingleStatus();
    final String divorcedStatus = personalConfig.getDivorcedStatus();
    final String widowedStatus = personalConfig.getWidowedStatus();

    public PersonalInformationTest() throws Exception {
    }

    @TestCaseId("1.183")
    @Test
    public void checkIfFirstNameFieldIsDisplayed() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(personalInformationPage.checkPresentsOfFirstNameField(),"FirstNameField is not displayed!");
    }
    @TestCaseId("1.184")
    @Test
    public void checkIfLastNameFieldIsDisplayed() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(personalInformationPage.checkPresentsOfLastNameField(),"LastNameField is not displayed!");
    }
    @TestCaseId("1.204")
    @Test
    public void checkIfUserCantBeRedirectedToNextStepWhenFirstNameEmpty() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.fillNotAllFields("","User");
        LOG.info("ProgressBarLevel:" + personalInformationPage.checkPresentsOfPersonalInformationScreen());
        LOG.info("User name is:" + personalInformationBO.getEmail());
        assertEquals(personalInformationPage.checkPresentsOfPersonalInformationScreen(),"width: 14%;","System allows to go to next stepsproperties when FirstName is empty!");
    }
    @TestCaseId("1.205")
    @Test
    public void checkIfUserCantBeRedirectedToNextStepWhenLastNameEmpty() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.fillNotAllFields("User","");
       LOG.info("ProgressBarLevel:" + personalInformationPage.checkPresentsOfPersonalInformationScreen());
       assertEquals(personalInformationPage.checkPresentsOfPersonalInformationScreen(),"width: 14%;","System allows to go to next stepsproperties when LastNameField is empty!");
    }
    @TestCaseId("1.206")
    @Test
    public void checkIfFirstNameCantBeWithSpaceOnly() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.fillNotAllFields(" ","User");
        LOG.info("ProgressBarLevel:" + personalInformationPage.checkPresentsOfPersonalInformationScreen());
        assertEquals(personalInformationPage.checkPresentsOfPersonalInformationScreen(),"width: 14%;","System allows to go to next stepsproperties when FirstName is with space only!");
    }
    @TestCaseId(" 1.207")
    @Test
    public void checkIfLastNameCantBeWithSpaceOnly() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.fillNotAllFields("User"," ");
        LOG.info("ProgressBarLevel:" + personalInformationPage.checkPresentsOfPersonalInformationScreen());
        assertEquals(personalInformationPage.checkPresentsOfPersonalInformationScreen(),"width: 14%;","System allows to go to next stepsproperties when LastName is with space only!");
    }
    @TestCaseId("1.226")
    @Test
    public void checkIfSelectedMonthIsDisplayed() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.selectFirstMonth();
        LOG.info("SelectedMonth:" + personalInformationPage.getSelectedMonth());
        assertEquals(personalInformationPage.getSelectedMonth(),selectedMonth,"SelectedMonth is not January!");
    }
    @TestCaseId("1.227")
    @Test
    public void checkIfSelectedDayIsDisplayed() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.selectFirstDay();
        LOG.info("SelectedDay:" + personalInformationPage.getSelectedDay());
        assertEquals(personalInformationPage.getSelectedDay(),selectedDay,"SelectedDay is not 1!");
    }
    @TestCaseId("1.228")
    @Test
    public void checkIfSelectedYearIsDisplayed() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.selectFirstYear();
        LOG.info("SelectedYear:" + personalInformationPage.getSelectedYear());
        assertEquals(personalInformationPage.getSelectedYear(),selectedYear,"SelectedYear is not 1928!");
    }
    @TestCaseId("1.229")
    @Test
    public void checkIfSelectedMaritalStatusIsDisplayed() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.goToPersonalInformationPage();
        personalInformationPage.selectMarriedStatus();
        LOG.info("SelectedMaritalStatus:" + personalInformationPage.getMaritalStatus());
        assertEquals(personalInformationPage.getMaritalStatus(),selectedMaritalStatus,"SelectedMaritalStatus is not displayed!");
    }
   @TestCaseId("1.242")
   @Test
   public void checkIfCountOfMonthsIsValid() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       LOG.info("CountOfMonths:" + personalInformationPage.getCountOfMonths());
       assertEquals(personalInformationPage.getCountOfMonths(),countOfMonths,"CountOfMonths is not 12!");
   }
   @TestCaseId("1.243")
   @Test
   public void checkIfCountOfDaysIs31() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       LOG.info("CountOfDys:" + personalInformationPage.getCountOfDays());
       assertEquals(personalInformationPage.getCountOfDays(),countOfDays,"CountOfDays is not 31!");
   }
   @TestCaseId("1.244")
   @Test
   public void checkCountYearsForValid() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       LOG.info("CountOfYears:" + personalInformationPage.getCountOfYears());
       personalInformationPage.clickOnYearOpenButton();
       List<WebElement> listOfYear = personalInformationPage.getListOfYears();
       for(int i=0;i<listOfYear.size();i++){
         assertEquals(listOfYear.get(i).getText(), Years.create().get(i),"Years are not valid!");
       }
   }
   @TestCaseId("1.245")
   @Test
   public void checkMaritalStatusesForValid() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnMaritalStatusOpenButton();
       assertEquals(personalInformationPage.getNameOfStatus(0),marriedStatus,"MarriedStatus is is not present!");
       assertEquals(personalInformationPage.getNameOfStatus(1),singleStatus,"SingleStatus is not present!");
       assertEquals(personalInformationPage.getNameOfStatus(2),divorcedStatus,"DivorcedStatus is not present!");
       assertEquals(personalInformationPage.getNameOfStatus(3),widowedStatus,"WidowedStatus is not present!");
   }
   @TestCaseId("1.246")
   @Test
   public void checkIfFirstDayIs1() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnDayOpenButton();
       assertEquals(personalInformationPage.getFirstDayValue(),"1","FirstDayValue is not 1!");
   }
   @TestCaseId("1.247")
   @Test
   public void checkIFLastDayIs31() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnDayOpenButton();
       assertEquals(personalInformationPage.getLastDayValue(),"31","LastDay is not 31!");
   }
   @TestCaseId("1.248")
   @Test
   public void checkIfFirstMonthIsJanuary() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnMonthOpenButton();
       assertEquals(personalInformationPage.getNameOfFirstMonth(),"Jan","FirstMonth is not January!");
   }
   @TestCaseId("1.249")
   @Test
   public void checkIfLastMonthIsDecember() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnMonthOpenButton();
       assertEquals(personalInformationPage.getNameOfLastMonth(),"Dec","LastMonth is not December!");
   }
   @TestCaseId("1.244")
   @Test
   public void checkIfFirstYearIs1928() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnYearOpenButton();
       assertEquals(personalInformationPage.getNameOfFirstYear(),"1928","FirstYear is not 1928!");
   }
   @TestCaseId("1.245")
   @Test
   public void checkIFLastYearIs2001() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnYearOpenButton();
       assertEquals(personalInformationPage.getNameOfLastYear(),"2001","LastYear is not 2001!");
   }
   @TestCaseId("1.265")
   @Test
   public void checkIfContinueButtonIsNotActiveWhenFirstNameIsEmpty() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.fillNotAllFields("","test");
       LOG.info("Active:" + personalInformationPage.getActiveOfContinueButton());
       assertEquals(personalInformationPage.getActiveOfContinueButton(),"true","ContinueButton is not disabled when FirstName field is empty!");
   }
   @TestCaseId("1.266")
   @Test
   public void checkIfContinueButtonIsNotActiveWhenLastNameIsEmpty() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.fillNotAllFields("test","");
       LOG.info("Active:" + personalInformationPage.getActiveOfContinueButton());
       assertEquals(personalInformationPage.getActiveOfContinueButton(),"true","ContinueButton is not disabled when LastName field is empty!");
   }
    @TestCaseId("1.250")
    @Test
    public void checkIfFemalButtonIsSelectedByDefault() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.fillNotAllFields("","");
       assertTrue(personalInformationPage.checkIfFemaleButtonIsActive(),"Female button is not active by default");
    }
   @TestCaseId("1.251")
   @Test
   public void checkIfMaleButtonIsSelectedAfterActivating() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationPage = new PersonalInformationPage();
        personalInformationBO.fillNotAllFields("","");
        personalInformationPage.clickOnMaleCheckBox();
        Thread.sleep(2500);
        assertTrue(personalInformationPage.checkIfMaleButtonIsActive(),"Male button is not active after selecting");
   }
   @TestCaseId("1.252")
   @Test
   public void checkIfFemaleButtonIsActiveAfterActivating() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnMaleCheckBox();
       personalInformationPage.clickOnFemaleCheckBox();
       assertTrue(personalInformationPage.checkIfFemaleButtonIsActiveAfterClicking(),"Female button is not active after selecting");
   }
   @TestCaseId("1.253")
   @Test
   public void checkIfGenderButtonsCantBeActiveSimultaneously() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnMaleCheckBox();
       personalInformationPage.clickOnFemaleCheckBox();
       assertEquals(personalInformationPage.getActiveOfGenderButtons(),1,"Gender buttons cant be active simultaneously");
   }
   @TestCaseId("1.274")
   @Test
   public void checkIfPricingPageIsDisplayedAfterClickingOnBackButton() throws Exception {
       personalInformationBO = new PersonalInformationBO();
       personalInformationPage = new PersonalInformationPage();
       personalInformationBO.goToPersonalInformationPage();
       personalInformationPage.clickOnBackButton();
       assertTrue(personalInformationPage.checkPresentsOfPreviousPage(),
               "User is not redirected to PricingPAge after clicking on back button");
   }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
       SingletonDriver.quit();
    }
}
