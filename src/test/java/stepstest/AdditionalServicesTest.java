package stepstest;

import allure.TestExecutionListener;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PersonalInformationBO;
import stepsbo.PetSectionBO;
import stepspage.AdditionalServicesPage;
import webdriver.SingletonDriver;

import static constants.ServicesConstants.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 3/6/2018.
 */
/*Good*/
@Listeners({ TestExecutionListener.class })
public class AdditionalServicesTest {
    protected PetSectionBO petSectionBO;
    protected PersonalInformationBO personalInformationBO;

    public AdditionalServicesTest() throws Exception {
    }

    @TestCaseId("")
    @Test
    public void checkIfNameOfFirstAdditionalServiceIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getFirstServiceName(),FIRST_SERVICE_NAME,"Name of first service is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidForFirstService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),FIRST_SERVICE_PRICE);
    }
    @TestCaseId("")
    @Test
    public void checkFirstIconForValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.checkFirstIconForValid(),FIRST_SERVICE_ICON,"FirstIcon is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfFirstServiceIsActiveAfterSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        assertEquals(additionalServicesPage.getActiveOfFirstService(),"true",
                "FirstService is not active after selecting");
    }
    @TestCaseId("")
    @Test
    public void checkIfFirstServiceIsDisabledAfterSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        assertEquals(additionalServicesPage.getDisabledOfFirstService(),"false",
                "FirstService is not disabled after deselecting");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingFirstService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),FIRST_SERVICE_PRICE,
                "Subtotal of firstService price is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalReloadAfterDisablingFirstService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),DEF_SUBTOTAL,
                "SubTotal is not reloaded after disabling firstService");
    }
    @TestCaseId("")
    @Test
    public void checkIfSecondServiceNameIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getSecondServiceName(),SECOND_SERVICE_NAME,
                "SecondService name is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSecondServicePriceIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getSecondServicePrice(),SECOND_SERVICE_PRICE,
                "SecondService price is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkSecondServiceLogoForValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.checkSecondServiceLogo(),SECOND_SERVICE_ICON,
                "SecondService logo is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSecondServiceIsActiveAfterSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        assertEquals(additionalServicesPage.getActiveOfSecondService(),"true",
                "SecondSection is not active after selecting");
    }
    @TestCaseId("")
    @Test
    public void checkIfSecondServiceIsDisabledAfterDoubleSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        assertEquals(additionalServicesPage.getDisabledOfSecondService(),"false",
                "SecondService is not disabled after double selecting");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingSecondService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),SECOND_SERVICE_PRICE,
                "Subtotal for SecondService is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsZeroAfterTwiceClickingOnHawaiianAirlinesService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),"$0",
                "Subtotal is not zero after deselecting hawaiian service");
    }
    @TestCaseId("")
    @Test
    public void checkNameOfThirdServiceForValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getNameOfThirdService(),THIRD_SERVICE_NAME,
                "Name of third service is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfThirdServicePriceIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getThirdServicePrice(),THIRD_SERVICE_PRICE,
                "Third service price is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfThirdServiceLogoIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.checkThirdServiceLogo(),THIRD_SERVICE_ICON,
                "Third service logo is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfThirdServiceIsActiveAfterSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        assertEquals(additionalServicesPage.getActiveOfThirdService(),"true",
                "Third service is not active after selecting");
    }
    @TestCaseId("")
    @Test
    public void testSkip1() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip2() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip3() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip4() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip5() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void checkIfThirdServiceIsDisabledAfterDoubleClicking() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        assertEquals(additionalServicesPage.getDisabledOfThirdService(),"false",
                "Third service is not disabled after double clicking");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidForThirdService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),THIRD_SERVICE_PRICE,
                "Third service subtotal is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsZeroAfterDisablingThirdService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),DEF_SUBTOTAL,
                "Subtotal is not zero after deselecting third service");
    }
    @TestCaseId("")
    @Test
    public void checkIfNameOfFourthServiceIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getNameOfFourthService(),FOURTH_SERVICE_NAME,
                "Fourth service name is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfPriceOfFourthServiceIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getPriceOfFourthService(),FOURTH_SERVICE_PRICE,
                "Fourth service price is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfFourthServiceLogoIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.checkFourthServiceLogo(),FOURTH_SERVICE_ICON,
                "Fourth service logo is not valid");
    }
    @TestCaseId("")
    @Test
    public void testSkip6() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip7() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip8() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip9() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip10() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip11() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip12() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip13() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip14() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void testSkip15() throws Exception {
        personalInformationBO = new PersonalInformationBO();
        personalInformationBO.goToPersonalInformationPage();
        assertTrue(true);
    }
    @TestCaseId("")
    @Test
    public void checkIfFourthServiceIsActiveAfterSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        assertEquals(additionalServicesPage.getActiveOfFourthService(),"true",
                "Fourth service is not active after selecting");
    }
    @TestCaseId("")
    @Test
    public void checkIfFourthServiceIsDisabledAfterDoubleClicking() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        assertEquals(additionalServicesPage.getDisabledOfFourthService(),"false",
                "Fourth service is not disabled after double clicking");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingFourthService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),FOURTH_SERVICE_PRICE,
                "Subtotal for fourth service is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsZeroAfterDisablingFourthService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),DEF_SUBTOTAL,
                "Subtotal is not zero after disabling forth service");
    }
    @TestCaseId("")
    @Test
    public void checkIfFifthServiceNameIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getNameOfFifthService(),FIFTH_SERVICE_NAME,
                "Fifth service name is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfFifthServicePriceIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.getPriceOfFifthService(),FIFTH_SERVICE_PRICE,
                "Fifth service price is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfFifthServiceLogoIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        assertEquals(additionalServicesPage.checkFifthServiceLogo(),FIFTH_SERVICE_ICON,
                "Fifth service logo is not valid");
    }
    @TestCaseId("")
    @Test
    public void checkIfFifthServiceIsActiveAfterSelecting() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnUnitedSpecialForm();
        assertEquals(additionalServicesPage.getActiveOfFifthService(),"true",
                "Fifth service is not active after selecting");
    }
    @TestCaseId("")
    @Test
    public void checkIfFifthServiceIfDisabledAfterDoubleClicking() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnUnitedSpecialForm();
        additionalServicesPage.clickOnUnitedSpecialForm();
        assertEquals(additionalServicesPage.getDisabledOfFifthService(),"false",
                "Fifth service is not disabled after double clicking");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingAllFiveServices() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        additionalServicesPage.clickOnUnitedSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),TOTAL_PRICE,
                "Subtotal is invalid after selecting all services");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingFirstAndSecondServices() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),FIRST_AND_SECOND_PRICE,
                "Subtotal is invalid after selecting first and second services");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingFirstAndThirdService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(), FIRST_AND_THIRD_PRICE,
                "Subtotal is invalid after selecting first and third service");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingFirstAndFourthService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),FIRST_AND_FOURTH_PRICE,
                "Subtotal is invalid after selecting first and fourth service");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingFirstAndFifthService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnAmericanAirlinesForm();
        additionalServicesPage.clickOnUnitedSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),FIRST_AND_FIFTH_PRICE,
                "Subtotal is invalid after selecting first and fifth service");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingSecondAndThirdService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnJetBlueAirwaysSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),SECOND_AND_THIRD_PRICE,
                "Subtotal is invalid after selecting second and third service");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingSecondAndFourthService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnSpiritAirwaysForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),SECOND_AND_FOURTH_PRICE,
                "Subtotal is invalid after selecting second and fourth service");
    }
    @TestCaseId("")
    @Test
    public void checkIfSubtotalIsValidAfterSelectingSecondAndFifthService() throws Exception {
        petSectionBO = new PetSectionBO();
        AdditionalServicesPage additionalServicesPage = petSectionBO.goToAdditionalServicesPage();
        additionalServicesPage.clickOnHawaiianAirlinesSpecialForm();
        additionalServicesPage.clickOnUnitedSpecialForm();
        assertEquals(additionalServicesPage.getSubtotalFieldValue(),SECOND_AND_FIFTH_PRICE,
                "Subtotal is invalid after selecting second and fifth service");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
