package stepstest;

import com.questions.QuestionsPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import testbase.TestBase;

/**
 * Created by M.Malyus on 2/5/2018.
 */
public class QuetionTest extends TestBase {

    @Test(groups = {"question"})
    public void test(){
     personalInformationBO.fillNotAllFields("test","test");
     questionsPage.checkPresentsOfQuestionsPage();
     questionsPage.analyzeTypeOfQuestions();
    }
}
