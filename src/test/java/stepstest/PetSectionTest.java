package stepstest;

import allure.TestExecutionListener;
import com.browserstack.local.Local;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PetSectionBO;
import stepsbo.QuestionsBO;
import stepspage.PersonalInformationPage;
import stepspage.PetSectionPage;
import webdriver.SingletonDriver;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.testng.Assert.assertEquals;


/**
 * Created by M.Malyus on 2/6/2018.
 */
/*Good*/
@Listeners({TestExecutionListener.class})
public class PetSectionTest {
    protected WebDriver webDriver;
    protected Local l;
    protected QuestionsBO questionsBO;
    protected PetSectionPage petSectionPage;
    protected PetSectionBO petSectionBO;
    protected PersonalInformationPage personalInformationPage = new PersonalInformationPage();
    private String onePetActive = "ng-untouched ng-valid ng-not-empty ng-dirty ng-valid-parse";
    private String disabledContinue = "disabled";

    public PetSectionTest() throws Exception {
    }

    @Test
    @TestCaseId("1.305")
    public void checkIfPetBreedAllowTwentySymbols() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsOnePet("twentySymbolsPetBreed", "Elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to write 20 symbole into PetBreed!");
    }

    @TestCaseId("1.306")
    @Test
    public void checkIfPetNameAllowTwentySymbols() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsOnePet("cat", "ElmoWithTwentySymbols");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to write 20 symbols into PetName!");
    }

    @TestCaseId("1.359")
    @Test
    public void checkIfBreedIsSavedAfterBackButton() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsOnePet("cat", "elmo");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        assertEquals(petSectionPage.getPetsBreedValue(), "cat", "PetsBreed is not valid after turning back!");
    }

    @TestCaseId("1.360")
    @Test
    public void checkIfNameISSavedAfterBackButton() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsOnePet("cat", "elmo");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        assertEquals(petSectionPage.getPetsNameValue(), "elmo", "PetsName is not valid after turning back!");
    }

    @TestCaseId("1.401")
    @Test
    public void checkIfUserCanUploadPhotoByChangePhotoButton() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadJpeg();
        petSectionPage.clickOnFirstCropButton();
        Assert.assertTrue(petSectionPage.checkPresentsOfCroppedPhoto(), "Photo is not uploaded by 'ChangePhoto' button!");
    }

    @TestCaseId("1.309")
    @Test
    public void checkIfUserCanEditSavedPetsBreed() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsOnePet("cat", "elmo");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clearPetsBreedInput();
        petSectionPage.writePetsBreed("frog");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        assertEquals(petSectionPage.getPetsBreedValue(), "frog", "Systen does not allow to edit pets breed!");
    }

    @TestCaseId("1.310")
    @Test
    public void checkIfUSerCanEditSavedPetsName() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsOnePet("cat", "elmo");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clearPetsNameInput();
        petSectionPage.writePetsName("kermet");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        assertEquals(petSectionPage.getPetsNameValue(), "kermet", "System does not allow to edit pets name!");
    }

    @TestCaseId("1.311")
    @Test
    public void checkIfUserCanEnterTwentySymbolsIntoSecondPetsBreed() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "kermet", "frogwithtwentysymbols");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow 20 symbols into pets breed #2!");
    }

    @TestCaseId("1.312")
    @Test
    public void checkIfUserCanEnterTwentySymbolsIntoSecondPetsName() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "kermetwithtwentysymbols", "frog");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow 20 symbols into pets name #2!");
    }

    @TestCaseId("1.315")
    @Test
    public void checkIfUserCanEditSecondPetsName() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "kermet", "cat");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.writeSecondPetsName("secondelmo");
        assertEquals(petSectionPage.getValueOfSecondPetsName(), "secondelmo", "System does not allow to edit pets name #2!");
    }

    @TestCaseId("1.316")
    @Test
    public void checkIfUserCanEditSecondPetsBreed() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "kermet", "cat");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.writeSecondPetsBreed("secondbreed");
        assertEquals(petSectionPage.getValueOfSecondPetsBreed(), "secondbreed", "System does not allow to edit pets breed#2!");
    }

    @TestCaseId("1.317")
    @Test
    public void checkIfUserCanEnterTwentySymbolsIntoThirdPetBreed() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsThreePets("frogwithtwentysymbols", "kermit");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to write pet breed#3 with 20 symbols!");
    }

    @TestCaseId("1.318")
    @Test
    public void checkIfUserCanEnterTwentySymbolsIntoThirdPetName() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsThreePets("frog", "kermitwithtwentysymbols");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to write pet name#3 with 20 symbols!");
    }

//    @TestCaseId("1.321")
//    @Test
//    public void checkIfUserCanEditThirdPetsBreed() throws Exception {
//        petSectionPage = new PetSectionPage();
//        petSectionBO = new PetSectionBO();
//        petSectionBO.fillNotAllFieldsThreePets("frog", "kermit");
//        petSectionPage.clickOnContinueButton();
//        petSectionPage.clickOnBackButtonOfNextStep();
//        petSectionPage.clickOnThreePetsButton();
//        petSectionPage.writeThirdPetsBreed("thirdbreed");
//        assertEquals(petSectionPage.getThirdPetsBreedValue(), "thirdbreed", "System does not allow to edit pet breed#3!");
//    }

    @TestCaseId("1.363")
    @Test
    public void checkIfSecondBreedIsSavedAfterTurnedBack() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "frog", "ermit");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clickOnTwoPetsButton();
        assertEquals(petSectionPage.getValueOfSecondPetsBreed(), "ermit", "System does not save correctly pets breed#2!");
    }

    @TestCaseId("1.364")
    @Test
    public void checkIfSecondNameIsSavedAfterTurnedBack() throws Exception {
        petSectionPage = new PetSectionPage();
        petSectionBO = new PetSectionBO();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "frog", "ermit");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clickOnTwoPetsButton();
        assertEquals(petSectionPage.getValueOfSecondPetsName(), "ermit", "System does not save correctly pet name#2!");
    }
    @TestCaseId("1.368")
    @Test
    public void checkIfOnePetButtonIsSelectedByDefault() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        assertEquals(petSectionPage.getActiveOfOnePetButton(), "ng-pristine ng-untouched ng-valid ng-empty", "OnePetButton is not active by default!");
    }

    @TestCaseId("1.369")
    @Test
    public void checkIfOnePetButtonIsActivatedAfterClickingOnIt() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.clickOnOnePetButton();
        assertEquals(petSectionPage.getActiveOfOnePetButton(), onePetActive, "OnePetButton is not active after activating!");
    }
    @TestCaseId("1.370")
    @Test
    public void checkIfTwoPetButtonIsActivatedAfterClickingOnIt() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        assertEquals(petSectionPage.getActiveOfSecondPetButton(), onePetActive, "TwoPetButton is not active after activating!");
    }
    @TestCaseId("1.381")
    @Test
    public void checkIfPetSectionTwoIsDisplayedAfterClickingOnTwoPetsButton() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        Assert.assertTrue(petSectionPage.checkIfTwoPetsSectionIsDisplayed(), "TwoPets section is not displayed after clicking on TwoPetsButton!");
    }
    @TestCaseId("1.382")
    @Test
    public void checkIfPetSectionIsNotDisplayedAfterDisablingTwoPetButton() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.clickOnOnePetButton();
        Assert.assertTrue(petSectionPage.checkIfTwoPetsSectionIsNotDisplayed(), "TwoPetSection is displayed after deactivating TwoPetsButton!");
    }
    /*This test-cases are commented because
    * now we can make orders for two pets
    * not for three*/
//    @TestCaseId("1.383")
//    @Test
//    public void checkIfThirdAndSecondPetSectionsAreDisplayedAfterClickingOnThreePetButton() throws Exception {
//        questionsBO = new QuestionsBO();
//        petSectionPage = new PetSectionPage();
//        questionsBO.goToPetSectionPage();
//        petSectionPage.clickOnThreePetsButton();
//        Assert.assertTrue(petSectionPage.checkIfTwoPetsSectionIsDisplayed(), "TwoPets section is no displayed after clicking on TwoPetsButton!");
//        Assert.assertTrue(petSectionPage.checkIfThreePetsSectionIsDisplayed(), "ThreePets section is not displayed after clicking on ThreePetsButton!");
//    }

//    @TestCaseId("1.384")
//    @Test
//    public void checkIfThirdAndSecondPetSectionsAreNotDisplayedAfterDisablingThreePetsButton() throws Exception {
//        questionsBO = new QuestionsBO();
//        petSectionPage = new PetSectionPage();
//        questionsBO.goToPetSectionPage();
//        petSectionPage.clickOnThreePetsButton();
//        petSectionPage.clickOnOnePetButton();
//        Assert.assertTrue(petSectionPage.checkIfTwoPetsSectionIsNotDisplayed(), "TwoPetsSection is displayed after disabling ThreePetsButton!");
//        Assert.assertTrue(petSectionPage.checkIfThreePetsSectionIsNotDisplayed(), "ThreePetsSection is displayed after disabling ThreePetsButton!");
//    }

//    @TestCaseId("1.385")
//    @Test
//    public void checkIfAllPetSectionsAreDisplayedAfterClickingOnThreePetsButton() throws Exception {
//        questionsBO = new QuestionsBO();
//        petSectionPage = new PetSectionPage();
//        questionsBO.goToPetSectionPage();
//        petSectionPage.clickOnThreePetsButton();
//        Assert.assertTrue(petSectionPage.checkPresentsOfOnePetSection(), "OnePetSection is not displayed after clicking on ThreePetsButton!");
//        Assert.assertTrue(petSectionPage.checkIfTwoPetsSectionIsDisplayed(), "TwoPetSection is not displayed after clicking on ThreePetsButton!");
//        Assert.assertTrue(petSectionPage.checkIfThreePetsSectionIsDisplayed(), "ThreePetSection is not displayed after clicking on ThreePetsButton!");
//    }

    @TestCaseId("1.387")
    @Test
    public void checkIfUserCanSelectPetsTypeInThePetsTypeDropdownList() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.selectPetsType(1);
    }
    @TestCaseId("1.388")
    @Test
    public void checkIfUserCanSelectPetsWeightInThePetsWeightDropdownList() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.selectPetsWeight(1);
    }
    @TestCaseId("1.389")
    @Test
    public void checkIfSelectedTypeIsDisplayedInPetsTypeDropdownMenu() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
       /*Select second type - Dog*/
        petSectionPage.selectPetsType(1);
        assertEquals(petSectionPage.getNameOfSelectedPetsType(), "Dog", "SelectedPetsTypeName is not valid!");
    }
    @TestCaseId("1.390")
    @Test
    public void checkIfSelectedPetsWeightIsDisplayedInPetsWeightDropdownMenu() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
       /*Select second weight - 10 To 20lbs.*/
        petSectionPage.selectPetsWeight(1);
        assertEquals(petSectionPage.getNameOfSelectedPetsWeight(), "10 To 20lbs.", "SelectedPetsWeight is not valid!");
    }

    //Id
    @TestCaseId("1.397")
    @Test
    public void checkUploadedPhotoIsDisplayed() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        Assert.assertTrue(petSectionPage.checkPresentsOfCroppedPhoto(), "Photo is not uploaded!");
    }

    @TestCaseId("1.398")
    @Test
    public void checkIfUploadPhotoButtonChangesToChangePhotoButton() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        assertEquals(petSectionPage.getTextOfUploadPhotoButton(), "Change Photo", "UploadPhotoButton does not change to ChangePhotoButton!");
    }

    @TestCaseId("1.408")
    @Test
    public void checkIfUserCanGoToShippingDetailsPageWhenFirstPetSectionIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillNotAllFieldsOnePet("cat", "elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to go to ShippingDetailsPage whe all fields of FirstPetSection are valid!");
    }

    @TestCaseId("1.409")
    @Test
    public void checkIsUserCanGoToShippingDetailsPageWhenFirstAndSecondPetSectionsAreValid() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillNotAllFieldsTwoPet("cat", "elmo", "fro", "kermit");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to go to ShippingDetailsPage when all fields of FirstAndSecondPetSection are valid!");
    }

    @TestCaseId("1.410")
    @Test
    public void checkIfSystemDoesNotAllowToGoToNextStepWhenSecondPetSectionIsEmpty() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillNotAllFieldsOnePet("cat", "elmo");
        petSectionPage.clickOnTwoPetsButton();
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allow to go to ShippingDetailsPage when SecondPetSection is empty!");
    }

    @TestCaseId("1.411")
    @Test
    public void checkIfSystemDoesNotAllowToGoToNextStepWhenFirstPetSectionIsEmptyAndSecondIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillNotAllFieldsTwoPet("", "", "cat", "elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when SecondPetSection is empty!");
    }

    @TestCaseId("1.412")
    @Test
    public void checkIfSystemDoesNotAllowWhenAllFieldsAreEmpty() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO. goToPetSectionPage();
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when FirstAndSecondPetSections are empty!");
    }

    @TestCaseId("1.413")
    @Test
    public void checkIfUserCanGoToShippingDetailsScreenWhenAllThreePetSectionsAreWithCorrectData() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillNotAllFieldsThreePets("cat", "elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "System does not allow to go to ShippingDetailsScreen whe all ThreePetSections are with valid value!");
    }

    @TestCaseId("1.414")
    @Test
    public void checkIfUserCanNotGoToShippingDetailsScreenWhenFirstAndSecondPetSectionsAreCorrectAndThirdSectionIsEmpty() throws Exception {
    /*   Leave empty fields of ThirdPetSection,
     * all other valid*/
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillNotAllFieldsThreePets("", "");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when first and second pet sections are valid and third is empty!");
    }

    @TestCaseId("1.415")
    @Test
    public void checkIfUserCanNotNavigateToShippingDetailsWhenFirstAndThirdPetSectionValidAndSecondIsEmpty() throws Exception {
     /*Leave empty fields of SecondPetSection,
     * all other valid*/
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillSomeFieldsOfAllThreePetsSections("cat", "elmo", "", "", "frog", "kermet");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when first and third pet sections are valid and second is empty!");
    }

    @TestCaseId("1.416")
    @Test
    public void checkIfUserCanNotNavigateToShippingDetailsWhenSecondAndThirdPetSectionsAreValidAndFirstIsEmpty() throws Exception {
     /* Leave empty fields of FirstPetSection,
     * all other valid*/
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillSomeFieldsOfAllThreePetsSections("", "", "cat", "elmo", "frog", "kermet");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when second adn third pet sections are valid and first is empty!");
    }

    @TestCaseId("1.417")
    @Test
    public void checkIfUserCanNotNavigateToShippingDetailsWhenOnlyFirstPetSectionIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillSomeFieldsOfAllThreePetsSections("cat", "elmo", "", "", "", "");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when only first section from three is valid!");
    }

    @TestCaseId("1.418")
    @Test
    public void checkIfUserCanNotGoToShippingDetailsWhenOnlySecondSectionIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillSomeFieldsOfAllThreePetsSections("", "", "cat", "elmo", "", "");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allow to go to ShippingDetailsPage when only second section from thee is valid!");
    }

    @TestCaseId("1.419")
    @Test
    public void checkIfUserCanNotGoToShippingDetailsWhenOnlyThirdSectionIsValid() throws Exception {
        petSectionBO = new PetSectionBO();
        petSectionPage = new PetSectionPage();
        petSectionBO.fillSomeFieldsOfAllThreePetsSections("", "", "", "", "cat", "elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "System allows to go to ShippingDetailsPage when only third section from three is valid!");
    }
    @TestCaseId("1.421")
    @Test
    public void checkIfContinueIsActiveWhenIDCardForPetIsNotSelected() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), null, "Continue button is disabled!");
    }
    @TestCaseId("1.422")
    @Test
    public void checkIfContinueButtonIsDisabledWhenIdCardForPetIsActivatedAndPhotoIsNotUploaded() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clickOnIDCardForPet();
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), null, "Continue button is not disabled when photo is not uploaded!");
    }
    @TestCaseId("1.423")
    @Test
    public void checkIfContinueButtonIsActiveWhenAllFieldsAreValidAnIDCardForPetPhotoIsUploaded() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clickOnIDCardForPet();
        petSectionPage.uploadPhoto();
        petSectionPage.clickOnCropButton();
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), null, "Continue button is not active when all fields are valid and photo for IDCardForPet is uploaded!");
    }
    @TestCaseId("1.427")
    @Test
    public void checkIfContinueButtonIsDisabledWhenPetsBreedIsEmpty() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsName("elmo");
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), "true", "Continue button is not disabled when pets breed is empty");
    }
    @TestCaseId("1.428")
    @Test
    public void checkIfContinueButtonIsDisabledWhenPetsBreedIsFilledWithSpace() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed(" ");
        petSectionPage.writePetsName("elmo");
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), "true", "Continue button is not disabled when pets breed is filled with space!");
    }
    @TestCaseId("1.433")
    @Test
    public void checkIfContinueButtonIsDisabledWhenPetsNameIsEmpty() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("");
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), "true", "Continue button is not disabled when pets name is empty!");
    }
    @TestCaseId("1.434")
    @Test
    public void checkIfContinueButtonIsDisabledWhenPetsNameIsFilledWithSpace() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName(" ");
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), "true", "Continue button is not disabled when pets name is filled with space!");
    }
    @TestCaseId("1.437")
    @Test
    public void checkIfContinueButtonIsDisabledWhenUserDeletePetsBreedFieldd() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clearPetsBreedInput();
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), "true", "Continue button is not disabled when creditcard delete pets breed field!");
    }
    @TestCaseId("1.438")
    @Test
    public void checkIfContinueButtonIsDisabledWhenUserDeletePetsNameField() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clearPetsNameInput();
        assertEquals(petSectionPage.getDisabledValueOfContinueButton(), "true", "Continue button is not disabled when usr delete pets name field!");
    }
    @TestCaseId("1.445")
    @Test
    public void checkIfAdditionalServicesPageIsDisplayedAfterClickingOnContinueButton() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "AdditionalServicesPage is not displayed when creditcard click on continue button!");
    }
    @TestCaseId("1.446")
    @Test
    public void checkIfPersonalInformationIsDisplayedAfterClickingOnBackButton() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.clickOnBackButton();
        personalInformationPage.writeIntoFirstNameField("elmo");
        assertThat(personalInformationPage.checkPresentsOfPersonalInformationScreen()).isEqualToIgnoringCase("width: 14%;");
    }
    @TestCaseId("1.449")
    @Test
    public void checkIfProgressBarIsValid() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "PetSection progress bar is not valid!");
    }
    @TestCaseId("1.453")
    @Test(groups = "petsection")
    public void checkIfProgressBarOfAdditionalServicesIaValid() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clickOnContinueButton();
        assertEquals(petSectionPage.checkPresentsOfNextStep(), "width: 56%;", "AdditionalServices progress bar is not valid!");
    }
    @TestCaseId("1.455")
    @Test
    public void checkIfProgressBarIsDecreasedAfterTurningBackFromAdditionalServicesToPetSection() throws Exception {
        questionsBO = new QuestionsBO();
        petSectionPage = new PetSectionPage();
        questionsBO.goToPetSectionPage();
        petSectionPage.writePetsBreed("cat");
        petSectionPage.writePetsName("elmo");
        petSectionPage.clickOnContinueButton();
        petSectionPage.clickOnBackButtonOfNextStep();
        petSectionPage.clearPetsNameInput();
        petSectionPage.writePetsName("elmo");
        assertEquals(petSectionPage.checkPresentsOfPetSection(), "width: 42%;", "PetSection progress bar is not decreased after turning back from AdditionalServices!");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }

}
