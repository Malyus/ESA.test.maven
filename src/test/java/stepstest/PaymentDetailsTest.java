package stepstest;

import allure.TestExecutionListener;
import constants.ShippingAddressConstants;
import org.apache.log4j.Logger;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.TestCaseId;
import stepsbo.PaymentDetailsBO;
import stepsbo.PetSectionBO;
import stepsbo.ShippingBO;
import stepspage.PaymentDetailsPage;
import stepspage.PlaceOrderPage;
import stepspage.ShippingAddressPage;
import util.parsers.PaymentJson;
import webdriver.SingletonDriver;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by M.Malyus on 3/26/2018.
 */
/*All this tests need update for asserts
* when PaymentDetailsPage will be validate*/
@Listeners({ TestExecutionListener.class })
public class PaymentDetailsTest {

    final static Logger LOG = Logger.getLogger(PaymentDetailsTest.class);
    protected PetSectionBO petSectionBO;
    protected ShippingBO shippingBO;
    private PaymentDetailsBO paymentDetailsBO;
    private ShippingAddressConstants shippingAddressConstants = new ShippingAddressConstants();
    private PaymentJson paymentJson = new PaymentJson();
    private String street = shippingAddressConstants.getStreet();
    private String city = shippingAddressConstants.getCity();
    private String state = shippingAddressConstants.getState();
    private String zip = shippingAddressConstants.getZip();
    private String validCreditCard = paymentJson.getCreditCard();
    private String validDate = paymentJson.getExpDate();
    private String validCvv = paymentJson.getCvv();
    private String progressBarSize = "width: 84%;";

    public PaymentDetailsTest() throws Exception {
    }

    @TestCaseId("1.569")
    @Test
    public void checkIfUserCanFinishTheRegistrationWithCurrentYear() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),
                "User can't finish PaymentDetailsPage with current year");
    }
    @TestCaseId("1.570")
    @Test
    public void checkIfUserCanFinishTheRegistrationWithNextYear() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),
                "User can't finish PaymentDetailsPage with next year");
    }
    @TestCaseId("1.571")
    @Test
    public void checkIfUserCantFinishTheRegistrationWhenCreditCardIncludesFifteenDigits() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage("4111 1111 1111 111",validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),
                "User can't finish PaymentDetailsPage when card number have only 15 digits");
    }
    @TestCaseId("1.572")
    @Test
    public void checkIfUserCanFinishTheRegistrationWheCreditCardIncludesSeventeenDigits() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage("4111 1111 1111 1111 1",validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),
                "User can't finish PaymentDetailsPage when card number have 17 digits");
    }
    @TestCaseId("1.573")
    @Test
    public void checkIfUserCanFinishTheRegistrationWhenCreditCardIsEmpty() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage("",validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),
                "User can't finish PaymentDetailsPage when card number is empty");
    }
    @TestCaseId("1.576")
    @Test
    public void checkIfUserCanFinishWithInvalidNumberOfMonth() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,"2020/13",validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),"Expiration date can't be with invalid number of month");
    }
    @TestCaseId("1.577")
    @Test
    public void checkIfUserCantFinishWhenExpDateIsEmpty() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,"",validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),"Expiration date can't be empty");
    }
    @TestCaseId("1.578")
    @Test
    public void checkIfUserCantFinishWhenOnlyDateIsCorrectAndCvvLessThan3Symbols() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage("1",validDate,"99");
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),"CreditCard and cvv can't be empty");
    }
    @TestCaseId("1.579")
    @Test
    public void checkIfUserCantFinishWhenOnlyDateIsCorrectAndCvvMoreThan3Symbols() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage("1",validDate,"99");
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),"CreditCard and cvv can't be empty");
    }
    @TestCaseId("1.580")
    @Test
    public void checkIfCreditNumberIsSaved() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,"99");
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        placeOrderPage.clickOnBackButton();
        paymentDetailsPage.checkPresentsOfPaymentDetailsPage();
        assertEquals(paymentDetailsPage.getCreditCardValue(),validCreditCard,"CreditCard and cvv can't be empty");
    }
    /*Today*/
    /*Update value of credit card after validation*/
    @TestCaseId("1.584")
    @Test
    public void checkIfCreditCardNumberIsSaved() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        placeOrderPage.checkPresentsOfPlaceOrderPage();
        placeOrderPage.clickOnBackButton();
        paymentDetailsPage.checkPresentsOfPaymentDetailsPage();
        assertEquals(paymentDetailsPage.getValueOfExpDate(),"",
                "Date is not saved after turning back");
    }
    @TestCaseId("1.586")
    @Test
    public void checkIfCvvIsSaved() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        placeOrderPage.checkPresentsOfPlaceOrderPage();
        placeOrderPage.clickOnBackButton();
        paymentDetailsPage.checkPresentsOfPaymentDetailsPage();
        assertEquals(paymentDetailsPage.getValueOfCvvField(),"",
                "Cvv is not saved after turning back");
    }
    @TestCaseId("1.633")
    @Test
    public void checkIfPlaceOrderIsDisplayedAfterClickingOnContinueButton() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsBO.fillFieldsOfPaymentDetailsPage(validCreditCard,validDate,validCvv);
        PlaceOrderPage placeOrderPage = paymentDetailsPage.clickOnContinueButton();
        assertTrue(placeOrderPage.checkPresentsOfPlaceOrderPage(),
                "User is not redirected to PlaceOrder after clicking on continue button");
    }

    @TestCaseId("1.634")
    @Test
    public void checkIfShippingDetailsIsShownAfterTurningBack() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsPage.checkPresentsOfPaymentDetailsPage();
        paymentDetailsPage.clickOnBackButton();
        ShippingAddressPage shippingAddressPage = new ShippingAddressPage();
        assertTrue(shippingAddressPage.checkPresentsOfShippingAddressPage(),
                "ShippingAddress is not shown after turning back");
    }
    @TestCaseId("1.638")
    @Test
    public void checkIfProgressBarSizeIsValid() throws Exception {
        paymentDetailsBO = new PaymentDetailsBO();
        PaymentDetailsPage paymentDetailsPage = paymentDetailsBO.goToPaymentDetailsPage();
        paymentDetailsPage.checkPresentsOfPaymentDetailsPage();
        assertEquals(paymentDetailsPage.getSizeOfProgressBar(),progressBarSize,
                "ProgressBarSize is not valid");
    }
    @AfterMethod(alwaysRun = true)
    public void tearDown() throws Exception {
        SingletonDriver.quit();
    }
}
